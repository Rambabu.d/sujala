require 'test_helper'

class CropListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @crop_list = crop_lists(:one)
  end

  test "should get index" do
    get crop_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_crop_list_url
    assert_response :success
  end

  test "should create crop_list" do
    assert_difference('CropList.count') do
      post crop_lists_url, params: { crop_list: { name: @crop_list.name } }
    end

    assert_redirected_to crop_list_url(CropList.last)
  end

  test "should show crop_list" do
    get crop_list_url(@crop_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_crop_list_url(@crop_list)
    assert_response :success
  end

  test "should update crop_list" do
    patch crop_list_url(@crop_list), params: { crop_list: { name: @crop_list.name } }
    assert_redirected_to crop_list_url(@crop_list)
  end

  test "should destroy crop_list" do
    assert_difference('CropList.count', -1) do
      delete crop_list_url(@crop_list)
    end

    assert_redirected_to crop_lists_url
  end
end
