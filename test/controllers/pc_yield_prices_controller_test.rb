require 'test_helper'

class PcYieldPricesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pc_yield_price = pc_yield_prices(:one)
  end

  test "should get index" do
    get pc_yield_prices_url
    assert_response :success
  end

  test "should get new" do
    get new_pc_yield_price_url
    assert_response :success
  end

  test "should create pc_yield_price" do
    assert_difference('PcYieldPrice.count') do
      post pc_yield_prices_url, params: { pc_yield_price: { crop_by_product_price: @pc_yield_price.crop_by_product_price, crop_by_product_yield: @pc_yield_price.crop_by_product_yield, crop_main_product_price: @pc_yield_price.crop_main_product_price, crop_main_product_yield: @pc_yield_price.crop_main_product_yield, intercrop_by_product_price: @pc_yield_price.intercrop_by_product_price, intercrop_by_product_yield: @pc_yield_price.intercrop_by_product_yield, intercrop_main_product_price: @pc_yield_price.intercrop_main_product_price, intercrop_main_product_yield: @pc_yield_price.intercrop_main_product_yield } }
    end

    assert_redirected_to pc_yield_price_url(PcYieldPrice.last)
  end

  test "should show pc_yield_price" do
    get pc_yield_price_url(@pc_yield_price)
    assert_response :success
  end

  test "should get edit" do
    get edit_pc_yield_price_url(@pc_yield_price)
    assert_response :success
  end

  test "should update pc_yield_price" do
    patch pc_yield_price_url(@pc_yield_price), params: { pc_yield_price: { crop_by_product_price: @pc_yield_price.crop_by_product_price, crop_by_product_yield: @pc_yield_price.crop_by_product_yield, crop_main_product_price: @pc_yield_price.crop_main_product_price, crop_main_product_yield: @pc_yield_price.crop_main_product_yield, intercrop_by_product_price: @pc_yield_price.intercrop_by_product_price, intercrop_by_product_yield: @pc_yield_price.intercrop_by_product_yield, intercrop_main_product_price: @pc_yield_price.intercrop_main_product_price, intercrop_main_product_yield: @pc_yield_price.intercrop_main_product_yield } }
    assert_redirected_to pc_yield_price_url(@pc_yield_price)
  end

  test "should destroy pc_yield_price" do
    assert_difference('PcYieldPrice.count', -1) do
      delete pc_yield_price_url(@pc_yield_price)
    end

    assert_redirected_to pc_yield_prices_url
  end
end
