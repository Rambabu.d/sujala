require 'test_helper'

class IrrigationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @irrigation = irrigations(:one)
  end

  test "should get index" do
    get irrigations_url
    assert_response :success
  end

  test "should get new" do
    get new_irrigation_url
    assert_response :success
  end

  test "should create irrigation" do
    assert_difference('Irrigation.count') do
      post irrigations_url, params: { irrigation: { defunct: @irrigation.defunct, kharif: @irrigation.kharif, perennial_cops: @irrigation.perennial_cops, rabi: @irrigation.rabi, source_of_irrigation: @irrigation.source_of_irrigation, summer: @irrigation.summer, water_depth: @irrigation.water_depth, water_quality: @irrigation.water_quality, working: @irrigation.working, yield: @irrigation.yield } }
    end

    assert_redirected_to irrigation_url(Irrigation.last)
  end

  test "should show irrigation" do
    get irrigation_url(@irrigation)
    assert_response :success
  end

  test "should get edit" do
    get edit_irrigation_url(@irrigation)
    assert_response :success
  end

  test "should update irrigation" do
    patch irrigation_url(@irrigation), params: { irrigation: { defunct: @irrigation.defunct, kharif: @irrigation.kharif, perennial_cops: @irrigation.perennial_cops, rabi: @irrigation.rabi, source_of_irrigation: @irrigation.source_of_irrigation, summer: @irrigation.summer, water_depth: @irrigation.water_depth, water_quality: @irrigation.water_quality, working: @irrigation.working, yield: @irrigation.yield } }
    assert_redirected_to irrigation_url(@irrigation)
  end

  test "should destroy irrigation" do
    assert_difference('Irrigation.count', -1) do
      delete irrigation_url(@irrigation)
    end

    assert_redirected_to irrigations_url
  end
end
