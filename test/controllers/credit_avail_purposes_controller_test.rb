require 'test_helper'

class CreditAvailPurposesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @credit_avail_purpose = credit_avail_purposes(:one)
  end

  test "should get index" do
    get credit_avail_purposes_url
    assert_response :success
  end

  test "should get new" do
    get new_credit_avail_purpose_url
    assert_response :success
  end

  test "should create credit_avail_purpose" do
    assert_difference('CreditAvailPurpose.count') do
      post credit_avail_purposes_url, params: { credit_avail_purpose: { credit_source_id: @credit_avail_purpose.credit_source_id, name: @credit_avail_purpose.name } }
    end

    assert_redirected_to credit_avail_purpose_url(CreditAvailPurpose.last)
  end

  test "should show credit_avail_purpose" do
    get credit_avail_purpose_url(@credit_avail_purpose)
    assert_response :success
  end

  test "should get edit" do
    get edit_credit_avail_purpose_url(@credit_avail_purpose)
    assert_response :success
  end

  test "should update credit_avail_purpose" do
    patch credit_avail_purpose_url(@credit_avail_purpose), params: { credit_avail_purpose: { credit_source_id: @credit_avail_purpose.credit_source_id, name: @credit_avail_purpose.name } }
    assert_redirected_to credit_avail_purpose_url(@credit_avail_purpose)
  end

  test "should destroy credit_avail_purpose" do
    assert_difference('CreditAvailPurpose.count', -1) do
      delete credit_avail_purpose_url(@credit_avail_purpose)
    end

    assert_redirected_to credit_avail_purposes_url
  end
end
