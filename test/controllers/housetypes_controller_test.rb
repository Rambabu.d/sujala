require 'test_helper'

class HousetypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @housetype = housetypes(:one)
  end

  test "should get index" do
    get housetypes_url
    assert_response :success
  end

  test "should get new" do
    get new_housetype_url
    assert_response :success
  end

  test "should create housetype" do
    assert_difference('Housetype.count') do
      post housetypes_url, params: { housetype: { is_active: @housetype.is_active, name: @housetype.name } }
    end

    assert_redirected_to housetype_url(Housetype.last)
  end

  test "should show housetype" do
    get housetype_url(@housetype)
    assert_response :success
  end

  test "should get edit" do
    get edit_housetype_url(@housetype)
    assert_response :success
  end

  test "should update housetype" do
    patch housetype_url(@housetype), params: { housetype: { is_active: @housetype.is_active, name: @housetype.name } }
    assert_redirected_to housetype_url(@housetype)
  end

  test "should destroy housetype" do
    assert_difference('Housetype.count', -1) do
      delete housetype_url(@housetype)
    end

    assert_redirected_to housetypes_url
  end
end
