require 'test_helper'

class SurveyNumbersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @survey_number = survey_numbers(:one)
  end

  test "should get index" do
    get survey_numbers_url
    assert_response :success
  end

  test "should get new" do
    get new_survey_number_url
    assert_response :success
  end

  test "should create survey_number" do
    assert_difference('SurveyNumber.count') do
      post survey_numbers_url, params: { survey_number: { farmer_id: @survey_number.farmer_id, grid1: @survey_number.grid1, grid2: @survey_number.grid2, grid3: @survey_number.grid3, grid4: @survey_number.grid4, hissa_number: @survey_number.hissa_number, number: @survey_number.number } }
    end

    assert_redirected_to survey_number_url(SurveyNumber.last)
  end

  test "should show survey_number" do
    get survey_number_url(@survey_number)
    assert_response :success
  end

  test "should get edit" do
    get edit_survey_number_url(@survey_number)
    assert_response :success
  end

  test "should update survey_number" do
    patch survey_number_url(@survey_number), params: { survey_number: { farmer_id: @survey_number.farmer_id, grid1: @survey_number.grid1, grid2: @survey_number.grid2, grid3: @survey_number.grid3, grid4: @survey_number.grid4, hissa_number: @survey_number.hissa_number, number: @survey_number.number } }
    assert_redirected_to survey_number_url(@survey_number)
  end

  test "should destroy survey_number" do
    assert_difference('SurveyNumber.count', -1) do
      delete survey_number_url(@survey_number)
    end

    assert_redirected_to survey_numbers_url
  end
end
