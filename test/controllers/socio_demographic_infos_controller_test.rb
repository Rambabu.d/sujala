require 'test_helper'

class SocioDemographicInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @socio_demographic_info = socio_demographic_infos(:one)
  end

  test "should get index" do
    get socio_demographic_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_socio_demographic_info_url
    assert_response :success
  end

  test "should create socio_demographic_info" do
    assert_difference('SocioDemographicInfo.count') do
      post socio_demographic_infos_url, params: { socio_demographic_info: { age: @socio_demographic_info.age, education_id: @socio_demographic_info.education_id, farmer_id: @socio_demographic_info.farmer_id, gender_id: @socio_demographic_info.gender_id, local_org_member: @socio_demographic_info.local_org_member, local_org_office_id: @socio_demographic_info.local_org_office_id, occupation_id: @socio_demographic_info.occupation_id } }
    end

    assert_redirected_to socio_demographic_info_url(SocioDemographicInfo.last)
  end

  test "should show socio_demographic_info" do
    get socio_demographic_info_url(@socio_demographic_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_socio_demographic_info_url(@socio_demographic_info)
    assert_response :success
  end

  test "should update socio_demographic_info" do
    patch socio_demographic_info_url(@socio_demographic_info), params: { socio_demographic_info: { age: @socio_demographic_info.age, education_id: @socio_demographic_info.education_id, farmer_id: @socio_demographic_info.farmer_id, gender_id: @socio_demographic_info.gender_id, local_org_member: @socio_demographic_info.local_org_member, local_org_office_id: @socio_demographic_info.local_org_office_id, occupation_id: @socio_demographic_info.occupation_id } }
    assert_redirected_to socio_demographic_info_url(@socio_demographic_info)
  end

  test "should destroy socio_demographic_info" do
    assert_difference('SocioDemographicInfo.count', -1) do
      delete socio_demographic_info_url(@socio_demographic_info)
    end

    assert_redirected_to socio_demographic_infos_url
  end
end
