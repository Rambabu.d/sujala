require 'test_helper'

class PcOtherExpensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pc_other_expense = pc_other_expenses(:one)
  end

  test "should get index" do
    get pc_other_expenses_url
    assert_response :success
  end

  test "should get new" do
    get new_pc_other_expense_url
    assert_response :success
  end

  test "should create pc_other_expense" do
    assert_difference('PcOtherExpense.count') do
      post pc_other_expenses_url, params: { pc_other_expense: { dryland_rental_value: @pc_other_expense.dryland_rental_value, electricity: @pc_other_expense.electricity, farmer_id: @pc_other_expense.farmer_id, insurance: @pc_other_expense.insurance, interest_on_fixed_cost: @pc_other_expense.interest_on_fixed_cost, interest_on_variable_cost: @pc_other_expense.interest_on_variable_cost, irrigated_rental_value: @pc_other_expense.irrigated_rental_value, land_revenue: @pc_other_expense.land_revenue, managerial_cost: @pc_other_expense.managerial_cost, miscellaneous: @pc_other_expense.miscellaneous, production_cost_id: @pc_other_expense.production_cost_id, user_id: @pc_other_expense.user_id } }
    end

    assert_redirected_to pc_other_expense_url(PcOtherExpense.last)
  end

  test "should show pc_other_expense" do
    get pc_other_expense_url(@pc_other_expense)
    assert_response :success
  end

  test "should get edit" do
    get edit_pc_other_expense_url(@pc_other_expense)
    assert_response :success
  end

  test "should update pc_other_expense" do
    patch pc_other_expense_url(@pc_other_expense), params: { pc_other_expense: { dryland_rental_value: @pc_other_expense.dryland_rental_value, electricity: @pc_other_expense.electricity, farmer_id: @pc_other_expense.farmer_id, insurance: @pc_other_expense.insurance, interest_on_fixed_cost: @pc_other_expense.interest_on_fixed_cost, interest_on_variable_cost: @pc_other_expense.interest_on_variable_cost, irrigated_rental_value: @pc_other_expense.irrigated_rental_value, land_revenue: @pc_other_expense.land_revenue, managerial_cost: @pc_other_expense.managerial_cost, miscellaneous: @pc_other_expense.miscellaneous, production_cost_id: @pc_other_expense.production_cost_id, user_id: @pc_other_expense.user_id } }
    assert_redirected_to pc_other_expense_url(@pc_other_expense)
  end

  test "should destroy pc_other_expense" do
    assert_difference('PcOtherExpense.count', -1) do
      delete pc_other_expense_url(@pc_other_expense)
    end

    assert_redirected_to pc_other_expenses_url
  end
end
