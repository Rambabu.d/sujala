require 'test_helper'

class IntercropListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @intercrop_list = intercrop_lists(:one)
  end

  test "should get index" do
    get intercrop_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_intercrop_list_url
    assert_response :success
  end

  test "should create intercrop_list" do
    assert_difference('IntercropList.count') do
      post intercrop_lists_url, params: { intercrop_list: { name: @intercrop_list.name } }
    end

    assert_redirected_to intercrop_list_url(IntercropList.last)
  end

  test "should show intercrop_list" do
    get intercrop_list_url(@intercrop_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_intercrop_list_url(@intercrop_list)
    assert_response :success
  end

  test "should update intercrop_list" do
    patch intercrop_list_url(@intercrop_list), params: { intercrop_list: { name: @intercrop_list.name } }
    assert_redirected_to intercrop_list_url(@intercrop_list)
  end

  test "should destroy intercrop_list" do
    assert_difference('IntercropList.count', -1) do
      delete intercrop_list_url(@intercrop_list)
    end

    assert_redirected_to intercrop_lists_url
  end
end
