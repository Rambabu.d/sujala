require 'test_helper'

class BasicNeedDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @basic_need_detail = basic_need_details(:one)
  end

  test "should get index" do
    get basic_need_details_url
    assert_response :success
  end

  test "should get new" do
    get new_basic_need_detail_url
    assert_response :success
  end

  test "should create basic_need_detail" do
    assert_difference('BasicNeedDetail.count') do
      post basic_need_details_url, params: { basic_need_detail: { farmer_id: @basic_need_detail.farmer_id, health_card: @basic_need_detail.health_card, hours_of_electricity: @basic_need_detail.hours_of_electricity, light: @basic_need_detail.light, no_of_days: @basic_need_detail.no_of_days, no_of_persons: @basic_need_detail.no_of_persons, pds_card: @basic_need_detail.pds_card, toilet_facility: @basic_need_detail.toilet_facility, user_id: @basic_need_detail.user_id, work_nrgea: @basic_need_detail.work_nrgea } }
    end

    assert_redirected_to basic_need_detail_url(BasicNeedDetail.last)
  end

  test "should show basic_need_detail" do
    get basic_need_detail_url(@basic_need_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_basic_need_detail_url(@basic_need_detail)
    assert_response :success
  end

  test "should update basic_need_detail" do
    patch basic_need_detail_url(@basic_need_detail), params: { basic_need_detail: { farmer_id: @basic_need_detail.farmer_id, health_card: @basic_need_detail.health_card, hours_of_electricity: @basic_need_detail.hours_of_electricity, light: @basic_need_detail.light, no_of_days: @basic_need_detail.no_of_days, no_of_persons: @basic_need_detail.no_of_persons, pds_card: @basic_need_detail.pds_card, toilet_facility: @basic_need_detail.toilet_facility, user_id: @basic_need_detail.user_id, work_nrgea: @basic_need_detail.work_nrgea } }
    assert_redirected_to basic_need_detail_url(@basic_need_detail)
  end

  test "should destroy basic_need_detail" do
    assert_difference('BasicNeedDetail.count', -1) do
      delete basic_need_detail_url(@basic_need_detail)
    end

    assert_redirected_to basic_need_details_url
  end
end
