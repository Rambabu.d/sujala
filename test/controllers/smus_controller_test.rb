require 'test_helper'

class SmusControllerTest < ActionDispatch::IntegrationTest
  setup do
    @smu = smus(:one)
  end

  test "should get index" do
    get smus_url
    assert_response :success
  end

  test "should get new" do
    get new_smu_url
    assert_response :success
  end

  test "should create smu" do
    assert_difference('Smu.count') do
      post smus_url, params: { smu: { is_active: @smu.is_active, name: @smu.name } }
    end

    assert_redirected_to smu_url(Smu.last)
  end

  test "should show smu" do
    get smu_url(@smu)
    assert_response :success
  end

  test "should get edit" do
    get edit_smu_url(@smu)
    assert_response :success
  end

  test "should update smu" do
    patch smu_url(@smu), params: { smu: { is_active: @smu.is_active, name: @smu.name } }
    assert_redirected_to smu_url(@smu)
  end

  test "should destroy smu" do
    assert_difference('Smu.count', -1) do
      delete smu_url(@smu)
    end

    assert_redirected_to smus_url
  end
end
