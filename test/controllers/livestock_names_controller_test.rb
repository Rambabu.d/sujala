require 'test_helper'

class LivestockNamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @livestock_name = livestock_names(:one)
  end

  test "should get index" do
    get livestock_names_url
    assert_response :success
  end

  test "should get new" do
    get new_livestock_name_url
    assert_response :success
  end

  test "should create livestock_name" do
    assert_difference('LivestockName.count') do
      post livestock_names_url, params: { livestock_name: { name: @livestock_name.name } }
    end

    assert_redirected_to livestock_name_url(LivestockName.last)
  end

  test "should show livestock_name" do
    get livestock_name_url(@livestock_name)
    assert_response :success
  end

  test "should get edit" do
    get edit_livestock_name_url(@livestock_name)
    assert_response :success
  end

  test "should update livestock_name" do
    patch livestock_name_url(@livestock_name), params: { livestock_name: { name: @livestock_name.name } }
    assert_redirected_to livestock_name_url(@livestock_name)
  end

  test "should destroy livestock_name" do
    assert_difference('LivestockName.count', -1) do
      delete livestock_name_url(@livestock_name)
    end

    assert_redirected_to livestock_names_url
  end
end
