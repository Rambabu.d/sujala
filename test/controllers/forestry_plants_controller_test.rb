require 'test_helper'

class ForestryPlantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @forestry_plant = forestry_plants(:one)
  end

  test "should get index" do
    get forestry_plants_url
    assert_response :success
  end

  test "should get new" do
    get new_forestry_plant_url
    assert_response :success
  end

  test "should create forestry_plant" do
    assert_difference('ForestryPlant.count') do
      post forestry_plants_url, params: { forestry_plant: { name: @forestry_plant.name } }
    end

    assert_redirected_to forestry_plant_url(ForestryPlant.last)
  end

  test "should show forestry_plant" do
    get forestry_plant_url(@forestry_plant)
    assert_response :success
  end

  test "should get edit" do
    get edit_forestry_plant_url(@forestry_plant)
    assert_response :success
  end

  test "should update forestry_plant" do
    patch forestry_plant_url(@forestry_plant), params: { forestry_plant: { name: @forestry_plant.name } }
    assert_redirected_to forestry_plant_url(@forestry_plant)
  end

  test "should destroy forestry_plant" do
    assert_difference('ForestryPlant.count', -1) do
      delete forestry_plant_url(@forestry_plant)
    end

    assert_redirected_to forestry_plants_url
  end
end
