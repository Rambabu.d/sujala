require 'test_helper'

class LocalOrganisationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @local_organisation = local_organisations(:one)
  end

  test "should get index" do
    get local_organisations_url
    assert_response :success
  end

  test "should get new" do
    get new_local_organisation_url
    assert_response :success
  end

  test "should create local_organisation" do
    assert_difference('LocalOrganisation.count') do
      post local_organisations_url, params: { local_organisation: { is_active: @local_organisation.is_active, name: @local_organisation.name } }
    end

    assert_redirected_to local_organisation_url(LocalOrganisation.last)
  end

  test "should show local_organisation" do
    get local_organisation_url(@local_organisation)
    assert_response :success
  end

  test "should get edit" do
    get edit_local_organisation_url(@local_organisation)
    assert_response :success
  end

  test "should update local_organisation" do
    patch local_organisation_url(@local_organisation), params: { local_organisation: { is_active: @local_organisation.is_active, name: @local_organisation.name } }
    assert_redirected_to local_organisation_url(@local_organisation)
  end

  test "should destroy local_organisation" do
    assert_difference('LocalOrganisation.count', -1) do
      delete local_organisation_url(@local_organisation)
    end

    assert_redirected_to local_organisations_url
  end
end
