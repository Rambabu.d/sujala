require 'test_helper'

class TopographiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @topography = topographies(:one)
  end

  test "should get index" do
    get topographies_url
    assert_response :success
  end

  test "should get new" do
    get new_topography_url
    assert_response :success
  end

  test "should create topography" do
    assert_difference('Topography.count') do
      post topographies_url, params: { topography: { is_active: @topography.is_active, name: @topography.name } }
    end

    assert_redirected_to topography_url(Topography.last)
  end

  test "should show topography" do
    get topography_url(@topography)
    assert_response :success
  end

  test "should get edit" do
    get edit_topography_url(@topography)
    assert_response :success
  end

  test "should update topography" do
    patch topography_url(@topography), params: { topography: { is_active: @topography.is_active, name: @topography.name } }
    assert_redirected_to topography_url(@topography)
  end

  test "should destroy topography" do
    assert_difference('Topography.count', -1) do
      delete topography_url(@topography)
    end

    assert_redirected_to topographies_url
  end
end
