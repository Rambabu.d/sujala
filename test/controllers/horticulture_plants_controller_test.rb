require 'test_helper'

class HorticulturePlantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @horticulture_plant = horticulture_plants(:one)
  end

  test "should get index" do
    get horticulture_plants_url
    assert_response :success
  end

  test "should get new" do
    get new_horticulture_plant_url
    assert_response :success
  end

  test "should create horticulture_plant" do
    assert_difference('HorticulturePlant.count') do
      post horticulture_plants_url, params: { horticulture_plant: { name: @horticulture_plant.name } }
    end

    assert_redirected_to horticulture_plant_url(HorticulturePlant.last)
  end

  test "should show horticulture_plant" do
    get horticulture_plant_url(@horticulture_plant)
    assert_response :success
  end

  test "should get edit" do
    get edit_horticulture_plant_url(@horticulture_plant)
    assert_response :success
  end

  test "should update horticulture_plant" do
    patch horticulture_plant_url(@horticulture_plant), params: { horticulture_plant: { name: @horticulture_plant.name } }
    assert_redirected_to horticulture_plant_url(@horticulture_plant)
  end

  test "should destroy horticulture_plant" do
    assert_difference('HorticulturePlant.count', -1) do
      delete horticulture_plant_url(@horticulture_plant)
    end

    assert_redirected_to horticulture_plants_url
  end
end
