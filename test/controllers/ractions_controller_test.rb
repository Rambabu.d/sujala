require 'test_helper'

class RactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @raction = ractions(:one)
  end

  test "should get index" do
    get ractions_url
    assert_response :success
  end

  test "should get new" do
    get new_raction_url
    assert_response :success
  end

  test "should create raction" do
    assert_difference('Raction.count') do
      post ractions_url, params: { raction: { name: @raction.name } }
    end

    assert_redirected_to raction_url(Raction.last)
  end

  test "should show raction" do
    get raction_url(@raction)
    assert_response :success
  end

  test "should get edit" do
    get edit_raction_url(@raction)
    assert_response :success
  end

  test "should update raction" do
    patch raction_url(@raction), params: { raction: { name: @raction.name } }
    assert_redirected_to raction_url(@raction)
  end

  test "should destroy raction" do
    assert_difference('Raction.count', -1) do
      delete raction_url(@raction)
    end

    assert_redirected_to ractions_url
  end
end
