require 'test_helper'

class PcOperationDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pc_operation_detail = pc_operation_details(:one)
  end

  test "should get index" do
    get pc_operation_details_url
    assert_response :success
  end

  test "should get new" do
    get new_pc_operation_detail_url
    assert_response :success
  end

  test "should create pc_operation_detail" do
    assert_difference('PcOperationDetail.count') do
      post pc_operation_details_url, params: { pc_operation_detail: { amount: @pc_operation_detail.amount, farmer_id: @pc_operation_detail.farmer_id, hired_bullock: @pc_operation_detail.hired_bullock, hired_machinery: @pc_operation_detail.hired_machinery, hired_men: @pc_operation_detail.hired_men, hired_tractor: @pc_operation_detail.hired_tractor, hired_women: @pc_operation_detail.hired_women, input: @pc_operation_detail.input, operation_detail: @pc_operation_detail.operation_detail, own_bullock: @pc_operation_detail.own_bullock, own_machinery: @pc_operation_detail.own_machinery, own_men: @pc_operation_detail.own_men, own_tractor: @pc_operation_detail.own_tractor, own_women: @pc_operation_detail.own_women, production_cost_id: @pc_operation_detail.production_cost_id, user_id: @pc_operation_detail.user_id } }
    end

    assert_redirected_to pc_operation_detail_url(PcOperationDetail.last)
  end

  test "should show pc_operation_detail" do
    get pc_operation_detail_url(@pc_operation_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_pc_operation_detail_url(@pc_operation_detail)
    assert_response :success
  end

  test "should update pc_operation_detail" do
    patch pc_operation_detail_url(@pc_operation_detail), params: { pc_operation_detail: { amount: @pc_operation_detail.amount, farmer_id: @pc_operation_detail.farmer_id, hired_bullock: @pc_operation_detail.hired_bullock, hired_machinery: @pc_operation_detail.hired_machinery, hired_men: @pc_operation_detail.hired_men, hired_tractor: @pc_operation_detail.hired_tractor, hired_women: @pc_operation_detail.hired_women, input: @pc_operation_detail.input, operation_detail: @pc_operation_detail.operation_detail, own_bullock: @pc_operation_detail.own_bullock, own_machinery: @pc_operation_detail.own_machinery, own_men: @pc_operation_detail.own_men, own_tractor: @pc_operation_detail.own_tractor, own_women: @pc_operation_detail.own_women, production_cost_id: @pc_operation_detail.production_cost_id, user_id: @pc_operation_detail.user_id } }
    assert_redirected_to pc_operation_detail_url(@pc_operation_detail)
  end

  test "should destroy pc_operation_detail" do
    assert_difference('PcOperationDetail.count', -1) do
      delete pc_operation_detail_url(@pc_operation_detail)
    end

    assert_redirected_to pc_operation_details_url
  end
end
