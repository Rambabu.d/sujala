require 'test_helper'

class PcWageRatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pc_wage_rate = pc_wage_rates(:one)
  end

  test "should get index" do
    get pc_wage_rates_url
    assert_response :success
  end

  test "should get new" do
    get new_pc_wage_rate_url
    assert_response :success
  end

  test "should create pc_wage_rate" do
    assert_difference('PcWageRate.count') do
      post pc_wage_rates_url, params: { pc_wage_rate: { bullock_pair: @pc_wage_rate.bullock_pair, farmer_id: @pc_wage_rate.farmer_id, female: @pc_wage_rate.female, machinery: @pc_wage_rate.machinery, male: @pc_wage_rate.male, production_cost_id: @pc_wage_rate.production_cost_id, tractor: @pc_wage_rate.tractor, user_id: @pc_wage_rate.user_id } }
    end

    assert_redirected_to pc_wage_rate_url(PcWageRate.last)
  end

  test "should show pc_wage_rate" do
    get pc_wage_rate_url(@pc_wage_rate)
    assert_response :success
  end

  test "should get edit" do
    get edit_pc_wage_rate_url(@pc_wage_rate)
    assert_response :success
  end

  test "should update pc_wage_rate" do
    patch pc_wage_rate_url(@pc_wage_rate), params: { pc_wage_rate: { bullock_pair: @pc_wage_rate.bullock_pair, farmer_id: @pc_wage_rate.farmer_id, female: @pc_wage_rate.female, machinery: @pc_wage_rate.machinery, male: @pc_wage_rate.male, production_cost_id: @pc_wage_rate.production_cost_id, tractor: @pc_wage_rate.tractor, user_id: @pc_wage_rate.user_id } }
    assert_redirected_to pc_wage_rate_url(@pc_wage_rate)
  end

  test "should destroy pc_wage_rate" do
    assert_difference('PcWageRate.count', -1) do
      delete pc_wage_rate_url(@pc_wage_rate)
    end

    assert_redirected_to pc_wage_rates_url
  end
end
