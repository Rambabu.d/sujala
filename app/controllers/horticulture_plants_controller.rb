class HorticulturePlantsController < ApplicationController
  before_action :set_horticulture_plant, only: [:show, :edit, :update, :destroy]

  # GET /horticulture_plants
  # GET /horticulture_plants.json
  def index
    @horticulture_plants = HorticulturePlant.all
    render layout: false
  end

  # GET /horticulture_plants/1
  # GET /horticulture_plants/1.json
  def show
  end

  # GET /horticulture_plants/new
  def new
    @horticulture_plant = HorticulturePlant.new
  end

  # GET /horticulture_plants/1/edit
  def edit
  end

  # POST /horticulture_plants
  # POST /horticulture_plants.json
  def create
    @horticulture_plant = HorticulturePlant.new(horticulture_plant_params)

    respond_to do |format|
      if @horticulture_plant.save
        format.html { redirect_to @horticulture_plant, notice: 'Horticulture plant was successfully created.' }
        format.json { render :show, status: :created, location: @horticulture_plant }
      else
        format.html { render :new }
        format.json { render json: @horticulture_plant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /horticulture_plants/1
  # PATCH/PUT /horticulture_plants/1.json
  def update
    respond_to do |format|
      if @horticulture_plant.update(horticulture_plant_params)
        format.html { redirect_to @horticulture_plant, notice: 'Horticulture plant was successfully updated.' }
        format.json { render :show, status: :ok, location: @horticulture_plant }
      else
        format.html { render :edit }
        format.json { render json: @horticulture_plant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /horticulture_plants/1
  # DELETE /horticulture_plants/1.json
  def destroy
    @horticulture_plant.destroy
    respond_to do |format|
      format.html { redirect_to horticulture_plants_url, notice: 'Horticulture plant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_horticulture_plant
      @horticulture_plant = HorticulturePlant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def horticulture_plant_params
      params.require(:horticulture_plant).permit(:name)
    end
end
