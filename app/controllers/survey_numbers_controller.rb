class SurveyNumbersController < ApplicationController
  before_action :set_survey_number, only: [:show, :edit, :update, :destroy]

  # GET /survey_numbers
  # GET /survey_numbers.json
  def index
    @survey_numbers = params[:farmer_id].present? ? SurveyNumber.where(farmer_id: params[:farmer_id]) : SurveyNumber.all
    render :layout => false
  end

  # GET /survey_numbers/1
  # GET /survey_numbers/1.json
  def show
  end

  # GET /survey_numbers/new
  def new
    @survey_number = SurveyNumber.new
  end

  # GET /survey_numbers/1/edit
  def edit
  end

  # POST /survey_numbers
  # POST /survey_numbers.json
  def create
    @survey_number = SurveyNumber.new(survey_number_params)

    respond_to do |format|
      if @survey_number.save
        format.html { redirect_to @survey_number, notice: 'Survey number was successfully created.' }
        format.json { render :show, status: :created, location: @survey_number }
      else
        format.html { render :new }
        format.json { render json: @survey_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /survey_numbers/1
  # PATCH/PUT /survey_numbers/1.json
  def update
    respond_to do |format|
      if @survey_number.update(survey_number_params)
        format.html { redirect_to @survey_number, notice: 'Survey number was successfully updated.' }
        format.json { render :show, status: :ok, location: @survey_number }
      else
        format.html { render :edit }
        format.json { render json: @survey_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /survey_numbers/1
  # DELETE /survey_numbers/1.json
  def destroy
    @survey_number.destroy
    respond_to do |format|
      format.html { redirect_to survey_numbers_url, notice: 'Survey number was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survey_number
      @survey_number = SurveyNumber.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def survey_number_params
      params.require(:survey_number).permit(:number, :hissa_number, :grid1, :grid2, :grid3, :grid4, :farmer_id)
    end
end
