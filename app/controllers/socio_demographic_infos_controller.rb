class SocioDemographicInfosController < ApplicationController
  before_action :set_socio_demographic_info, only: [:show, :edit, :update, :destroy]

  # GET /socio_demographic_infos
  # GET /socio_demographic_infos.json
  def index
    @socio_demographic_infos = SocioDemographicInfo.where(farmer_id: params[:farmer_id]) unless params[:farmer_id].blank?
    @socio_demographic_infos = @socio_demographic_infos || SocioDemographicInfo.all
    #@socio_demographic_info = SocioDemographicInfo.new
    render :layout => false 
  end

  # GET /socio_demographic_infos/1
  # GET /socio_demographic_infos/1.json
  def show
  end

  # GET /socio_demographic_infos/new
  def new
    @socio_demographic_info = SocioDemographicInfo.new
  end

  # GET /socio_demographic_infos/1/edit
  def edit
  end

  # POST /socio_demographic_infos
  # POST /socio_demographic_infos.json
  def create

    @socio_demographic_info = SocioDemographicInfo.new(socio_demographic_info_params)

    respond_to do |format|
      if @socio_demographic_info.save
        format.html { redirect_to @socio_demographic_info, notice: 'Socio demographic info was successfully created.' }
        format.json { render :show, status: :created, location: @socio_demographic_info }
      else
        format.html { render :new }
        format.json { render json: @socio_demographic_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /socio_demographic_infos/1
  # PATCH/PUT /socio_demographic_infos/1.json
  def update
    respond_to do |format|
      if @socio_demographic_info.update(socio_demographic_info_params)
        format.html { redirect_to @socio_demographic_info, notice: 'Socio demographic info was successfully updated.' }
        format.json { render :show, status: :ok, location: @socio_demographic_info }
      else
        format.html { render :edit }
        format.json { render json: @socio_demographic_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /socio_demographic_infos/1
  # DELETE /socio_demographic_infos/1.json
  def destroy
    @socio_demographic_info.destroy
    respond_to do |format|
      format.html { redirect_to socio_demographic_infos_url, notice: 'Socio demographic info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_socio_demographic_info
      @socio_demographic_info = SocioDemographicInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def socio_demographic_info_params
      params.require(:socio_demographic_info).permit(:farmer_id, :occupation_id,
       :education_id, :age, :gender_id, :local_organisation_member, :farmer_member_name, :local_organisation_id)
    end
end
