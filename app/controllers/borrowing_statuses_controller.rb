class BorrowingStatusesController < ApplicationController
  before_action :set_borrowing_status, only: [:show, :edit, :update, :destroy]

  # GET /borrowing_statuses
  # GET /borrowing_statuses.json
  def index
    @borrowing_statuses = params[:farmer_id].present? ? BorrowingStatus.where(farmer_id: params[:farmer_id]) : BorrowingStatus.all
    render layout: false
  end

  # GET /borrowing_statuses/1
  # GET /borrowing_statuses/1.json
  def show
  end

  # GET /borrowing_statuses/new
  def new
    @borrowing_status = BorrowingStatus.new
  end

  # GET /borrowing_statuses/1/edit
  def edit
  end

  # POST /borrowing_statuses
  # POST /borrowing_statuses.json
  def create
    BorrowingStatus.where(farmer_id: params[:farmer_id]).destroy_all
    @borrowing_status = BorrowingStatus.new(borrowing_status_params)

    respond_to do |format|
      if @borrowing_status.save
        format.html { redirect_to @borrowing_status, notice: 'Borrowing status was successfully created.' }
        format.json { render :show, status: :created, location: @borrowing_status }
      else
        format.html { render :new }
        format.json { render json: @borrowing_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /borrowing_statuses/1
  # PATCH/PUT /borrowing_statuses/1.json
  def update
    respond_to do |format|
      if @borrowing_status.update(borrowing_status_params)
        format.html { redirect_to @borrowing_status, notice: 'Borrowing status was successfully updated.' }
        format.json { render :show, status: :ok, location: @borrowing_status }
      else
        format.html { render :edit }
        format.json { render json: @borrowing_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /borrowing_statuses/1
  # DELETE /borrowing_statuses/1.json
  def destroy
    @borrowing_status.destroy
    respond_to do |format|
      format.html { redirect_to borrowing_statuses_url, notice: 'Borrowing status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_borrowing_status
      @borrowing_status = BorrowingStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def borrowing_status_params
      params.require(:borrowing_status).permit(:farmer_id, :have_bank_account, :have_savings, :credit_availed, :amount, :amount_availed_adequate)
    end
end
