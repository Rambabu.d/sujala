class PlantCountsController < ApplicationController
  before_action :set_plant_count, only: [:show, :edit, :update, :destroy]

  # GET /plant_counts
  # GET /plant_counts.json
  def index
    @plant_counts = params[:farmer_id].present? ? PlantCount.where(farmer_id: params[:farmer_id]) : PlantCount.all
    render layout: false
  end

  # GET /plant_counts/1
  # GET /plant_counts/1.json
  def show
  end

  # GET /plant_counts/new
  def new
    @plant_count = PlantCount.new
  end

  # GET /plant_counts/1/edit
  def edit
  end

  # POST /plant_counts
  # POST /plant_counts.json
  def create
    @plant_count = PlantCount.new(plant_count_params)

    respond_to do |format|
      if @plant_count.save
        format.html { redirect_to @plant_count, notice: 'Plant count was successfully created.' }
        format.json { render :show, status: :created, location: @plant_count }
      else
        format.html { render :new }
        format.json { render json: @plant_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plant_counts/1
  # PATCH/PUT /plant_counts/1.json
  def update
    respond_to do |format|
      if @plant_count.update(plant_count_params)
        format.html { redirect_to @plant_count, notice: 'Plant count was successfully updated.' }
        format.json { render :show, status: :ok, location: @plant_count }
      else
        format.html { render :edit }
        format.json { render json: @plant_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plant_counts/1
  # DELETE /plant_counts/1.json
  def destroy
    @plant_count.destroy
    respond_to do |format|
      format.html { redirect_to plant_counts_url, notice: 'Plant count was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plant_count
      @plant_count = PlantCount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plant_count_params
      params.require(:plant_count).permit(:farmer_id, :horticulture_plant_id, :field, :backyard)
    end
end
