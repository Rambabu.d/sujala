class AdditionalInvestmentCapacitiesController < ApplicationController
  before_action :set_additional_investment_capacity, only: [:show, :edit, :update, :destroy]

  # GET /additional_investment_capacities
  # GET /additional_investment_capacities.json
  def index
    @additional_investment_capacities = params[:farmer_id].present? ? AdditionalInvestmentCapacity.where(farmer_id: params[:farmer_id]) : AdditionalInvestmentCapacity.all
    render layout: false
  end

  # GET /additional_investment_capacities/1
  # GET /additional_investment_capacities/1.json
  def show
  end

  # GET /additional_investment_capacities/new
  def new
    @additional_investment_capacity = AdditionalInvestmentCapacity.new
  end

  # GET /additional_investment_capacities/1/edit
  def edit
  end

  # POST /additional_investment_capacities
  # POST /additional_investment_capacities.json
  def create
    @additional_investment_capacity = AdditionalInvestmentCapacity.new(additional_investment_capacity_params)

    respond_to do |format|
      if @additional_investment_capacity.save
        format.html { redirect_to @additional_investment_capacity, notice: 'Additional investment capacity was successfully created.' }
        format.json { render :show, status: :created, location: @additional_investment_capacity }
      else
        format.html { render :new }
        format.json { render json: @additional_investment_capacity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /additional_investment_capacities/1
  # PATCH/PUT /additional_investment_capacities/1.json
  def update
    respond_to do |format|
      if @additional_investment_capacity.update(additional_investment_capacity_params)
        format.html { redirect_to @additional_investment_capacity, notice: 'Additional investment capacity was successfully updated.' }
        format.json { render :show, status: :ok, location: @additional_investment_capacity }
      else
        format.html { render :edit }
        format.json { render json: @additional_investment_capacity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /additional_investment_capacities/1
  # DELETE /additional_investment_capacities/1.json
  def destroy
    @additional_investment_capacity.destroy
    respond_to do |format|
      format.html { redirect_to additional_investment_capacities_url, notice: 'Additional investment capacity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_additional_investment_capacity
      @additional_investment_capacity = AdditionalInvestmentCapacity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def additional_investment_capacity_params
      params.require(:additional_investment_capacity).permit(:farmer_id, :purpose_id, :extent_of_investment, :source_of_funds)
    end
end
