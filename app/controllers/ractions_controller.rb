class RactionsController < ApplicationController
  before_action :set_raction, only: [:show, :edit, :update, :destroy]

  # GET /ractions
  # GET /ractions.json
  def index
    @ractions = Raction.all
  end

  # GET /ractions/1
  # GET /ractions/1.json
  def show
  end

  # GET /ractions/new
  def new
    @raction = Raction.new
  end

  # GET /ractions/1/edit
  def edit
  end

  # POST /ractions
  # POST /ractions.json
  def create
    @raction = Raction.new(raction_params)

    respond_to do |format|
      if @raction.save
        format.html { redirect_to @raction, notice: 'Raction was successfully created.' }
        format.json { render :show, status: :created, location: @raction }
      else
        format.html { render :new }
        format.json { render json: @raction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ractions/1
  # PATCH/PUT /ractions/1.json
  def update
    respond_to do |format|
      if @raction.update(raction_params)
        format.html { redirect_to @raction, notice: 'Raction was successfully updated.' }
        format.json { render :show, status: :ok, location: @raction }
      else
        format.html { render :edit }
        format.json { render json: @raction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ractions/1
  # DELETE /ractions/1.json
  def destroy
    @raction.destroy
    respond_to do |format|
      format.html { redirect_to ractions_url, notice: 'Raction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_raction
      @raction = Raction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def raction_params
      params.require(:raction).permit(:name)
    end
end
