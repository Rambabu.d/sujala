class FodderAvailabilitiesController < ApplicationController
  before_action :set_fodder_availability, only: [:show, :edit, :update, :destroy]

  # GET /fodder_availabilities
  # GET /fodder_availabilities.json
  def index
    @fodder_availabilities = params[:farmer_id].present? ? FodderAvailability.where(farmer_id: params[:farmer_id]) : FodderAvailability.all
    render layout: false
  end

  # GET /fodder_availabilities/1
  # GET /fodder_availabilities/1.json
  def show
  end

  # GET /fodder_availabilities/new
  def new
    @fodder_availability = FodderAvailability.new
  end

  # GET /fodder_availabilities/1/edit
  def edit
  end

  # POST /fodder_availabilities
  # POST /fodder_availabilities.json
  def create
    @fodder_availability = FodderAvailability.new(fodder_availability_params)

    respond_to do |format|
      if @fodder_availability.save
        format.html { redirect_to @fodder_availability, notice: 'Fodder availability was successfully created.' }
        format.json { render :show, status: :created, location: @fodder_availability }
      else
        format.html { render :new }
        format.json { render json: @fodder_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fodder_availabilities/1
  # PATCH/PUT /fodder_availabilities/1.json
  def update
    respond_to do |format|
      if @fodder_availability.update(fodder_availability_params)
        format.html { redirect_to @fodder_availability, notice: 'Fodder availability was successfully updated.' }
        format.json { render :show, status: :ok, location: @fodder_availability }
      else
        format.html { render :edit }
        format.json { render json: @fodder_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fodder_availabilities/1
  # DELETE /fodder_availabilities/1.json
  def destroy
    @fodder_availability.destroy
    respond_to do |format|
      format.html { redirect_to fodder_availabilities_url, notice: 'Fodder availability was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fodder_availability
      @fodder_availability = FodderAvailability.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fodder_availability_params
      params.require(:fodder_availability).permit(:farmer_id, :fodder_type, :quantity, :duration, :adequacy)
    end
end
