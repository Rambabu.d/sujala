class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::MimeResponds
  protect_from_forgery with: :null_session
  #protect_from_forgery with: :exception

  rescue_from ActionController::RoutingError do |exception|
    logger.error 'Routing error occurred'
    redirect_to root_path
  end

  helper_method :null_zero

  def null_zero(value)
    value.blank? ? 0 : value
  end
  
end
