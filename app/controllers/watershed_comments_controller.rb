class WatershedCommentsController < ApplicationController
  before_action :set_watershed_comment, only: [:show, :edit, :update, :destroy]

  # GET /watershed_comments
  # GET /watershed_comments.json
  def index
    @watershed_comments = params[:farmer_id].present? ? WatershedComment.where(farmer_id: params[:farmer_id]) : WatershedComment.all
    render layout: false
  end

  # GET /watershed_comments/1
  # GET /watershed_comments/1.json
  def show
  end

  # GET /watershed_comments/new
  def new
    @watershed_comment = WatershedComment.new
  end

  # GET /watershed_comments/1/edit
  def edit
  end

  # POST /watershed_comments
  # POST /watershed_comments.json
  def create
    @watershed_comment = WatershedComment.new(watershed_comment_params)

    respond_to do |format|
      if @watershed_comment.save
        format.html { redirect_to @watershed_comment, notice: 'Watershed comment was successfully created.' }
        format.json { render :show, status: :created, location: @watershed_comment }
      else
        format.html { render :new }
        format.json { render json: @watershed_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /watershed_comments/1
  # PATCH/PUT /watershed_comments/1.json
  def update
    respond_to do |format|
      if @watershed_comment.update(watershed_comment_params)
        format.html { redirect_to @watershed_comment, notice: 'Watershed comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @watershed_comment }
      else
        format.html { render :edit }
        format.json { render json: @watershed_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /watershed_comments/1
  # DELETE /watershed_comments/1.json
  def destroy
    @watershed_comment.destroy
    respond_to do |format|
      format.html { redirect_to watershed_comments_url, notice: 'Watershed comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watershed_comment
      @watershed_comment = WatershedComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watershed_comment_params
      params.require(:watershed_comment).permit(:farmer_id, :comments)
    end
end
