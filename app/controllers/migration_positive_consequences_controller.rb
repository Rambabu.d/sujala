class MigrationPositiveConsequencesController < ApplicationController
  before_action :set_migration_positive_consequence, only: [:show, :edit, :update, :destroy]

  # GET /migration_positive_consequences
  # GET /migration_positive_consequences.json
  def index
    @migration_positive_consequences = MigrationPositiveConsequence.all
    render layout: false
  end

  # GET /migration_positive_consequences/1
  # GET /migration_positive_consequences/1.json
  def show
  end

  # GET /migration_positive_consequences/new
  def new
    @migration_positive_consequence = MigrationPositiveConsequence.new
  end

  # GET /migration_positive_consequences/1/edit
  def edit
  end

  # POST /migration_positive_consequences
  # POST /migration_positive_consequences.json
  def create
    @migration_positive_consequence = MigrationPositiveConsequence.new(migration_positive_consequence_params)

    respond_to do |format|
      if @migration_positive_consequence.save
        format.html { redirect_to @migration_positive_consequence, notice: 'Migration positive consequence was successfully created.' }
        format.json { render :show, status: :created, location: @migration_positive_consequence }
      else
        format.html { render :new }
        format.json { render json: @migration_positive_consequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /migration_positive_consequences/1
  # PATCH/PUT /migration_positive_consequences/1.json
  def update
    respond_to do |format|
      if @migration_positive_consequence.update(migration_positive_consequence_params)
        format.html { redirect_to @migration_positive_consequence, notice: 'Migration positive consequence was successfully updated.' }
        format.json { render :show, status: :ok, location: @migration_positive_consequence }
      else
        format.html { render :edit }
        format.json { render json: @migration_positive_consequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /migration_positive_consequences/1
  # DELETE /migration_positive_consequences/1.json
  def destroy
    @migration_positive_consequence.destroy
    respond_to do |format|
      format.html { redirect_to migration_positive_consequences_url, notice: 'Migration positive consequence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_migration_positive_consequence
      @migration_positive_consequence = MigrationPositiveConsequence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def migration_positive_consequence_params
      params.require(:migration_positive_consequence).permit(:name)
    end
end
