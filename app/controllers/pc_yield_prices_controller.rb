class PcYieldPricesController < ApplicationController
  before_action :set_pc_yield_price, only: [:show, :edit, :update, :destroy]

  # GET /pc_yield_prices
  # GET /pc_yield_prices.json
  def index
    if params[:crop_list_id].present?
      @pc_yield_prices = [ProductionCost.where(farmer_id: params[:farmer_id], crop_list_id: params[:crop_list_id]).
        first.pc_yield_prices.last]
    else
      @pc_yield_prices = params[:farmer_id].present? ? PcYieldPrice.where(farmer_id: params[:farmer_id]) : PcYieldPrice.all
    end
    render layout: false
  end

  # GET /pc_yield_prices/1
  # GET /pc_yield_prices/1.json
  def show
  end

  # GET /pc_yield_prices/new
  def new
    @pc_yield_price = PcYieldPrice.new
  end

  # GET /pc_yield_prices/1/edit
  def edit
  end

  # POST /pc_yield_prices
  # POST /pc_yield_prices.json
  def create
    @pc_yield_price = PcYieldPrice.new(pc_yield_price_params)

    respond_to do |format|
      if @pc_yield_price.save
        format.html { redirect_to @pc_yield_price, notice: 'Pc yield price was successfully created.' }
        format.json { render :show, status: :created, location: @pc_yield_price }
      else
        format.html { render :new }
        format.json { render json: @pc_yield_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pc_yield_prices/1
  # PATCH/PUT /pc_yield_prices/1.json
  def update
    respond_to do |format|
      if @pc_yield_price.update(pc_yield_price_params)
        format.html { redirect_to @pc_yield_price, notice: 'Pc yield price was successfully updated.' }
        format.json { render :show, status: :ok, location: @pc_yield_price }
      else
        format.html { render :edit }
        format.json { render json: @pc_yield_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pc_yield_prices/1
  # DELETE /pc_yield_prices/1.json
  def destroy
    @pc_yield_price.destroy
    respond_to do |format|
      format.html { redirect_to pc_yield_prices_url, notice: 'Pc yield price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pc_yield_price
      @pc_yield_price = PcYieldPrice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pc_yield_price_params
      params.require(:pc_yield_price).permit(:value, :production_cost_id, :farmer_id, :user_id, :crop_main_product_yield, :crop_main_product_price, :crop_by_product_yield, :crop_by_product_price, :intercrop_main_product_yield, :intercrop_main_product_price, :intercrop_by_product_yield, :intercrop_by_product_price)
    end
end
