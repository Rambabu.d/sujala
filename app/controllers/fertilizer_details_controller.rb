class FertilizerDetailsController < ApplicationController
  before_action :set_fertilizer_detail, only: [:show, :edit, :update, :destroy]

  # GET /fertilizer_details
  # GET /fertilizer_details.json
  def index
    @fertilizer_details = params[:farmer_id].present? ? FertilizerDetail.where(farmer_id: params[:farmer_id]) : FertilizerDetail.all
    render layout: false
  end

  # GET /fertilizer_details/1
  # GET /fertilizer_details/1.json
  def show
  end

  # GET /fertilizer_details/new
  def new
    @fertilizer_detail = FertilizerDetail.new
  end

  # GET /fertilizer_details/1/edit
  def edit
  end

  # POST /fertilizer_details
  # POST /fertilizer_details.json
  def create
    @fertilizer_detail = FertilizerDetail.new(fertilizer_detail_params)

    respond_to do |format|
      if @fertilizer_detail.save
        format.html { redirect_to @fertilizer_detail, notice: 'Fertilizer detail was successfully created.' }
        format.json { render :show, status: :created, location: @fertilizer_detail }
      else
        format.html { render :new }
        format.json { render json: @fertilizer_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fertilizer_details/1
  # PATCH/PUT /fertilizer_details/1.json
  def update
    respond_to do |format|
      if @fertilizer_detail.update(fertilizer_detail_params)
        format.html { redirect_to @fertilizer_detail, notice: 'Fertilizer detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @fertilizer_detail }
      else
        format.html { render :edit }
        format.json { render json: @fertilizer_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fertilizer_details/1
  # DELETE /fertilizer_details/1.json
  def destroy
    @fertilizer_detail.destroy
    respond_to do |format|
      format.html { redirect_to fertilizer_details_url, notice: 'Fertilizer detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fertilizer_detail
      @fertilizer_detail = FertilizerDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fertilizer_detail_params
      params.require(:fertilizer_detail).permit(:no_of_times, :production_cost_id, :farmer_id, :user_id,:fertilizer_id, :own_men_mandays, :hired_men_mandays, :own_women_mandays, :hired_women_mandays, :own_bullock_days, :hired_bullock_days, :own_tractor_days, :hired_tractor_days, :input, :amount)
    end
end
