class InstitutionalCreditsController < ApplicationController
  before_action :set_institutional_credit, only: [:show, :edit, :update, :destroy]

  # GET /institutional_credits
  # GET /institutional_credits.json
  def index
    @institutional_credits = params[:farmer_id].present? ? InstitutionalCredit.where(farmer_id: params[:farmer_id]) : InstitutionalCredit.all
    render layout: false
  end

  # GET /institutional_credits/1
  # GET /institutional_credits/1.json
  def show
  end

  # GET /institutional_credits/new
  def new
    @institutional_credit = InstitutionalCredit.new
  end

  # GET /institutional_credits/1/edit
  def edit
  end

  # POST /institutional_credits
  # POST /institutional_credits.json
  def create
    @institutional_credit = InstitutionalCredit.new(institutional_credit_params)

    respond_to do |format|
      if @institutional_credit.save
        format.html { redirect_to @institutional_credit, notice: 'Institutional credit was successfully created.' }
        format.json { render :show, status: :created, location: @institutional_credit }
      else
        format.html { render :new }
        format.json { render json: @institutional_credit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /institutional_credits/1
  # PATCH/PUT /institutional_credits/1.json
  def update
    respond_to do |format|
      if @institutional_credit.update(institutional_credit_params)
        format.html { redirect_to @institutional_credit, notice: 'Institutional credit was successfully updated.' }
        format.json { render :show, status: :ok, location: @institutional_credit }
      else
        format.html { render :edit }
        format.json { render json: @institutional_credit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /institutional_credits/1
  # DELETE /institutional_credits/1.json
  def destroy
    @institutional_credit.destroy
    respond_to do |format|
      format.html { redirect_to institutional_credits_url, notice: 'Institutional credit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_institutional_credit
      @institutional_credit = InstitutionalCredit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def institutional_credit_params
      params.require(:institutional_credit).permit(:farmer_id, :source, :institution_amount, :rate_of_interest, :credit_avail_purpose_id, :repayment_status, :reactions)
    end
end
