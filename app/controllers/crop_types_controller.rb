class CropTypesController < ApplicationController
  before_action :set_crop_type, only: [:show, :edit, :update, :destroy]

  # GET /crop_types
  # GET /crop_types.json
  def index
    @crop_types = CropType.all
  end

  # GET /crop_types/1
  # GET /crop_types/1.json
  def show
  end

  # GET /crop_types/new
  def new
    @crop_type = CropType.new
  end

  # GET /crop_types/1/edit
  def edit
  end

  # POST /crop_types
  # POST /crop_types.json
  def create
    @crop_type = CropType.new(crop_type_params)

    respond_to do |format|
      if @crop_type.save
        format.html { redirect_to @crop_type, notice: 'Crop type was successfully created.' }
        format.json { render :show, status: :created, location: @crop_type }
      else
        format.html { render :new }
        format.json { render json: @crop_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /crop_types/1
  # PATCH/PUT /crop_types/1.json
  def update
    respond_to do |format|
      if @crop_type.update(crop_type_params)
        format.html { redirect_to @crop_type, notice: 'Crop type was successfully updated.' }
        format.json { render :show, status: :ok, location: @crop_type }
      else
        format.html { render :edit }
        format.json { render json: @crop_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /crop_types/1
  # DELETE /crop_types/1.json
  def destroy
    @crop_type.destroy
    respond_to do |format|
      format.html { redirect_to crop_types_url, notice: 'Crop type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_crop_type
      @crop_type = CropType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def crop_type_params
      params.require(:crop_type).permit(:name)
    end
end
