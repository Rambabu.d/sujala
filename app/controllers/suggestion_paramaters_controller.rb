class SuggestionParamatersController < ApplicationController
  before_action :set_suggestion_paramater, only: [:show, :edit, :update, :destroy]

  # GET /suggestion_paramaters
  # GET /suggestion_paramaters.json
  def index
    @suggestion_paramaters = SuggestionParamater.all
    render layout: false
  end

  # GET /suggestion_paramaters/1
  # GET /suggestion_paramaters/1.json
  def show
  end

  # GET /suggestion_paramaters/new
  def new
    @suggestion_paramater = SuggestionParamater.new
  end

  # GET /suggestion_paramaters/1/edit
  def edit
  end

  # POST /suggestion_paramaters
  # POST /suggestion_paramaters.json
  def create
    @suggestion_paramater = SuggestionParamater.new(suggestion_paramater_params)

    respond_to do |format|
      if @suggestion_paramater.save
        format.html { redirect_to @suggestion_paramater, notice: 'Suggestion paramater was successfully created.' }
        format.json { render :show, status: :created, location: @suggestion_paramater }
      else
        format.html { render :new }
        format.json { render json: @suggestion_paramater.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /suggestion_paramaters/1
  # PATCH/PUT /suggestion_paramaters/1.json
  def update
    respond_to do |format|
      if @suggestion_paramater.update(suggestion_paramater_params)
        format.html { redirect_to @suggestion_paramater, notice: 'Suggestion paramater was successfully updated.' }
        format.json { render :show, status: :ok, location: @suggestion_paramater }
      else
        format.html { render :edit }
        format.json { render json: @suggestion_paramater.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suggestion_paramaters/1
  # DELETE /suggestion_paramaters/1.json
  def destroy
    @suggestion_paramater.destroy
    respond_to do |format|
      format.html { redirect_to suggestion_paramaters_url, notice: 'Suggestion paramater was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suggestion_paramater
      @suggestion_paramater = SuggestionParamater.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def suggestion_paramater_params
      params.require(:suggestion_paramater).permit(:name)
    end
end
