class SmusController < ApplicationController
  before_action :set_smu, only: [:show, :edit, :update, :destroy]

  # GET /smus
  # GET /smus.json
  def index
    @smus = Smu.all
    render layout: false
  end

  # GET /smus/1
  # GET /smus/1.json
  def show
  end

  # GET /smus/new
  def new
    @smu = Smu.new
  end

  # GET /smus/1/edit
  def edit
  end

  # POST /smus
  # POST /smus.json
  def create
    @smu = Smu.new(smu_params)

    respond_to do |format|
      if @smu.save
        format.html { redirect_to @smu, notice: 'Smu was successfully created.' }
        format.json { render :show, status: :created, location: @smu }
      else
        format.html { render :new }
        format.json { render json: @smu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /smus/1
  # PATCH/PUT /smus/1.json
  def update
    respond_to do |format|
      if @smu.update(smu_params)
        format.html { redirect_to @smu, notice: 'Smu was successfully updated.' }
        format.json { render :show, status: :ok, location: @smu }
      else
        format.html { render :edit }
        format.json { render json: @smu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /smus/1
  # DELETE /smus/1.json
  def destroy
    @smu.destroy
    respond_to do |format|
      format.html { redirect_to smus_url, notice: 'Smu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_smu
      @smu = Smu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def smu_params
      params.require(:smu).permit(:name, :is_active)
    end
end
