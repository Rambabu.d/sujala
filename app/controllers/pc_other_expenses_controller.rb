class PcOtherExpensesController < ApplicationController
  before_action :set_pc_other_expense, only: [:show, :edit, :update, :destroy]

  # GET /pc_other_expenses
  # GET /pc_other_expenses.json
  def index
    @pc_other_expenses = params[:farmer_id].present? ? PcOtherExpense.where(farmer_id: params[:farmer_id]) : PcOtherExpense.all
    render layout: false
  end

  # GET /pc_other_expenses/1
  # GET /pc_other_expenses/1.json
  def show
  end

  # GET /pc_other_expenses/new
  def new
    @pc_other_expense = PcOtherExpense.new
  end

  # GET /pc_other_expenses/1/edit
  def edit
  end

  # POST /pc_other_expenses
  # POST /pc_other_expenses.json
  def create
    @pc_other_expense = PcOtherExpense.new(pc_other_expense_params)

    respond_to do |format|
      if @pc_other_expense.save
        format.html { redirect_to @pc_other_expense, notice: 'Pc other expense was successfully created.' }
        format.json { render :show, status: :created, location: @pc_other_expense }
      else
        format.html { render :new }
        format.json { render json: @pc_other_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pc_other_expenses/1
  # PATCH/PUT /pc_other_expenses/1.json
  def update
    respond_to do |format|
      if @pc_other_expense.update(pc_other_expense_params)
        format.html { redirect_to @pc_other_expense, notice: 'Pc other expense was successfully updated.' }
        format.json { render :show, status: :ok, location: @pc_other_expense }
      else
        format.html { render :edit }
        format.json { render json: @pc_other_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pc_other_expenses/1
  # DELETE /pc_other_expenses/1.json
  def destroy
    @pc_other_expense.destroy
    respond_to do |format|
      format.html { redirect_to pc_other_expenses_url, notice: 'Pc other expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pc_other_expense
      @pc_other_expense = PcOtherExpense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pc_other_expense_params
      params.require(:pc_other_expense).permit(:seed_price_main, :seed_price_inter, :production_cost_id, :farmer_id, :user_id, :electricity, :insurance, :irrigated_rental_value, :dryland_rental_value, :managerial_cost, :land_revenue, :interest_on_variable_cost, :interest_on_fixed_cost, :miscellaneous, :repairs_and_maintenance_charges)
    end
end
