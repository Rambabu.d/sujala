class IrrigationsController < ApplicationController
  before_action :set_irrigation, only: [:show, :edit, :update, :destroy]

  # GET /irrigations
  # GET /irrigations.json
  def index
    @irrigations = params[:farmer_id].present? ? Irrigation.where(farmer_id: params[:farmer_id]) : Irrigation.all
    render layout: false
  end

  # GET /irrigations/1
  # GET /irrigations/1.json
  def show
  end

  # GET /irrigations/new
  def new
    @irrigation = Irrigation.new
  end

  # GET /irrigations/1/edit
  def edit
  end

  # POST /irrigations
  # POST /irrigations.json
  def create
    @irrigation = Irrigation.new(irrigation_params)

    respond_to do |format|
      if @irrigation.save
        format.html { redirect_to @irrigation, notice: 'Irrigation was successfully created.' }
        format.json { render :show, status: :created, location: @irrigation }
      else
        format.html { render :new }
        format.json { render json: @irrigation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /irrigations/1
  # PATCH/PUT /irrigations/1.json
  def update
    respond_to do |format|
      if @irrigation.update(irrigation_params)
        format.html { redirect_to @irrigation, notice: 'Irrigation was successfully updated.' }
        format.json { render :show, status: :ok, location: @irrigation }
      else
        format.html { render :edit }
        format.json { render json: @irrigation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /irrigations/1
  # DELETE /irrigations/1.json
  def destroy
    @irrigation.destroy
    respond_to do |format|
      format.html { redirect_to irrigations_url, notice: 'Irrigation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_irrigation
      @irrigation = Irrigation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def irrigation_params
      params.require(:irrigation).permit(:farmer_id, :source_of_irrigation, :defunct, :working, :water_depth, :yield, :kharif, :rabi, :summer, :perennial_cops, :water_quality)
    end
end
