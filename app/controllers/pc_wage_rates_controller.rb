class PcWageRatesController < ApplicationController
  before_action :set_pc_wage_rate, only: [:show, :edit, :update, :destroy]

  # GET /pc_wage_rates
  # GET /pc_wage_rates.json
  def index
    @pc_wage_rates = params[:farmer_id].present? ? PcWageRate.where(farmer_id: params[:farmer_id]) : PcWageRate.all
    render layout: false
  end

  # GET /pc_wage_rates/1
  # GET /pc_wage_rates/1.json
  def show
  end

  # GET /pc_wage_rates/new
  def new
    @pc_wage_rate = PcWageRate.new
  end

  # GET /pc_wage_rates/1/edit
  def edit
  end

  # POST /pc_wage_rates
  # POST /pc_wage_rates.json
  def create
    @pc_wage_rate = PcWageRate.new(pc_wage_rate_params)

    respond_to do |format|
      if @pc_wage_rate.save
        format.html { redirect_to @pc_wage_rate, notice: 'Pc wage rate was successfully created.' }
        format.json { render :show, status: :created, location: @pc_wage_rate }
      else
        format.html { render :new }
        format.json { render json: @pc_wage_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pc_wage_rates/1
  # PATCH/PUT /pc_wage_rates/1.json
  def update
    respond_to do |format|
      if @pc_wage_rate.update(pc_wage_rate_params)
        format.html { redirect_to @pc_wage_rate, notice: 'Pc wage rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @pc_wage_rate }
      else
        format.html { render :edit }
        format.json { render json: @pc_wage_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pc_wage_rates/1
  # DELETE /pc_wage_rates/1.json
  def destroy
    @pc_wage_rate.destroy
    respond_to do |format|
      format.html { redirect_to pc_wage_rates_url, notice: 'Pc wage rate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pc_wage_rate
      @pc_wage_rate = PcWageRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pc_wage_rate_params
      params.require(:pc_wage_rate).permit(:irrigation_charge, :male, :female, :bullock_pair, :tractor, :machinery, :production_cost_id, :farmer_id, :user_id)
    end
end
