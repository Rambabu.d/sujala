class FoodMaterialsController < ApplicationController
  before_action :set_food_material, only: [:show, :edit, :update, :destroy]

  # GET /food_materials
  # GET /food_materials.json
  def index
    @food_materials = FoodMaterial.all
    render layout: false
  end

  # GET /food_materials/1
  # GET /food_materials/1.json
  def show
  end

  # GET /food_materials/new
  def new
    @food_material = FoodMaterial.new
  end

  # GET /food_materials/1/edit
  def edit
  end

  # POST /food_materials
  # POST /food_materials.json
  def create
    @food_material = FoodMaterial.new(food_material_params)

    respond_to do |format|
      if @food_material.save
        format.html { redirect_to @food_material, notice: 'Food material was successfully created.' }
        format.json { render :show, status: :created, location: @food_material }
      else
        format.html { render :new }
        format.json { render json: @food_material.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /food_materials/1
  # PATCH/PUT /food_materials/1.json
  def update
    respond_to do |format|
      if @food_material.update(food_material_params)
        format.html { redirect_to @food_material, notice: 'Food material was successfully updated.' }
        format.json { render :show, status: :ok, location: @food_material }
      else
        format.html { render :edit }
        format.json { render json: @food_material.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /food_materials/1
  # DELETE /food_materials/1.json
  def destroy
    @food_material.destroy
    respond_to do |format|
      format.html { redirect_to food_materials_url, notice: 'Food material was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_food_material
      @food_material = FoodMaterial.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def food_material_params
      params.require(:food_material).permit(:name)
    end
end
