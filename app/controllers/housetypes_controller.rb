class HousetypesController < ApplicationController
  before_action :set_housetype, only: [:show, :edit, :update, :destroy]

  # GET /housetypes
  # GET /housetypes.json
  def index
    @housetypes = Housetype.all
  end

  # GET /housetypes/1
  # GET /housetypes/1.json
  def show
  end

  # GET /housetypes/new
  def new
    @housetype = Housetype.new
  end

  # GET /housetypes/1/edit
  def edit
  end

  # POST /housetypes
  # POST /housetypes.json
  def create
    @housetype = Housetype.new(housetype_params)

    respond_to do |format|
      if @housetype.save
        format.html { redirect_to @housetype, notice: 'Housetype was successfully created.' }
        format.json { render :show, status: :created, location: @housetype }
      else
        format.html { render :new }
        format.json { render json: @housetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /housetypes/1
  # PATCH/PUT /housetypes/1.json
  def update
    respond_to do |format|
      if @housetype.update(housetype_params)
        format.html { redirect_to @housetype, notice: 'Housetype was successfully updated.' }
        format.json { render :show, status: :ok, location: @housetype }
      else
        format.html { render :edit }
        format.json { render json: @housetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /housetypes/1
  # DELETE /housetypes/1.json
  def destroy
    @housetype.destroy
    respond_to do |format|
      format.html { redirect_to housetypes_url, notice: 'Housetype was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_housetype
      @housetype = Housetype.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def housetype_params
      params.require(:housetype).permit(:name, :is_active)
    end
end
