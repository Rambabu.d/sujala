class BasicNeedDetailsController < ApplicationController
  before_action :set_basic_need_detail, only: [:show, :edit, :update, :destroy]

  # GET /basic_need_details
  # GET /basic_need_details.json
  def index
    @basic_need_details = params[:farmer_id].present? ? BasicNeedDetail.where(farmer_id: params[:farmer_id]) : BasicNeedDetail.all
    render layout: false
  end

  # GET /basic_need_details/1
  # GET /basic_need_details/1.json
  def show
  end

  # GET /basic_need_details/new
  def new
    @basic_need_detail = BasicNeedDetail.new
  end

  # GET /basic_need_details/1/edit
  def edit
  end

  # POST /basic_need_details
  # POST /basic_need_details.json
  def create
    BasicNeedDetail.where(farmer_id: params[:farmer_id]).destroy_all
    @basic_need_detail = BasicNeedDetail.new(basic_need_detail_params)

    respond_to do |format|
      if @basic_need_detail.save
        format.html { redirect_to @basic_need_detail, notice: 'Basic need detail was successfully created.' }
        format.json { render :show, status: :created, location: @basic_need_detail }
      else
        format.html { render :new }
        format.json { render json: @basic_need_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /basic_need_details/1
  # PATCH/PUT /basic_need_details/1.json
  def update
    respond_to do |format|
      if @basic_need_detail.update(basic_need_detail_params)
        format.html { redirect_to @basic_need_detail, notice: 'Basic need detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @basic_need_detail }
      else
        format.html { render :edit }
        format.json { render json: @basic_need_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basic_need_details/1
  # DELETE /basic_need_details/1.json
  def destroy
    @basic_need_detail.destroy
    respond_to do |format|
      format.html { redirect_to basic_need_details_url, notice: 'Basic need detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic_need_detail
      @basic_need_detail = BasicNeedDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def basic_need_detail_params
      params.require(:basic_need_detail).permit(:light, :toilet_facility, :health_card, :pds_card, :work_nrgea, :no_of_persons, :no_of_days, :hours_of_electricity, :farmer_id, :user_id)
    end
end
