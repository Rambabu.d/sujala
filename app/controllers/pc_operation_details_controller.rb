class PcOperationDetailsController < ApplicationController
  before_action :set_pc_operation_detail, only: [:show, :edit, :update, :destroy]

  # GET /pc_operation_details
  # GET /pc_operation_details.json
  def index
    @pc_operation_details = params[:farmer_id].present? ? PcOperationDetail.where(farmer_id: params[:farmer_id]) : PcOperationDetail.all
    render layout: false
  end

  # GET /pc_operation_details/1
  # GET /pc_operation_details/1.json
  def show
  end

  # GET /pc_operation_details/new
  def new
    @pc_operation_detail = PcOperationDetail.new
  end

  # GET /pc_operation_details/1/edit
  def edit
  end

  # POST /pc_operation_details
  # POST /pc_operation_details.json
  def create
    @pc_operation_detail = PcOperationDetail.new(pc_operation_detail_params)

    respond_to do |format|
      if @pc_operation_detail.save
        format.html { redirect_to @pc_operation_detail, notice: 'Pc operation detail was successfully created.' }
        format.json { render :show, status: :created, location: @pc_operation_detail }
      else
        format.html { render :new }
        format.json { render json: @pc_operation_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pc_operation_details/1
  # PATCH/PUT /pc_operation_details/1.json
  def update
    respond_to do |format|
      if @pc_operation_detail.update(pc_operation_detail_params)
        format.html { redirect_to @pc_operation_detail, notice: 'Pc operation detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @pc_operation_detail }
      else
        format.html { render :edit }
        format.json { render json: @pc_operation_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pc_operation_details/1
  # DELETE /pc_operation_details/1.json
  def destroy
    @pc_operation_detail.destroy
    respond_to do |format|
      format.html { redirect_to pc_operation_details_url, notice: 'Pc operation detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pc_operation_detail
      @pc_operation_detail = PcOperationDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pc_operation_detail_params
      params.require(:pc_operation_detail).permit(:production_cost_id, :farmer_id, :user_id, :operation_detail, :own_men, :own_women, :hired_men, :hired_women, :own_bullock, :hired_bullock, :own_tractor, :hired_tractor, :own_machinery, :hired_machinery, :input, :amount)
    end
end
