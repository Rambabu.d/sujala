class WatershedAwarenessesController < ApplicationController
  before_action :set_watershed_awareness, only: [:show, :edit, :update, :destroy]

  # GET /watershed_awarenesses
  # GET /watershed_awarenesses.json
  def index
    @watershed_awarenesses = params[:farmer_id].present? ? WatershedAwareness.where(farmer_id: params[:farmer_id]) : WatershedAwareness.all
    render layout: false
  end

  # GET /watershed_awarenesses/1
  # GET /watershed_awarenesses/1.json
  def show
  end

  # GET /watershed_awarenesses/new
  def new
    @watershed_awareness = WatershedAwareness.new
  end

  # GET /watershed_awarenesses/1/edit
  def edit
  end

  # POST /watershed_awarenesses
  # POST /watershed_awarenesses.json
  def create
    @watershed_awareness = WatershedAwareness.new(watershed_awareness_params)

    respond_to do |format|
      if @watershed_awareness.save
        format.html { redirect_to @watershed_awareness, notice: 'Watershed awareness was successfully created.' }
        format.json { render :show, status: :created, location: @watershed_awareness }
      else
        format.html { render :new }
        format.json { render json: @watershed_awareness.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /watershed_awarenesses/1
  # PATCH/PUT /watershed_awarenesses/1.json
  def update
    respond_to do |format|
      if @watershed_awareness.update(watershed_awareness_params)
        format.html { redirect_to @watershed_awareness, notice: 'Watershed awareness was successfully updated.' }
        format.json { render :show, status: :ok, location: @watershed_awareness }
      else
        format.html { render :edit }
        format.json { render json: @watershed_awareness.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /watershed_awarenesses/1
  # DELETE /watershed_awarenesses/1.json
  def destroy
    @watershed_awareness.destroy
    respond_to do |format|
      format.html { redirect_to watershed_awarenesses_url, notice: 'Watershed awareness was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watershed_awareness
      @watershed_awareness = WatershedAwareness.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watershed_awareness_params
      params.require(:watershed_awareness).permit(:farmer_id, :particular_id, :awareness)
    end
end
