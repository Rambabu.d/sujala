class CultivateHorticulturesController < ApplicationController
  before_action :set_cultivate_horticulture, only: [:show, :edit, :update, :destroy]

  # GET /cultivate_horticultures
  # GET /cultivate_horticultures.json
  def index
    @cultivate_horticultures = params[:farmer_id].present? ? CultivateHorticulture.where(farmer_id: params[:farmer_id]) : CultivateHorticulture.all
    render layout: false
  end

  # GET /cultivate_horticultures/1
  # GET /cultivate_horticultures/1.json
  def show
  end

  # GET /cultivate_horticultures/new
  def new
    @cultivate_horticulture = CultivateHorticulture.new
  end

  # GET /cultivate_horticultures/1/edit
  def edit
  end

  # POST /cultivate_horticultures
  # POST /cultivate_horticultures.json
  def create
    @cultivate_horticulture = CultivateHorticulture.new(cultivate_horticulture_params)

    respond_to do |format|
      if @cultivate_horticulture.save
        format.html { redirect_to @cultivate_horticulture, notice: 'Cultivate horticulture was successfully created.' }
        format.json { render :show, status: :created, location: @cultivate_horticulture }
      else
        format.html { render :new }
        format.json { render json: @cultivate_horticulture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cultivate_horticultures/1
  # PATCH/PUT /cultivate_horticultures/1.json
  def update
    respond_to do |format|
      if @cultivate_horticulture.update(cultivate_horticulture_params)
        format.html { redirect_to @cultivate_horticulture, notice: 'Cultivate horticulture was successfully updated.' }
        format.json { render :show, status: :ok, location: @cultivate_horticulture }
      else
        format.html { render :edit }
        format.json { render json: @cultivate_horticulture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cultivate_horticultures/1
  # DELETE /cultivate_horticultures/1.json
  def destroy
    @cultivate_horticulture.destroy
    respond_to do |format|
      format.html { redirect_to cultivate_horticultures_url, notice: 'Cultivate horticulture was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cultivate_horticulture
      @cultivate_horticulture = CultivateHorticulture.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cultivate_horticulture_params
      params.require(:cultivate_horticulture).permit(:farmer_id, :horticulture_plant_id, :number)
    end
end
