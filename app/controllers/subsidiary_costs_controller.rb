class SubsidiaryCostsController < ApplicationController
  before_action :set_subsidiary_cost, only: [:show, :edit, :update, :destroy]

  # GET /subsidiary_costs
  # GET /subsidiary_costs.json
  def index
    @subsidiary_costs = params[:farmer_id].present? ? SubsidiaryCost.where(farmer_id: params[:farmer_id]) : SubsidiaryCost.all
    render layout: false
  end

  # GET /subsidiary_costs/1
  # GET /subsidiary_costs/1.json
  def show
  end

  # GET /subsidiary_costs/new
  def new
    @subsidiary_cost = SubsidiaryCost.new
  end

  # GET /subsidiary_costs/1/edit
  def edit
  end

  # POST /subsidiary_costs
  # POST /subsidiary_costs.json
  def create
    SubsidiaryCost.where(farmer_id: params[:farmer_id]).destroy_all
    @subsidiary_cost = SubsidiaryCost.new(subsidiary_cost_params)

    respond_to do |format|
      if @subsidiary_cost.save
        format.html { redirect_to @subsidiary_cost, notice: 'Subsidiary cost was successfully created.' }
        format.json { render :show, status: :created, location: @subsidiary_cost }
      else
        format.html { render :new }
        format.json { render json: @subsidiary_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subsidiary_costs/1
  # PATCH/PUT /subsidiary_costs/1.json
  def update
    respond_to do |format|
      if @subsidiary_cost.update(subsidiary_cost_params)
        format.html { redirect_to @subsidiary_cost, notice: 'Subsidiary cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @subsidiary_cost }
      else
        format.html { render :edit }
        format.json { render json: @subsidiary_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subsidiary_costs/1
  # DELETE /subsidiary_costs/1.json
  def destroy
    @subsidiary_cost.destroy
    respond_to do |format|
      format.html { redirect_to subsidiary_costs_url, notice: 'Subsidiary cost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subsidiary_cost
      @subsidiary_cost = SubsidiaryCost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subsidiary_cost_params
      params.require(:subsidiary_cost).permit(:farmer_id, :dry_fodder, :green_fodder, :concentrates, :manure, :milk_price, :lacation_period)
    end
end
