class MicroWaterShedsController < ApplicationController
  before_action :set_micro_water_shed, only: [:show, :edit, :update, :destroy]

  # GET /micro_water_sheds
  # GET /micro_water_sheds.json
  def index
    @micro_water_sheds = MicroWaterShed.where(sub_water_shed_id: params[:sub_water_shed_id]).preload(:sub_water_shed) unless params[:sub_water_shed_id].blank?
    @micro_water_sheds ||= MicroWaterShed.all.preload(:sub_water_shed)
    render layout: false
  end

  # GET /micro_water_sheds/1
  # GET /micro_water_sheds/1.json
  def show
  end

  # GET /micro_water_sheds/new
  def new
    @micro_water_shed = MicroWaterShed.new
  end

  # GET /micro_water_sheds/1/edit
  def edit
  end

  # POST /micro_water_sheds
  # POST /micro_water_sheds.json
  def create
    @micro_water_shed = MicroWaterShed.new(micro_water_shed_params)

    respond_to do |format|
      if @micro_water_shed.save
        format.html { redirect_to @micro_water_shed, notice: 'Micro water shed was successfully created.' }
        format.json { render :show, status: :created, location: @micro_water_shed }
      else
        format.html { render :new }
        format.json { render json: @micro_water_shed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /micro_water_sheds/1
  # PATCH/PUT /micro_water_sheds/1.json
  def update
    respond_to do |format|
      if @micro_water_shed.update(micro_water_shed_params)
        format.html { redirect_to @micro_water_shed, notice: 'Micro water shed was successfully updated.' }
        format.json { render :show, status: :ok, location: @micro_water_shed }
      else
        format.html { render :edit }
        format.json { render json: @micro_water_shed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /micro_water_sheds/1
  # DELETE /micro_water_sheds/1.json
  def destroy
    @micro_water_shed.destroy
    respond_to do |format|
      format.html { redirect_to micro_water_sheds_url, notice: 'Micro water shed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_micro_water_shed
      @micro_water_shed = MicroWaterShed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def micro_water_shed_params
      params.require(:micro_water_shed).permit(:name, :code, :sub_water_shed_id)
    end
end
