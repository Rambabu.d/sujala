class PpcDetailsController < ApplicationController
  before_action :set_ppc_detail, only: [:show, :edit, :update, :destroy]

  # GET /ppc_details
  # GET /ppc_details.json
  def index
    @ppc_details = params[:farmer_id].present? ? PpcDetail.where(farmer_id: params[:farmer_id]) : PpcDetail.all
    render layout: false
  end

  # GET /ppc_details/1
  # GET /ppc_details/1.json
  def show
  end

  # GET /ppc_details/new
  def new
    @ppc_detail = PpcDetail.new
  end

  # GET /ppc_details/1/edit
  def edit
  end

  # POST /ppc_details
  # POST /ppc_details.json
  def create
    @ppc_detail = PpcDetail.new(ppc_detail_params)

    respond_to do |format|
      if @ppc_detail.save
        format.html { redirect_to @ppc_detail, notice: 'Ppc detail was successfully created.' }
        format.json { render :show, status: :created, location: @ppc_detail }
      else
        format.html { render :new }
        format.json { render json: @ppc_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ppc_details/1
  # PATCH/PUT /ppc_details/1.json
  def update
    respond_to do |format|
      if @ppc_detail.update(ppc_detail_params)
        format.html { redirect_to @ppc_detail, notice: 'Ppc detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @ppc_detail }
      else
        format.html { render :edit }
        format.json { render json: @ppc_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ppc_details/1
  # DELETE /ppc_details/1.json
  def destroy
    @ppc_detail.destroy
    respond_to do |format|
      format.html { redirect_to ppc_details_url, notice: 'Ppc detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ppc_detail
      @ppc_detail = PpcDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ppc_detail_params
      params.require(:ppc_detail).permit(:sprays_count, :production_cost_id, :farmer_id, :user_id, :details_of_ppc_application, :own_men_mandays, :hired_men_mandays, :own_women_mandays, :hired_women_mandays, :own_bullock_days, :hired_bullock_days, :own_tractor_days, :hired_tractor_days, :input, :amount)
    end
end
