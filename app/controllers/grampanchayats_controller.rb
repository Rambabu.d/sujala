class GrampanchayatsController < ApplicationController
  before_action :set_grampanchayat, only: [:show, :edit, :update, :destroy]

  # GET /grampanchayats
  # GET /grampanchayats.json
  def index
    @grampanchayats = Grampanchayat.where(hobli_id: params[:hobli_id]).preload(:hobli) unless params[:hobli_id].blank?
    @grampanchayats ||= Grampanchayat.all.preload(:hobli)
    render layout: false
  end

  # GET /grampanchayats/1
  # GET /grampanchayats/1.json
  def show
  end

  # GET /grampanchayats/new
  def new
    @grampanchayat = Grampanchayat.new
  end

  # GET /grampanchayats/1/edit
  def edit
  end

  # POST /grampanchayats
  # POST /grampanchayats.json
  def create
    @grampanchayat = Grampanchayat.new(grampanchayat_params)

    respond_to do |format|
      if @grampanchayat.save
        format.html { redirect_to @grampanchayat, notice: 'Grampanchayat was successfully created.' }
        format.json { render :show, status: :created, location: @grampanchayat }
      else
        format.html { render :new }
        format.json { render json: @grampanchayat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grampanchayats/1
  # PATCH/PUT /grampanchayats/1.json
  def update
    respond_to do |format|
      if @grampanchayat.update(grampanchayat_params)
        format.html { redirect_to @grampanchayat, notice: 'Grampanchayat was successfully updated.' }
        format.json { render :show, status: :ok, location: @grampanchayat }
      else
        format.html { render :edit }
        format.json { render json: @grampanchayat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grampanchayats/1
  # DELETE /grampanchayats/1.json
  def destroy
    @grampanchayat.destroy
    respond_to do |format|
      format.html { redirect_to grampanchayats_url, notice: 'Grampanchayat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grampanchayat
      @grampanchayat = Grampanchayat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def grampanchayat_params
      params.require(:grampanchayat).permit(:name, :hobli_id)
    end
end
