class FamilyAnnualIncomesController < ApplicationController
  before_action :set_family_annual_income, only: [:show, :edit, :update, :destroy]

  # GET /family_annual_incomes
  # GET /family_annual_incomes.json
  def index
    @family_annual_incomes = params[:farmer_id].present? ? FamilyAnnualIncome.where(farmer_id: params[:farmer_id]) : FamilyAnnualIncome.all
    render layout: false
  end

  # GET /family_annual_incomes/1
  # GET /family_annual_incomes/1.json
  def show
  end

  # GET /family_annual_incomes/new
  def new
    @family_annual_income = FamilyAnnualIncome.new
  end

  # GET /family_annual_incomes/1/edit
  def edit
  end

  # POST /family_annual_incomes
  # POST /family_annual_incomes.json
  def create
    @family_annual_income = FamilyAnnualIncome.new(family_annual_income_params)

    respond_to do |format|
      if @family_annual_income.save
        format.html { redirect_to @family_annual_income, notice: 'Family annual income was successfully created.' }
        format.json { render :show, status: :created, location: @family_annual_income }
      else
        format.html { render :new }
        format.json { render json: @family_annual_income.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /family_annual_incomes/1
  # PATCH/PUT /family_annual_incomes/1.json
  def update
    respond_to do |format|
      if @family_annual_income.update(family_annual_income_params)
        format.html { redirect_to @family_annual_income, notice: 'Family annual income was successfully updated.' }
        format.json { render :show, status: :ok, location: @family_annual_income }
      else
        format.html { render :edit }
        format.json { render json: @family_annual_income.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /family_annual_incomes/1
  # DELETE /family_annual_incomes/1.json
  def destroy
    @family_annual_income.destroy
    respond_to do |format|
      format.html { redirect_to family_annual_incomes_url, notice: 'Family annual income was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_family_annual_income
      @family_annual_income = FamilyAnnualIncome.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def family_annual_income_params
      params.require(:family_annual_income).permit(:farmer_id, :income_source_id, :total_income, :total_expenditure)
    end
end
