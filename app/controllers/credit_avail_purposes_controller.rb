class CreditAvailPurposesController < ApplicationController
  before_action :set_credit_avail_purpose, only: [:show, :edit, :update, :destroy]

  # GET /credit_avail_purposes
  # GET /credit_avail_purposes.json
  def index
    credit_source = CreditSource.where(name: params[:from])
    @credit_avail_purposes = credit_source.empty? ? CreditAvailPurpose.all : CreditAvailPurpose.where(credit_source_id: credit_source)
    render layout: false
  end

  # GET /credit_avail_purposes/1
  # GET /credit_avail_purposes/1.json
  def show
  end

  # GET /credit_avail_purposes/new
  def new
    @credit_avail_purpose = CreditAvailPurpose.new
  end

  # GET /credit_avail_purposes/1/edit
  def edit
  end

  # POST /credit_avail_purposes
  # POST /credit_avail_purposes.json
  def create
    @credit_avail_purpose = CreditAvailPurpose.new(credit_avail_purpose_params)

    respond_to do |format|
      if @credit_avail_purpose.save
        format.html { redirect_to @credit_avail_purpose, notice: 'Credit avail purpose was successfully created.' }
        format.json { render :show, status: :created, location: @credit_avail_purpose }
      else
        format.html { render :new }
        format.json { render json: @credit_avail_purpose.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /credit_avail_purposes/1
  # PATCH/PUT /credit_avail_purposes/1.json
  def update
    respond_to do |format|
      if @credit_avail_purpose.update(credit_avail_purpose_params)
        format.html { redirect_to @credit_avail_purpose, notice: 'Credit avail purpose was successfully updated.' }
        format.json { render :show, status: :ok, location: @credit_avail_purpose }
      else
        format.html { render :edit }
        format.json { render json: @credit_avail_purpose.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /credit_avail_purposes/1
  # DELETE /credit_avail_purposes/1.json
  def destroy
    @credit_avail_purpose.destroy
    respond_to do |format|
      format.html { redirect_to credit_avail_purposes_url, notice: 'Credit avail purpose was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_avail_purpose
      @credit_avail_purpose = CreditAvailPurpose.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_avail_purpose_params
      params.require(:credit_avail_purpose).permit(:name, :credit_source_id)
    end
end
