class SubsidiaryEnterprisesController < ApplicationController
  before_action :set_subsidiary_enterprise, only: [:show, :edit, :update, :destroy]

  # GET /subsidiary_enterprises
  # GET /subsidiary_enterprises.json
  def index
    @subsidiary_enterprises = params[:farmer_id].present? ? SubsidiaryEnterprise.where(farmer_id: params[:farmer_id]) : SubsidiaryEnterprise.all
    render layout: false
  end

  # GET /subsidiary_enterprises/1
  # GET /subsidiary_enterprises/1.json
  def show
  end

  # GET /subsidiary_enterprises/new
  def new
    @subsidiary_enterprise = SubsidiaryEnterprise.new
  end

  # GET /subsidiary_enterprises/1/edit
  def edit
  end

  # POST /subsidiary_enterprises
  # POST /subsidiary_enterprises.json
  def create
    @subsidiary_enterprise = SubsidiaryEnterprise.new(subsidiary_enterprise_params)

    respond_to do |format|
      if @subsidiary_enterprise.save
        format.html { redirect_to @subsidiary_enterprise, notice: 'Subsidiary enterprise was successfully created.' }
        format.json { render :show, status: :created, location: @subsidiary_enterprise }
      else
        format.html { render :new }
        format.json { render json: @subsidiary_enterprise.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subsidiary_enterprises/1
  # PATCH/PUT /subsidiary_enterprises/1.json
  def update
    respond_to do |format|
      if @subsidiary_enterprise.update(subsidiary_enterprise_params)
        format.html { redirect_to @subsidiary_enterprise, notice: 'Subsidiary enterprise was successfully updated.' }
        format.json { render :show, status: :ok, location: @subsidiary_enterprise }
      else
        format.html { render :edit }
        format.json { render json: @subsidiary_enterprise.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subsidiary_enterprises/1
  # DELETE /subsidiary_enterprises/1.json
  def destroy
    @subsidiary_enterprise.destroy
    respond_to do |format|
      format.html { redirect_to subsidiary_enterprises_url, notice: 'Subsidiary enterprise was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subsidiary_enterprise
      @subsidiary_enterprise = SubsidiaryEnterprise.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subsidiary_enterprise_params
      params.require(:subsidiary_enterprise).permit(:farmer_id, :livestock_name_id, :no_of_animals, :no_of_milching_animals, :present_value_of_animals, :monthly_feed_dry, :monthly_feed_green, :monthly_feed_conc, :labour_no_of_hrs, :labour_amount, :healthcare_expenses, :milk, :lacation_period, :manure, :price_per_unit)
    end
end
