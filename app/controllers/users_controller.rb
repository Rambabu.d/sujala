class UsersController < ApplicationController
	
	def register
		render :layout => false 
	end

	def signin
		render :layout => false
	end

	def template
	  render :layout => false 
	end

	def index
		@users = User.all
	end

	def actions
		@actions = User.find(params[:user]).roles.joins(:ractions).pluck("ractions.name")
	end

	def user_roles_template
		render :layout => false
	end

	def user_roles
		@users = User.where(id: params[:user_id])
		render :layout => false 
	end

	def create_user_role
		@user_role = UserRole.new
		@params = params.require(:user_role).permit(:user_id, :role_id)
		@role = @user_role.update_attributes(@params)
		render :plain => "Ok", :layout => false
	end

	def delete_user_role
		@user_role = UserRole.where(user_id: params[:user_id], role_id: params[:role_id])
		@role = @user_role.destroy_all
		render :plain => "Ok", :layout => false
	end

end