class LivestockNamesController < ApplicationController
  before_action :set_livestock_name, only: [:show, :edit, :update, :destroy]

  # GET /livestock_names
  # GET /livestock_names.json
  def index
    @livestock_names = LivestockName.all
    render layout: false
  end

  # GET /livestock_names/1
  # GET /livestock_names/1.json
  def show
  end

  # GET /livestock_names/new
  def new
    @livestock_name = LivestockName.new
  end

  # GET /livestock_names/1/edit
  def edit
  end

  # POST /livestock_names
  # POST /livestock_names.json
  def create
    @livestock_name = LivestockName.new(livestock_name_params)

    respond_to do |format|
      if @livestock_name.save
        format.html { redirect_to @livestock_name, notice: 'Livestock name was successfully created.' }
        format.json { render :show, status: :created, location: @livestock_name }
      else
        format.html { render :new }
        format.json { render json: @livestock_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /livestock_names/1
  # PATCH/PUT /livestock_names/1.json
  def update
    respond_to do |format|
      if @livestock_name.update(livestock_name_params)
        format.html { redirect_to @livestock_name, notice: 'Livestock name was successfully updated.' }
        format.json { render :show, status: :ok, location: @livestock_name }
      else
        format.html { render :edit }
        format.json { render json: @livestock_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /livestock_names/1
  # DELETE /livestock_names/1.json
  def destroy
    @livestock_name.destroy
    respond_to do |format|
      format.html { redirect_to livestock_names_url, notice: 'Livestock name was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_livestock_name
      @livestock_name = LivestockName.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def livestock_name_params
      params.require(:livestock_name).permit(:name)
    end
end
