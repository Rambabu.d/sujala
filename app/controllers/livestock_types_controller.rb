class LivestockTypesController < ApplicationController
  before_action :set_livestock_type, only: [:show, :edit, :update, :destroy]

  # GET /livestock_types
  # GET /livestock_types.json
  def index
    @livestock_types = LivestockType.all
    render layout: false
  end

  # GET /livestock_types/1
  # GET /livestock_types/1.json
  def show
  end

  # GET /livestock_types/new
  def new
    @livestock_type = LivestockType.new
  end

  # GET /livestock_types/1/edit
  def edit
  end

  # POST /livestock_types
  # POST /livestock_types.json
  def create
    @livestock_type = LivestockType.new(livestock_type_params)

    respond_to do |format|
      if @livestock_type.save
        format.html { redirect_to @livestock_type, notice: 'Livestock type was successfully created.' }
        format.json { render :show, status: :created, location: @livestock_type }
      else
        format.html { render :new }
        format.json { render json: @livestock_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /livestock_types/1
  # PATCH/PUT /livestock_types/1.json
  def update
    respond_to do |format|
      if @livestock_type.update(livestock_type_params)
        format.html { redirect_to @livestock_type, notice: 'Livestock type was successfully updated.' }
        format.json { render :show, status: :ok, location: @livestock_type }
      else
        format.html { render :edit }
        format.json { render json: @livestock_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /livestock_types/1
  # DELETE /livestock_types/1.json
  def destroy
    @livestock_type.destroy
    respond_to do |format|
      format.html { redirect_to livestock_types_url, notice: 'Livestock type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_livestock_type
      @livestock_type = LivestockType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def livestock_type_params
      params.require(:livestock_type).permit(:name, :is_active)
    end
end
