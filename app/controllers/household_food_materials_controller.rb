class HouseholdFoodMaterialsController < ApplicationController
  before_action :set_household_food_material, only: [:show, :edit, :update, :destroy]

  # GET /household_food_materials
  # GET /household_food_materials.json
  def index
    @household_food_materials = params[:farmer_id].present? ? HouseholdFoodMaterial.where(farmer_id: params[:farmer_id]) : HouseholdFoodMaterial.all
    render layout: false
  end

  # GET /household_food_materials/1
  # GET /household_food_materials/1.json
  def show
  end

  # GET /household_food_materials/new
  def new
    @household_food_material = HouseholdFoodMaterial.new
  end

  # GET /household_food_materials/1/edit
  def edit
  end

  # POST /household_food_materials
  # POST /household_food_materials.json
  def create
    @household_food_material = HouseholdFoodMaterial.new(household_food_material_params)

    respond_to do |format|
      if @household_food_material.save
        format.html { redirect_to @household_food_material, notice: 'Household food material was successfully created.' }
        format.json { render :show, status: :created, location: @household_food_material }
      else
        format.html { render :new }
        format.json { render json: @household_food_material.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /household_food_materials/1
  # PATCH/PUT /household_food_materials/1.json
  def update
    respond_to do |format|
      if @household_food_material.update(household_food_material_params)
        format.html { redirect_to @household_food_material, notice: 'Household food material was successfully updated.' }
        format.json { render :show, status: :ok, location: @household_food_material }
      else
        format.html { render :edit }
        format.json { render json: @household_food_material.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /household_food_materials/1
  # DELETE /household_food_materials/1.json
  def destroy
    @household_food_material.destroy
    respond_to do |format|
      format.html { redirect_to household_food_materials_url, notice: 'Household food material was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_household_food_material
      @household_food_material = HouseholdFoodMaterial.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def household_food_material_params
      params.require(:household_food_material).permit(:farmer_id, :food_material_id, :status)
    end
end
