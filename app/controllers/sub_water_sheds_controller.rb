class SubWaterShedsController < ApplicationController
  before_action :set_sub_water_shed, only: [:show, :edit, :update, :destroy]

  # GET /sub_water_sheds
  # GET /sub_water_sheds.json
  def index
    @sub_water_sheds = SubWaterShed.where(district_id: params[:district_id]).preload(:district) unless params[:district_id].blank?
    @sub_water_sheds ||= SubWaterShed.all.preload(:district)
    render layout: false
  end

  # GET /sub_water_sheds/1
  # GET /sub_water_sheds/1.json
  def show
  end

  # GET /sub_water_sheds/new
  def new
    @sub_water_shed = SubWaterShed.new
  end

  # GET /sub_water_sheds/1/edit
  def edit
  end

  # POST /sub_water_sheds
  # POST /sub_water_sheds.json
  def create
    @sub_water_shed = SubWaterShed.new(sub_water_shed_params)

    respond_to do |format|
      if @sub_water_shed.save
        format.html { redirect_to @sub_water_shed, notice: 'Sub water shed was successfully created.' }
        format.json { render :show, status: :created, location: @sub_water_shed }
      else
        format.html { render :new }
        format.json { render json: @sub_water_shed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sub_water_sheds/1
  # PATCH/PUT /sub_water_sheds/1.json
  def update
    respond_to do |format|
      if @sub_water_shed.update(sub_water_shed_params)
        format.html { redirect_to @sub_water_shed, notice: 'Sub water shed was successfully updated.' }
        format.json { render :show, status: :ok, location: @sub_water_shed }
      else
        format.html { render :edit }
        format.json { render json: @sub_water_shed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_water_sheds/1
  # DELETE /sub_water_sheds/1.json
  def destroy
    @sub_water_shed.destroy
    respond_to do |format|
      format.html { redirect_to sub_water_sheds_url, notice: 'Sub water shed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sub_water_shed
      @sub_water_shed = SubWaterShed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sub_water_shed_params
      params.require(:sub_water_shed).permit(:name, :code, :district_id)
    end
end
