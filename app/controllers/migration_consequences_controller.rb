class MigrationConsequencesController < ApplicationController
  before_action :set_migration_consequence, only: [:show, :edit, :update, :destroy]

  # GET /migration_consequences
  # GET /migration_consequences.json
  def index
    @migration_consequences = params[:farmer_id].present? ? MigrationConsequence.where(farmer_id: params[:farmer_id]) : MigrationConsequence.all
    render layout: false
  end

  # GET /migration_consequences/1
  # GET /migration_consequences/1.json
  def show
  end

  # GET /migration_consequences/new
  def new
    @migration_consequence = MigrationConsequence.new
  end

  # GET /migration_consequences/1/edit
  def edit
  end

  # POST /migration_consequences
  # POST /migration_consequences.json
  def create
    @migration_consequence = MigrationConsequence.new(migration_consequence_params)

    respond_to do |format|
      if @migration_consequence.save
        format.html { redirect_to @migration_consequence, notice: 'Migration consequence was successfully created.' }
        format.json { render :show, status: :created, location: @migration_consequence }
      else
        format.html { render :new }
        format.json { render json: @migration_consequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /migration_consequences/1
  # PATCH/PUT /migration_consequences/1.json
  def update
    respond_to do |format|
      if @migration_consequence.update(migration_consequence_params)
        format.html { redirect_to @migration_consequence, notice: 'Migration consequence was successfully updated.' }
        format.json { render :show, status: :ok, location: @migration_consequence }
      else
        format.html { render :edit }
        format.json { render json: @migration_consequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /migration_consequences/1
  # DELETE /migration_consequences/1.json
  def destroy
    @migration_consequence.destroy
    respond_to do |format|
      format.html { redirect_to migration_consequences_url, notice: 'Migration consequence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_migration_consequence
      @migration_consequence = MigrationConsequence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def migration_consequence_params
      params.require(:migration_consequence).permit(:migration_positive_consequence_id, :migration_negative_consequence_id, :farmer_id)
    end
end
