class IntercropListsController < ApplicationController
  before_action :set_intercrop_list, only: [:show, :edit, :update, :destroy]

  # GET /intercrop_lists
  # GET /intercrop_lists.json
  def index
    @intercrop_lists = IntercropList.all
    render layout: false
  end

  # GET /intercrop_lists/1
  # GET /intercrop_lists/1.json
  def show
  end

  # GET /intercrop_lists/new
  def new
    @intercrop_list = IntercropList.new
  end

  # GET /intercrop_lists/1/edit
  def edit
  end

  # POST /intercrop_lists
  # POST /intercrop_lists.json
  def create
    @intercrop_list = IntercropList.new(intercrop_list_params)

    respond_to do |format|
      if @intercrop_list.save
        format.html { redirect_to @intercrop_list, notice: 'Intercrop list was successfully created.' }
        format.json { render :show, status: :created, location: @intercrop_list }
      else
        format.html { render :new }
        format.json { render json: @intercrop_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /intercrop_lists/1
  # PATCH/PUT /intercrop_lists/1.json
  def update
    respond_to do |format|
      if @intercrop_list.update(intercrop_list_params)
        format.html { redirect_to @intercrop_list, notice: 'Intercrop list was successfully updated.' }
        format.json { render :show, status: :ok, location: @intercrop_list }
      else
        format.html { render :edit }
        format.json { render json: @intercrop_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /intercrop_lists/1
  # DELETE /intercrop_lists/1.json
  def destroy
    @intercrop_list.destroy
    respond_to do |format|
      format.html { redirect_to intercrop_lists_url, notice: 'Intercrop list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_intercrop_list
      @intercrop_list = IntercropList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def intercrop_list_params
      params.require(:intercrop_list).permit(:name)
    end
end
