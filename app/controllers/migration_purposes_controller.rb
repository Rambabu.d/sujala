class MigrationPurposesController < ApplicationController
  before_action :set_migration_purpose, only: [:show, :edit, :update, :destroy]

  # GET /migration_purposes
  # GET /migration_purposes.json
  def index
    @migration_purposes = MigrationPurpose.all
    render layout: false
  end

  # GET /migration_purposes/1
  # GET /migration_purposes/1.json
  def show
  end

  # GET /migration_purposes/new
  def new
    @migration_purpose = MigrationPurpose.new
  end

  # GET /migration_purposes/1/edit
  def edit
  end

  # POST /migration_purposes
  # POST /migration_purposes.json
  def create
    @migration_purpose = MigrationPurpose.new(migration_purpose_params)

    respond_to do |format|
      if @migration_purpose.save
        format.html { redirect_to @migration_purpose, notice: 'Migration purpose was successfully created.' }
        format.json { render :show, status: :created, location: @migration_purpose }
      else
        format.html { render :new }
        format.json { render json: @migration_purpose.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /migration_purposes/1
  # PATCH/PUT /migration_purposes/1.json
  def update
    respond_to do |format|
      if @migration_purpose.update(migration_purpose_params)
        format.html { redirect_to @migration_purpose, notice: 'Migration purpose was successfully updated.' }
        format.json { render :show, status: :ok, location: @migration_purpose }
      else
        format.html { render :edit }
        format.json { render json: @migration_purpose.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /migration_purposes/1
  # DELETE /migration_purposes/1.json
  def destroy
    @migration_purpose.destroy
    respond_to do |format|
      format.html { redirect_to migration_purposes_url, notice: 'Migration purpose was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_migration_purpose
      @migration_purpose = MigrationPurpose.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def migration_purpose_params
      params.require(:migration_purpose).permit(:name)
    end
end
