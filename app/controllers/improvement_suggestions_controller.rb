class ImprovementSuggestionsController < ApplicationController
  before_action :set_improvement_suggestion, only: [:show, :edit, :update, :destroy]

  # GET /improvement_suggestions
  # GET /improvement_suggestions.json
  def index
    @improvement_suggestions = params[:farmer_id].present? ? ImprovementSuggestion.where(farmer_id: params[:farmer_id]) : ImprovementSuggestion.all
    render layout: false
  end

  # GET /improvement_suggestions/1
  # GET /improvement_suggestions/1.json
  def show
  end

  # GET /improvement_suggestions/new
  def new
    @improvement_suggestion = ImprovementSuggestion.new
  end

  # GET /improvement_suggestions/1/edit
  def edit
  end

  # POST /improvement_suggestions
  # POST /improvement_suggestions.json
  def create
    @improvement_suggestion = ImprovementSuggestion.new(improvement_suggestion_params)

    respond_to do |format|
      if @improvement_suggestion.save
        format.html { redirect_to @improvement_suggestion, notice: 'Improvement suggestion was successfully created.' }
        format.json { render :show, status: :created, location: @improvement_suggestion }
      else
        format.html { render :new }
        format.json { render json: @improvement_suggestion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /improvement_suggestions/1
  # PATCH/PUT /improvement_suggestions/1.json
  def update
    respond_to do |format|
      if @improvement_suggestion.update(improvement_suggestion_params)
        format.html { redirect_to @improvement_suggestion, notice: 'Improvement suggestion was successfully updated.' }
        format.json { render :show, status: :ok, location: @improvement_suggestion }
      else
        format.html { render :edit }
        format.json { render json: @improvement_suggestion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /improvement_suggestions/1
  # DELETE /improvement_suggestions/1.json
  def destroy
    @improvement_suggestion.destroy
    respond_to do |format|
      format.html { redirect_to improvement_suggestions_url, notice: 'Improvement suggestion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_improvement_suggestion
      @improvement_suggestion = ImprovementSuggestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def improvement_suggestion_params
      params.require(:improvement_suggestion).permit(:farmer_id, :suggestion_paramater_id, :suggestions)
    end
end
