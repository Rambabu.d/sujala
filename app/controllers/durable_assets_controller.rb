class DurableAssetsController < ApplicationController
  before_action :set_asset, only: [:show, :edit, :update, :destroy]

  # GET /durable_assets
  # GET /durable_assets.json
  def index
    @durable_assets = params[:farmer_id].present? ? DurableAsset.where(farmer_id: params[:farmer_id]) : DurableAsset.all
    render :layout => false 
  end

  # GET /durable_assets/1
  # GET /durable_assets/1.json
  def show
  end

  # GET /durable_assets/new
  def new
    @durable_asset = DurableAsset.new
  end

  # GET /durable_assets/1/edit
  def edit
  end

  # POST /durable_assets
  # POST /durable_assets.json
  def create
    @durable_asset = DurableAsset.new(asset_params)

    respond_to do |format|
      if @durable_asset.save
        format.html { redirect_to @durable_asset, notice: 'Asset was successfully created.' }
        format.json { render :show, status: :created, location: @durable_asset }
      else
        format.html { render :new }
        format.json { render json: @durable_asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /durable_assets/1
  # PATCH/PUT /durable_assets/1.json
  def update
    respond_to do |format|
      if @durable_asset.update(asset_params)
        format.html { redirect_to @durable_asset, notice: 'Asset was successfully updated.' }
        format.json { render :show, status: :ok, location: @durable_asset }
      else
        format.html { render :edit }
        format.json { render json: @durable_asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /durable_assets/1
  # DELETE /durable_assets/1.json
  def destroy
    @durable_asset.destroy
    respond_to do |format|
      format.html { redirect_to durable_assets_url, notice: 'Asset was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asset
      @durable_asset = DurableAsset.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asset_params
      params.require(:durable_asset).permit(:farmer_id, :asset_type_id, :quantity, :present_value, :is_active)
    end
end
