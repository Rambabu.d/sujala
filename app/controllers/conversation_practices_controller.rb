class ConversationPracticesController < ApplicationController
  before_action :set_conversation_practice, only: [:show, :edit, :update, :destroy]

  # GET /conversation_practices
  # GET /conversation_practices.json
  def index
    @conversation_practices = params[:farmer_id].present? ? ConversationPractice.where(farmer_id: params[:farmer_id]) : ConversationPractice.all
    render layout: false
  end

  # GET /conversation_practices/1
  # GET /conversation_practices/1.json
  def show
  end

  # GET /conversation_practices/new
  def new
    @conversation_practice = ConversationPractice.new
  end

  # GET /conversation_practices/1/edit
  def edit
  end

  # POST /conversation_practices
  # POST /conversation_practices.json
  def create
    @conversation_practice = ConversationPractice.new(conversation_practice_params)

    respond_to do |format|
      if @conversation_practice.save
        format.html { redirect_to @conversation_practice, notice: 'Conversation practice was successfully created.' }
        format.json { render :show, status: :created, location: @conversation_practice }
      else
        format.html { render :new }
        format.json { render json: @conversation_practice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conversation_practices/1
  # PATCH/PUT /conversation_practices/1.json
  def update
    respond_to do |format|
      if @conversation_practice.update(conversation_practice_params)
        format.html { redirect_to @conversation_practice, notice: 'Conversation practice was successfully updated.' }
        format.json { render :show, status: :ok, location: @conversation_practice }
      else
        format.html { render :edit }
        format.json { render json: @conversation_practice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversation_practices/1
  # DELETE /conversation_practices/1.json
  def destroy
    @conversation_practice.destroy
    respond_to do |format|
      format.html { redirect_to conversation_practices_url, notice: 'Conversation practice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conversation_practice
      @conversation_practice = ConversationPractice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conversation_practice_params
      params.require(:conversation_practice).permit(:farmer_id, :conversation_structure_id, :year, :status, :implemented_by, :name_of_agency)
    end
end
