class FarmersController < ApplicationController
  before_action :set_farmer, only: [:show, :edit, :update, :destroy]
  #before_action :authenticate_user!
  include CleanPagination
  # GET /farmers
  # GET /farmers.json
  def index
    max_per_page = 15
    #byebug
    unless params[:user].blank?
      if User.find(params[:user]).roles.collect(&:name).include?("admin")
        #@farmers = Farmer.includes(:village, :caste, :smu, :topography, :grampanchayat).all
  
        @farmer_count = params[:micro_water_shed].blank? ? Farmer.count : Farmer.joins(:village).where('villages.micro_water_shed_id' => params[:micro_water_shed]).count
        paginate @farmer_count, max_per_page do |limit, offset|
          if params[:micro_water_shed].blank?
          @farmers = Farmer.includes(:village, :caste, :smu, :topography, :grampanchayat).limit(limit).offset(offset)
        else
          @farmers = Farmer.joins(:village).where('villages.micro_water_shed_id' => params[:micro_water_shed]).includes(:village, :caste, :smu, :topography, :grampanchayat).limit(limit).offset(offset)
          end
        end
      else
      if params[:micro_water_shed].blank?
        @farmers_current_user = Farmer.where(user_id: params[:user]).includes(:village, :caste, :smu, :topography, :grampanchayat) 
      else
        @farmers_current_user = Farmer.joins(:village).where('villages.micro_water_shed_id' => params[:micro_water_shed])
        .where(user_id: params[:user]).includes(:village, :caste, :smu, :topography, :grampanchayat) 
      end
        paginate @farmers_current_user.count, max_per_page do |limit, offset|
          @farmers = @farmers_current_user.limit(limit).offset(offset)
        end
      end
    end

    render :layout => false 
  end

  # GET /farmers/1
  # GET /farmers/1.json
  def show
  end

  def count
    render json: Farmer.farmer_count(params[:type], params[:micro_water_shed])
  end

  # GET /farmers/new
  def new
    @farmer = Farmer.new
    render :layout => false 
  end

  # GET /farmers/1/edit
  def edit
  end

  # POST /farmers
  # POST /farmers.json
  def create
    @farmer = Farmer.new(farmer_params)

    respond_to do |format|
      if @farmer.save
        format.html { redirect_to @farmer, notice: 'Farmer was successfully created.' }
        format.json { render :show, status: :created, location: @farmer }
      else
        format.html { render :new }
        format.json { render json: @farmer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farmers/1
  # PATCH/PUT /farmers/1.json
  def update
    respond_to do |format|
      if @farmer.update(farmer_params)
        format.html { redirect_to @farmer, notice: 'Farmer was successfully updated.' }
        format.json { render :show, status: :ok, location: @farmer }
      else
        format.html { render :edit }
        format.json { render json: @farmer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmers/1
  # DELETE /farmers/1.json
  def destroy
    @farmer.destroy
    respond_to do |format|
      format.html { redirect_to farmers_url, notice: 'Farmer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farmer
      @farmer = Farmer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def farmer_params
      params.require(:farmer).permit(:name, :village_id, :hamlet, :caste_id, :aadhar, :father_name, :phone, :smu_id, :topography_id, :grampanchayat_id, :user_id)
    end
end
