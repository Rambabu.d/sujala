class FarmingConstraintsController < ApplicationController
  before_action :set_farming_constraint, only: [:show, :edit, :update, :destroy]

  # GET /farming_constraints
  # GET /farming_constraints.json
  def index
    @farming_constraints = params[:farmer_id].present? ? FarmingConstraint.where(farmer_id: params[:farmer_id]) : FarmingConstraint.all
    render layout: false
  end

  # GET /farming_constraints/1
  # GET /farming_constraints/1.json
  def show
  end

  # GET /farming_constraints/new
  def new
    @farming_constraint = FarmingConstraint.new
  end

  # GET /farming_constraints/1/edit
  def edit
  end

  # POST /farming_constraints
  # POST /farming_constraints.json
  def create
    @farming_constraint = FarmingConstraint.new(farming_constraint_params)

    respond_to do |format|
      if @farming_constraint.save
        format.html { redirect_to @farming_constraint, notice: 'Farming constraint was successfully created.' }
        format.json { render :show, status: :created, location: @farming_constraint }
      else
        format.html { render :new }
        format.json { render json: @farming_constraint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farming_constraints/1
  # PATCH/PUT /farming_constraints/1.json
  def update
    respond_to do |format|
      if @farming_constraint.update(farming_constraint_params)
        format.html { redirect_to @farming_constraint, notice: 'Farming constraint was successfully updated.' }
        format.json { render :show, status: :ok, location: @farming_constraint }
      else
        format.html { render :edit }
        format.json { render json: @farming_constraint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farming_constraints/1
  # DELETE /farming_constraints/1.json
  def destroy
    @farming_constraint.destroy
    respond_to do |format|
      format.html { redirect_to farming_constraints_url, notice: 'Farming constraint was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farming_constraint
      @farming_constraint = FarmingConstraint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def farming_constraint_params
      params.require(:farming_constraint).permit(:farmer_id, :constraint_id, :response)
    end
end
