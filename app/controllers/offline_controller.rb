class OfflineController < ApplicationController

  def sync
    @sync_data = params['_json']

    @sync_data.each do |record|
      @model = JSON.parse(record['params']).keys.first
      if record['farmer'].blank? && @model == "farmer"
        Farmer.create(JSON.parse(record['params'])['farmer'])
      else
        @farmer = record['farmer']
        @farmer.permit!
        @farmer.delete :id
        @farmer_record = Farmer.where(@farmer).first
        @record = JSON.parse(record['params'])[@model]
        @record['farmer_id'] = @farmer_record.id

        @attrs = @record.select{|k,v| k.ends_with?("_id")}.reject{|k,v| k=='farmer_id'}
        @attrs.keys.each do |att|
          @id_value = @attrs[att]
          if @id_value > 9999999999
            @rr = @sync_data.find{|sd| sd['id'] == @id_value}
            mod_column = JSON.parse(@rr['params']).keys.first
            mod = JSON.parse(@rr['params'])[mod_column]
            mod['farmer_id'] = @farmer_record.id

            _rr = mod_column.classify.constantize.where(mod).first
            
            @record[att] = _rr.id
          end
        end
        @record = @record.reject{|k,v|  v.class == Hash}
        @model.classify.constantize.where(@record).first_or_create
      end
    end
    render plain: 'OK'
  end

  def manifest
    render :layout => false
  end
end