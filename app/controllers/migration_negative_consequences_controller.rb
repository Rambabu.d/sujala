class MigrationNegativeConsequencesController < ApplicationController
  before_action :set_migration_negative_consequence, only: [:show, :edit, :update, :destroy]

  # GET /migration_negative_consequences
  # GET /migration_negative_consequences.json
  def index
    @migration_negative_consequences = MigrationNegativeConsequence.all
    render layout: false
  end

  # GET /migration_negative_consequences/1
  # GET /migration_negative_consequences/1.json
  def show
  end

  # GET /migration_negative_consequences/new
  def new
    @migration_negative_consequence = MigrationNegativeConsequence.new
  end

  # GET /migration_negative_consequences/1/edit
  def edit
  end

  # POST /migration_negative_consequences
  # POST /migration_negative_consequences.json
  def create
    @migration_negative_consequence = MigrationNegativeConsequence.new(migration_negative_consequence_params)

    respond_to do |format|
      if @migration_negative_consequence.save
        format.html { redirect_to @migration_negative_consequence, notice: 'Migration negative consequence was successfully created.' }
        format.json { render :show, status: :created, location: @migration_negative_consequence }
      else
        format.html { render :new }
        format.json { render json: @migration_negative_consequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /migration_negative_consequences/1
  # PATCH/PUT /migration_negative_consequences/1.json
  def update
    respond_to do |format|
      if @migration_negative_consequence.update(migration_negative_consequence_params)
        format.html { redirect_to @migration_negative_consequence, notice: 'Migration negative consequence was successfully updated.' }
        format.json { render :show, status: :ok, location: @migration_negative_consequence }
      else
        format.html { render :edit }
        format.json { render json: @migration_negative_consequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /migration_negative_consequences/1
  # DELETE /migration_negative_consequences/1.json
  def destroy
    @migration_negative_consequence.destroy
    respond_to do |format|
      format.html { redirect_to migration_negative_consequences_url, notice: 'Migration negative consequence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_migration_negative_consequence
      @migration_negative_consequence = MigrationNegativeConsequence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def migration_negative_consequence_params
      params.require(:migration_negative_consequence).permit(:name)
    end
end
