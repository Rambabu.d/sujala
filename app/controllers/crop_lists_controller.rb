class CropListsController < ApplicationController
  before_action :set_crop_list, only: [:show, :edit, :update, :destroy]

  # GET /crop_lists
  # GET /crop_lists.json
  def index
    @crop_lists = params[:farmer_id].present? ? CropList.where(id: ProductionCost.where(farmer_id: params[:farmer_id]).pluck(:crop_list_id)) : CropList.all
    render layout: false
  end

  # GET /crop_lists/1
  # GET /crop_lists/1.json
  def show
  end

  # GET /crop_lists/new
  def new
    @crop_list = CropList.new
  end

  # GET /crop_lists/1/edit
  def edit
  end

  # POST /crop_lists
  # POST /crop_lists.json
  def create
    @crop_list = CropList.new(crop_list_params)

    respond_to do |format|
      if @crop_list.save
        format.html { redirect_to @crop_list, notice: 'Crop list was successfully created.' }
        format.json { render :show, status: :created, location: @crop_list }
      else
        format.html { render :new }
        format.json { render json: @crop_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /crop_lists/1
  # PATCH/PUT /crop_lists/1.json
  def update
    respond_to do |format|
      if @crop_list.update(crop_list_params)
        format.html { redirect_to @crop_list, notice: 'Crop list was successfully updated.' }
        format.json { render :show, status: :ok, location: @crop_list }
      else
        format.html { render :edit }
        format.json { render json: @crop_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /crop_lists/1
  # DELETE /crop_lists/1.json
  def destroy
    @crop_list.destroy
    respond_to do |format|
      format.html { redirect_to crop_lists_url, notice: 'Crop list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_crop_list
      @crop_list = CropList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def crop_list_params
      params.require(:crop_list).permit(:name)
    end
end
