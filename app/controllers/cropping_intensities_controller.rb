class CroppingIntensitiesController < ApplicationController
  before_action :set_cropping_intensity, only: [:show, :edit, :update, :destroy]

  # GET /cropping_intensities
  # GET /cropping_intensities.json
  def index
    @cropping_intensities = params[:farmer_id].present? ? CroppingIntensity.where(farmer_id: params[:farmer_id]) : CroppingIntensity.all
    render layout: false
  end

  # GET /cropping_intensities/1
  # GET /cropping_intensities/1.json
  def show
  end

  # GET /cropping_intensities/new
  def new
    @cropping_intensity = CroppingIntensity.new
  end

  # GET /cropping_intensities/1/edit
  def edit
  end

  # POST /cropping_intensities
  # POST /cropping_intensities.json
  def create
    @cropping_intensity = CroppingIntensity.new(cropping_intensity_params)

    respond_to do |format|
      if @cropping_intensity.save
        format.html { redirect_to @cropping_intensity, notice: 'Cropping intensity was successfully created.' }
        format.json { render :show, status: :created, location: @cropping_intensity }
      else
        format.html { render :new }
        format.json { render json: @cropping_intensity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cropping_intensities/1
  # PATCH/PUT /cropping_intensities/1.json
  def update
    respond_to do |format|
      if @cropping_intensity.update(cropping_intensity_params)
        format.html { redirect_to @cropping_intensity, notice: 'Cropping intensity was successfully updated.' }
        format.json { render :show, status: :ok, location: @cropping_intensity }
      else
        format.html { render :edit }
        format.json { render json: @cropping_intensity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cropping_intensities/1
  # DELETE /cropping_intensities/1.json
  def destroy
    @cropping_intensity.destroy
    respond_to do |format|
      format.html { redirect_to cropping_intensities_url, notice: 'Cropping intensity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cropping_intensity
      @cropping_intensity = CroppingIntensity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cropping_intensity_params
      params.require(:cropping_intensity).permit(:farmer_id, :survey_number_id, :total_land, :kharif, :rabi, :summer, :crop_id, :intercrop_id)
    end
end
