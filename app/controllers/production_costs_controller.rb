class ProductionCostsController < ApplicationController
  before_action :set_production_cost, only: [:show, :edit, :update, :destroy]

  # GET /production_costs
  # GET /production_costs.json
  def index
    @production_costs = params[:farmer_id].present? ?  ProductionCost.where(farmer_id: params[:farmer_id]) : ProductionCost.all
    render layout: false
  end

  # GET /production_costs/1
  # GET /production_costs/1.json
  def show
  end

  # GET /production_costs/new
  def new
    @production_cost = ProductionCost.new
  end

  # GET /production_costs/1/edit
  def edit
  end

  # POST /production_costs
  # POST /production_costs.json
  def create
    @production_cost = ProductionCost.new(production_cost_params)
    respond_to do |format|
      if @production_cost.save
        format.html { redirect_to @production_cost, notice: 'Production cost was successfully created.' }
        format.json { render :show, status: :created, location: @production_cost }
      else
        format.html { render :new }
        format.json { render json: @production_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /production_costs/1
  # PATCH/PUT /production_costs/1.json
  def update
    respond_to do |format|
      if @production_cost.update(production_cost_params)
        format.html { redirect_to @production_cost, notice: 'Production cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @production_cost }
      else
        format.html { render :edit }
        format.json { render json: @production_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /production_costs/1
  # DELETE /production_costs/1.json
  def destroy
    @production_cost.destroy
    respond_to do |format|
      format.html { redirect_to production_costs_url, notice: 'Production cost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_production_cost
      @production_cost = ProductionCost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def production_cost_params
      params.require(:production_cost).permit(:crop_type,:crop_number,:crop_list_id,:intercrop_list_id,
        :extent_of_area,:season,:type_of_land,:irrigation_method,:establishment,:annual_maintenance,
        :age_of_garden,:lifespan_of_garden,:farmer_id, :user_id, :seed_qty, :plants_count, :seeding_count,
        :seed_price_inter)
    end
end
