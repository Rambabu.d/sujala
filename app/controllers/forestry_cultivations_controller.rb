class ForestryCultivationsController < ApplicationController
  before_action :set_forestry_cultivation, only: [:show, :edit, :update, :destroy]

  # GET /forestry_cultivations
  # GET /forestry_cultivations.json
  def index
    @forestry_cultivations = params[:farmer_id].present? ? ForestryCultivation.where(farmer_id: params[:farmer_id]) : ForestryCultivation.all
    render layout: false
  end

  # GET /forestry_cultivations/1
  # GET /forestry_cultivations/1.json
  def show
  end

  # GET /forestry_cultivations/new
  def new
    @forestry_cultivation = ForestryCultivation.new
  end

  # GET /forestry_cultivations/1/edit
  def edit
  end

  # POST /forestry_cultivations
  # POST /forestry_cultivations.json
  def create
    @forestry_cultivation = ForestryCultivation.new(forestry_cultivation_params)

    respond_to do |format|
      if @forestry_cultivation.save
        format.html { redirect_to @forestry_cultivation, notice: 'Forestry cultivation was successfully created.' }
        format.json { render :show, status: :created, location: @forestry_cultivation }
      else
        format.html { render :new }
        format.json { render json: @forestry_cultivation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forestry_cultivations/1
  # PATCH/PUT /forestry_cultivations/1.json
  def update
    respond_to do |format|
      if @forestry_cultivation.update(forestry_cultivation_params)
        format.html { redirect_to @forestry_cultivation, notice: 'Forestry cultivation was successfully updated.' }
        format.json { render :show, status: :ok, location: @forestry_cultivation }
      else
        format.html { render :edit }
        format.json { render json: @forestry_cultivation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forestry_cultivations/1
  # DELETE /forestry_cultivations/1.json
  def destroy
    @forestry_cultivation.destroy
    respond_to do |format|
      format.html { redirect_to forestry_cultivations_url, notice: 'Forestry cultivation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forestry_cultivation
      @forestry_cultivation = ForestryCultivation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forestry_cultivation_params
      params.require(:forestry_cultivation).permit(:farmer_id, :forestry_plant_id, :number)
    end
end
