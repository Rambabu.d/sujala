class PrivateCreditsController < ApplicationController
  before_action :set_private_credit, only: [:show, :edit, :update, :destroy]

  # GET /private_credits
  # GET /private_credits.json
  def index
    @private_credits = params[:farmer_id].present? ? PrivateCredit.where(farmer_id: params[:farmer_id]) : PrivateCredit.all
    render layout: false
  end

  # GET /private_credits/1
  # GET /private_credits/1.json
  def show
  end

  # GET /private_credits/new
  def new
    @private_credit = PrivateCredit.new
  end

  # GET /private_credits/1/edit
  def edit
  end

  # POST /private_credits
  # POST /private_credits.json
  def create
    @private_credit = PrivateCredit.new(private_credit_params)

    respond_to do |format|
      if @private_credit.save
        format.html { redirect_to @private_credit, notice: 'Private credit was successfully created.' }
        format.json { render :show, status: :created, location: @private_credit }
      else
        format.html { render :new }
        format.json { render json: @private_credit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /private_credits/1
  # PATCH/PUT /private_credits/1.json
  def update
    respond_to do |format|
      if @private_credit.update(private_credit_params)
        format.html { redirect_to @private_credit, notice: 'Private credit was successfully updated.' }
        format.json { render :show, status: :ok, location: @private_credit }
      else
        format.html { render :edit }
        format.json { render json: @private_credit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /private_credits/1
  # DELETE /private_credits/1.json
  def destroy
    @private_credit.destroy
    respond_to do |format|
      format.html { redirect_to private_credits_url, notice: 'Private credit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_private_credit
      @private_credit = PrivateCredit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def private_credit_params
      params.require(:private_credit).permit(:farmer_id, :source, :private_amount, :rate_of_interest, :credit_avail_purpose_id, :repayment_status, :reactions)
    end
end
