class LabourMigrationDetailsController < ApplicationController
  before_action :set_labour_migration_detail, only: [:show, :edit, :update, :destroy]

  # GET /labour_migration_details
  # GET /labour_migration_details.json
  def index
    @labour_migration_details = params[:farmer_id].present? ? LabourMigrationDetail.where(farmer_id: params[:farmer_id]) : LabourMigrationDetail.all
    render layout: false
  end

  # GET /labour_migration_details/1
  # GET /labour_migration_details/1.json
  def show
  end

  # GET /labour_migration_details/new
  def new
    @labour_migration_detail = LabourMigrationDetail.new
  end

  # GET /labour_migration_details/1/edit
  def edit
  end

  # POST /labour_migration_details
  # POST /labour_migration_details.json
  def create
    @labour_migration_detail = LabourMigrationDetail.new(labour_migration_detail_params)

    respond_to do |format|
      if @labour_migration_detail.save
        format.html { redirect_to @labour_migration_detail, notice: 'Labour migration detail was successfully created.' }
        format.json { render :show, status: :created, location: @labour_migration_detail }
      else
        format.html { render :new }
        format.json { render json: @labour_migration_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /labour_migration_details/1
  # PATCH/PUT /labour_migration_details/1.json
  def update
    respond_to do |format|
      if @labour_migration_detail.update(labour_migration_detail_params)
        format.html { redirect_to @labour_migration_detail, notice: 'Labour migration detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @labour_migration_detail }
      else
        format.html { render :edit }
        format.json { render json: @labour_migration_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /labour_migration_details/1
  # DELETE /labour_migration_details/1.json
  def destroy
    @labour_migration_detail.destroy
    respond_to do |format|
      format.html { redirect_to labour_migration_details_url, notice: 'Labour migration detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_labour_migration_detail
      @labour_migration_detail = LabourMigrationDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def labour_migration_detail_params
      params.require(:labour_migration_detail).permit(:farmer_id, :user_id, :socio_demographic_info_id, :gender, :duration, :distance_migrated, :place, :migration_purpose_id, :savings)
    end
end
