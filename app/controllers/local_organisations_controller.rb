class LocalOrganisationsController < ApplicationController
  before_action :set_local_organisation, only: [:show, :edit, :update, :destroy]

  # GET /local_organisations
  # GET /local_organisations.json
  def index
    @local_organisations = LocalOrganisation.all
    render layout: false
  end

  # GET /local_organisations/1
  # GET /local_organisations/1.json
  def show
  end

  # GET /local_organisations/new
  def new
    @local_organisation = LocalOrganisation.new
  end

  # GET /local_organisations/1/edit
  def edit
  end

  # POST /local_organisations
  # POST /local_organisations.json
  def create
    @local_organisation = LocalOrganisation.new(local_organisation_params)

    respond_to do |format|
      if @local_organisation.save
        format.html { redirect_to @local_organisation, notice: 'Local organisation was successfully created.' }
        format.json { render :show, status: :created, location: @local_organisation }
      else
        format.html { render :new }
        format.json { render json: @local_organisation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /local_organisations/1
  # PATCH/PUT /local_organisations/1.json
  def update
    respond_to do |format|
      if @local_organisation.update(local_organisation_params)
        format.html { redirect_to @local_organisation, notice: 'Local organisation was successfully updated.' }
        format.json { render :show, status: :ok, location: @local_organisation }
      else
        format.html { render :edit }
        format.json { render json: @local_organisation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /local_organisations/1
  # DELETE /local_organisations/1.json
  def destroy
    @local_organisation.destroy
    respond_to do |format|
      format.html { redirect_to local_organisations_url, notice: 'Local organisation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_local_organisation
      @local_organisation = LocalOrganisation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def local_organisation_params
      params.require(:local_organisation).permit(:name, :is_active)
    end
end
