class IntercropsController < ApplicationController
  before_action :set_intercrop, only: [:show, :edit, :update, :destroy]

  # GET /intercrops
  # GET /intercrops.json
  def index
    @intercrops = Intercrop.all
    render layout: false
  end

  # GET /intercrops/1
  # GET /intercrops/1.json
  def show
  end

  # GET /intercrops/new
  def new
    @intercrop = Intercrop.new
  end

  # GET /intercrops/1/edit
  def edit
  end

  # POST /intercrops
  # POST /intercrops.json
  def create
    @intercrop = Intercrop.new(intercrop_params)

    respond_to do |format|
      if @intercrop.save
        format.html { redirect_to @intercrop, notice: 'Intercrop was successfully created.' }
        format.json { render :show, status: :created, location: @intercrop }
      else
        format.html { render :new }
        format.json { render json: @intercrop.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /intercrops/1
  # PATCH/PUT /intercrops/1.json
  def update
    respond_to do |format|
      if @intercrop.update(intercrop_params)
        format.html { redirect_to @intercrop, notice: 'Intercrop was successfully updated.' }
        format.json { render :show, status: :ok, location: @intercrop }
      else
        format.html { render :edit }
        format.json { render json: @intercrop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /intercrops/1
  # DELETE /intercrops/1.json
  def destroy
    @intercrop.destroy
    respond_to do |format|
      format.html { redirect_to intercrops_url, notice: 'Intercrop was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_intercrop
      @intercrop = Intercrop.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def intercrop_params
      params.require(:intercrop).permit(:title)
    end
end
