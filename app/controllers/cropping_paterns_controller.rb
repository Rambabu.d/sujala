class CroppingPaternsController < ApplicationController
  before_action :set_cropping_patern, only: [:show, :edit, :update, :destroy]

  # GET /cropping_paterns
  # GET /cropping_paterns.json
  def index
    @cropping_paterns = params[:farmer_id].present? ? CroppingPatern.where(farmer_id: params[:farmer_id]) : CroppingPatern.all
    render layout: false
  end

  # GET /cropping_paterns/1
  # GET /cropping_paterns/1.json
  def show
  end

  # GET /cropping_paterns/new
  def new
    @cropping_patern = CroppingPatern.new
  end

  # GET /cropping_paterns/1/edit
  def edit
  end

  # POST /cropping_paterns
  # POST /cropping_paterns.json
  def create
    @cropping_patern = CroppingPatern.new(cropping_patern_params)

    respond_to do |format|
      if @cropping_patern.save
        format.html { redirect_to @cropping_patern, notice: 'Cropping patern was successfully created.' }
        format.json { render :show, status: :created, location: @cropping_patern }
      else
        format.html { render :new }
        format.json { render json: @cropping_patern.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cropping_paterns/1
  # PATCH/PUT /cropping_paterns/1.json
  def update
    respond_to do |format|
      if @cropping_patern.update(cropping_patern_params)
        format.html { redirect_to @cropping_patern, notice: 'Cropping patern was successfully updated.' }
        format.json { render :show, status: :ok, location: @cropping_patern }
      else
        format.html { render :edit }
        format.json { render json: @cropping_patern.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cropping_paterns/1
  # DELETE /cropping_paterns/1.json
  def destroy
    @cropping_patern.destroy
    respond_to do |format|
      format.html { redirect_to cropping_paterns_url, notice: 'Cropping patern was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cropping_patern
      @cropping_patern = CroppingPatern.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cropping_patern_params
      params.require(:cropping_patern).permit(:farmer_id, :survey_number_id, :total_land, :crop_id, :intercrop_id, :season)
    end
end
