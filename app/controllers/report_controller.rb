class ReportController < ApplicationController
  def index
  	render :layout => false 
  end

  def category
  	@category_reports = get_report("category")
  end

  def topography
  	@reports = get_report("topography")
    
  end

  def smu
    @smu_reports = get_report("smu")
  end

  def cultivation_cost

    @microwatershed_id = params[:micro_water_shed]
    @farmer_category = params[:farmer_category]
    @crop_list_id = params[:crop_list]
    @inter_crop_list_id = params[:inter_crop_list]

    @section_one = ActiveRecord::Base.connection.exec_query("CALL cost_of_cultivation(#{@microwatershed_id}, #{@crop_list_id}, #{@inter_crop_list_id}, '#{params[:farmer_category]}')")
    ActiveRecord::Base.clear_active_connections!

    @section_one = @section_one.to_hash
    #@section_one = CultivationCost.section_one(@microwatershed_id, params[:farmer_category], @crop_list_id, @inter_crop_list_id)

    @b1_15 = @section_one.find{|coc| coc['name'] == 'Interest on Investment'}
    @b1_16_list = ['Seed Main Crop', 'Seed Inter Crop', 'FYM', 'Fertilizer + micronutrients', 'Pesticides (PPC)', 'Risk Premium']
    @b1_16 = @section_one.select{|_coc| @b1_16_list.include?(_coc['name'])}.collect{|_coc| _coc['unit_cost']}.compact.sum * 0.12



    @b1_17_reject_list = ['Interest on Investment', 'Rental Value of Land', 'Family Human Labour', 'Risk Premium']
    @b1_17_total = @section_one.reject{|_coc| @b1_17_reject_list.include?(_coc['name']) }.collect{|_coc| _coc['unit_cost']}.compact.sum + @b1_15['unit_cost'] + @b1_16 unless @section_one.blank?
    
    @rental = @section_one.find{|coc| coc['name'] == 'Rental Value of Land'}

    # @crop_economics = CropEconomic.data(@microwatershed_id, @farmer_category, @crop_list_id, @inter_crop_list_id)
    # @gross_income = @crop_economics.collect{|a| a.unit_cost / a.area}.compact.sum unless @crop_economics.blank?
    @crop_economics = ActiveRecord::Base.connection.exec_query("CALL crop_economics(#{@microwatershed_id}, #{@crop_list_id}, #{@inter_crop_list_id}, '#{params[:farmer_category]}')").to_hash
    ActiveRecord::Base.clear_active_connections!

    @gross_income = @crop_economics.collect{|ce|  ce['name'].include?('quantity') ? ce['unit_cost'] : 0}.compact.sum unless @crop_economics.blank?

    # @family_labour = FamilyLabour.data(@microwatershed_id, @farmer_category, @crop_list_id, @inter_crop_list_id).first
    @family_labour = @section_one.find{|coc| coc['name'] == 'Family Human Labour'}

    # @risk_premia = RiskPremium.data(@microwatershed_id, @farmer_category, @crop_list_id, @inter_crop_list_id).first
    @risk_premia = @section_one.find{|coc| coc['name'] == 'Risk Premium'}

        @existing_main_crops = ProductionCost.joins(:farmer => {:village => :micro_water_shed})
      .where("villages.micro_water_shed_id" => @microwatershed_id)
      .joins(:crop_list).pluck('crop_lists.name').uniq


    render :layout => false
  end


  def report_list
    @list = REPORT.keys
    render json: @list
  end

  def farmer_count
    @report = REPORT[params[:report_type]]["ctrl"].classify.constantize
    @farmer_count = @report.farmer_count(params[:type], params[:micro_water_shed], REPORT[params[:report_type]])
    render json: @farmer_count
  end

  def live_stock
    @data = LiveStockReport.data(params[:micro_water_shed])
    render :layout => false
  end

  def marketing
    micro_water_shed = params[:micro_water_shed]
    @pc_yield = PcYieldPrice.joins(:farmer => {:village => :micro_water_shed}).where("villages.micro_water_shed_id" => micro_water_shed)
    @marketing = Marketing.joins(:farmer => {:village => :micro_water_shed}).where("villages.micro_water_shed_id" => micro_water_shed)
    # @op_obtained = @marketing.joins(:crop).group("crop_lists.name").sum(:output_obtained)
    @op_obtained  = @pc_yield.joins(:production_cost => :crop_list).group('crop_lists.name').sum(:crop_main_product_yield)
    # @op_retained = @marketing.joins(:crop).group("crop_lists.name").sum(:output_retained)
    
    @op_sold = @marketing.joins(:crop).group("crop_lists.name").sum(:output_sold)
    @price = @pc_yield.joins(:production_cost => :crop_list).group('crop_lists.name').sum(:crop_main_product_price)
    @fcount = @marketing.joins(:crop).group("crop_lists.name").count

     render :layout => false
  end

  def comments
    @comments = WatershedComment.data(params[:micro_water_shed])
    render :layout => false
  end

  def main_yield
    _dry = MajorYield.joins(:farmer => :village).where("villages.micro_water_shed_id = #{params[:micro_water_shed]}")
      .where(:type_of_land => "Dry")
     @main_yield_dry = _dry.group(:name).sum(:main_yield)
      @main_yield_dry_area = _dry.group(:name).sum(:extent_of_area)
    @potential_yield_dry = _dry.group(:name).sum(:potential_yield)
      @potential_yield_dry_count = _dry.group(:name).count
    _irrigated = MajorYield.joins(:farmer => :village).where("villages.micro_water_shed_id = #{params[:micro_water_shed]}")
      .where(:type_of_land => "Irrigated")
    @main_yield_irrigated = _irrigated.group(:name).sum(:main_yield)
      @main_yield_irrigated_area = _irrigated.group(:name).sum(:extent_of_area)
    @potential_yield_irrigated = _irrigated.group(:name).sum(:potential_yield)
      @potential_yield_irrigated_count = _irrigated.group(:name).count

    render :layout => false
  end

  def soil_status
    @statuses = ConversationPractice.joins(:farmer => :village).where("villages.micro_water_shed_id = #{params[:micro_water_shed]}").joins(:conversation_structure).group('conversation_structures.name', :status).count
    render :layout => false
  end

  def additional_invst_src
    @total = Farmer.joins(:village).where("villages.micro_water_shed_id" => params[:micro_water_shed]).count
    @srcs = AdditionalInvestmentCapacity.joins(:farmer => :village).where("villages.micro_water_shed_id = #{params[:micro_water_shed]}").joins(:purpose).group(:source_of_funds, 'purposes.name').count
    render :layout => false
  end

  private

  def get_report type
    @category_reports = REPORT[params[:report_type]]["ctrl"].classify.constantize.send(type, params[:report_type], params[:micro_water_shed])
    @category_reports = @category_reports.collect{|report| OpenStruct.new(report)}
  end

  def farmer_params
    params.require(:report).permit(:report_type, :micro_water_shed)
  end

  
end
