class ConversationStructuresController < ApplicationController
  before_action :set_conversation_structure, only: [:show, :edit, :update, :destroy]

  # GET /conversation_structures
  # GET /conversation_structures.json
  def index
    @conversation_structures = ConversationStructure.all
    render layout: false
  end

  # GET /conversation_structures/1
  # GET /conversation_structures/1.json
  def show
  end

  # GET /conversation_structures/new
  def new
    @conversation_structure = ConversationStructure.new
  end

  # GET /conversation_structures/1/edit
  def edit
  end

  # POST /conversation_structures
  # POST /conversation_structures.json
  def create
    @conversation_structure = ConversationStructure.new(conversation_structure_params)

    respond_to do |format|
      if @conversation_structure.save
        format.html { redirect_to @conversation_structure, notice: 'Conversation structure was successfully created.' }
        format.json { render :show, status: :created, location: @conversation_structure }
      else
        format.html { render :new }
        format.json { render json: @conversation_structure.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conversation_structures/1
  # PATCH/PUT /conversation_structures/1.json
  def update
    respond_to do |format|
      if @conversation_structure.update(conversation_structure_params)
        format.html { redirect_to @conversation_structure, notice: 'Conversation structure was successfully updated.' }
        format.json { render :show, status: :ok, location: @conversation_structure }
      else
        format.html { render :edit }
        format.json { render json: @conversation_structure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversation_structures/1
  # DELETE /conversation_structures/1.json
  def destroy
    @conversation_structure.destroy
    respond_to do |format|
      format.html { redirect_to conversation_structures_url, notice: 'Conversation structure was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conversation_structure
      @conversation_structure = ConversationStructure.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conversation_structure_params
      params.require(:conversation_structure).permit(:name)
    end
end
