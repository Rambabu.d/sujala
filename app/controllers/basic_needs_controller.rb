class BasicNeedsController < ApplicationController
  before_action :set_basic_need, only: [:show, :edit, :update, :destroy]

  # GET /basic_needs
  # GET /basic_needs.json
  def index
    @basic_needs = params[:farmer_id].present? ? BasicNeed.where(farmer_id: params[:farmer_id]) : BasicNeed.all
    render layout: false
  end

  # GET /basic_needs/1
  # GET /basic_needs/1.json
  def show
  end

  # GET /basic_needs/new
  def new
    @basic_need = BasicNeed.new
  end

  # GET /basic_needs/1/edit
  def edit
  end

  # POST /basic_needs
  # POST /basic_needs.json
  def create
    #BasicNeed.where(farmer_id: params[:farmer_id]).destroy_all
    @basic_need = BasicNeed.new(basic_need_params)

    respond_to do |format|
      if @basic_need.save
        format.html { redirect_to @basic_need, notice: 'Basic need was successfully created.' }
        format.json { render :show, status: :created, location: @basic_need }
      else
        format.html { render :new }
        format.json { render json: @basic_need.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /basic_needs/1
  # PATCH/PUT /basic_needs/1.json
  def update
    respond_to do |format|
      if @basic_need.update(basic_need_params)
        format.html { redirect_to @basic_need, notice: 'Basic need was successfully updated.' }
        format.json { render :show, status: :ok, location: @basic_need }
      else
        format.html { render :edit }
        format.json { render json: @basic_need.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basic_needs/1
  # DELETE /basic_needs/1.json
  def destroy
    @basic_need.destroy
    respond_to do |format|
      format.html { redirect_to basic_needs_url, notice: 'Basic need was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic_need
      @basic_need = BasicNeed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def basic_need_params
      params.require(:basic_need).permit(:minimum_need, :farmer_id, :response, :source, :distance)
    end
end
