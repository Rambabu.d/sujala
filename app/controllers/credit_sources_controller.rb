class CreditSourcesController < ApplicationController
  before_action :set_credit_source, only: [:show, :edit, :update, :destroy]

  # GET /credit_sources
  # GET /credit_sources.json
  def index
    @credit_sources = CreditSource.all
    render layout: false
  end

  # GET /credit_sources/1
  # GET /credit_sources/1.json
  def show
  end

  # GET /credit_sources/new
  def new
    @credit_source = CreditSource.new
  end

  # GET /credit_sources/1/edit
  def edit
  end

  # POST /credit_sources
  # POST /credit_sources.json
  def create
    @credit_source = CreditSource.new(credit_source_params)

    respond_to do |format|
      if @credit_source.save
        format.html { redirect_to @credit_source, notice: 'Credit source was successfully created.' }
        format.json { render :show, status: :created, location: @credit_source }
      else
        format.html { render :new }
        format.json { render json: @credit_source.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /credit_sources/1
  # PATCH/PUT /credit_sources/1.json
  def update
    respond_to do |format|
      if @credit_source.update(credit_source_params)
        format.html { redirect_to @credit_source, notice: 'Credit source was successfully updated.' }
        format.json { render :show, status: :ok, location: @credit_source }
      else
        format.html { render :edit }
        format.json { render json: @credit_source.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /credit_sources/1
  # DELETE /credit_sources/1.json
  def destroy
    @credit_source.destroy
    respond_to do |format|
      format.html { redirect_to credit_sources_url, notice: 'Credit source was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_source
      @credit_source = CreditSource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_source_params
      params.require(:credit_source).permit(:name)
    end
end
