class ForestrySpeciesController < ApplicationController
  before_action :set_forestry_specy, only: [:show, :edit, :update, :destroy]

  # GET /forestry_species
  # GET /forestry_species.json
  def index
    @forestry_species = params[:farmer_id].present? ? ForestrySpecy.where(farmer_id: params[:farmer_id]) : ForestrySpecy.all
    render layout: false
  end

  # GET /forestry_species/1
  # GET /forestry_species/1.json
  def show
  end

  # GET /forestry_species/new
  def new
    @forestry_specy = ForestrySpecy.new
  end

  # GET /forestry_species/1/edit
  def edit
  end

  # POST /forestry_species
  # POST /forestry_species.json
  def create
    @forestry_specy = ForestrySpecy.new(forestry_specy_params)

    respond_to do |format|
      if @forestry_specy.save
        format.html { redirect_to @forestry_specy, notice: 'Forestry specy was successfully created.' }
        format.json { render :show, status: :created, location: @forestry_specy }
      else
        format.html { render :new }
        format.json { render json: @forestry_specy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forestry_species/1
  # PATCH/PUT /forestry_species/1.json
  def update
    respond_to do |format|
      if @forestry_specy.update(forestry_specy_params)
        format.html { redirect_to @forestry_specy, notice: 'Forestry specy was successfully updated.' }
        format.json { render :show, status: :ok, location: @forestry_specy }
      else
        format.html { render :edit }
        format.json { render json: @forestry_specy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forestry_species/1
  # DELETE /forestry_species/1.json
  def destroy
    @forestry_specy.destroy
    respond_to do |format|
      format.html { redirect_to forestry_species_url, notice: 'Forestry specy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forestry_specy
      @forestry_specy = ForestrySpecy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forestry_specy_params
      params.require(:forestry_specy).permit(:farmer_id, :forestry_plant_id, :field, :backyard)
    end
end
