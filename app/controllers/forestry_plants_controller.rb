class ForestryPlantsController < ApplicationController
  before_action :set_forestry_plant, only: [:show, :edit, :update, :destroy]

  # GET /forestry_plants
  # GET /forestry_plants.json
  def index
    @forestry_plants = ForestryPlant.all
    render layout: false
  end

  # GET /forestry_plants/1
  # GET /forestry_plants/1.json
  def show
  end

  # GET /forestry_plants/new
  def new
    @forestry_plant = ForestryPlant.new
  end

  # GET /forestry_plants/1/edit
  def edit
  end

  # POST /forestry_plants
  # POST /forestry_plants.json
  def create
    @forestry_plant = ForestryPlant.new(forestry_plant_params)

    respond_to do |format|
      if @forestry_plant.save
        format.html { redirect_to @forestry_plant, notice: 'Forestry plant was successfully created.' }
        format.json { render :show, status: :created, location: @forestry_plant }
      else
        format.html { render :new }
        format.json { render json: @forestry_plant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forestry_plants/1
  # PATCH/PUT /forestry_plants/1.json
  def update
    respond_to do |format|
      if @forestry_plant.update(forestry_plant_params)
        format.html { redirect_to @forestry_plant, notice: 'Forestry plant was successfully updated.' }
        format.json { render :show, status: :ok, location: @forestry_plant }
      else
        format.html { render :edit }
        format.json { render json: @forestry_plant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forestry_plants/1
  # DELETE /forestry_plants/1.json
  def destroy
    @forestry_plant.destroy
    respond_to do |format|
      format.html { redirect_to forestry_plants_url, notice: 'Forestry plant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forestry_plant
      @forestry_plant = ForestryPlant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forestry_plant_params
      params.require(:forestry_plant).permit(:name)
    end
end
