@app.controller 'MigrationConsequenceCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/migration_consequences.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "migration_positive_consequences")
    farmer.get($scope, "migration_negative_consequences")

    farmer.index($scope).then (response) ->
      $scope.vm = _.last $scope.data_list
      if $scope.vm
        $scope.btns(false, true, true)


    $scope.parameter = () ->
      JSON.stringify(migration_consequence: {
        farmer_id: $scope.farmer.id,
        migration_positive_consequence_id: $scope.vm.migration_positive_consequence.id,
        migration_negative_consequence_id: $scope.vm.migration_negative_consequence.id,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)
    $scope.submit = ->
      farmer.save($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        $scope.btns(false, true, true)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]