@app.controller 'WageRatesCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/pc_wage_rates.json" + "?farmer_id=" + $scope.farmer.id

    $http.get("/production_costs.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.production_costs = response.data

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(pc_wage_rate: {
        farmer_id: $scope.farmer.id,
        production_cost_id: $scope.vm.production_cost.id,
        male: $scope.vm.male,
        female: $scope.vm.female,
        bullock_pair: $scope.vm.bullock_pair,
        tractor: $scope.vm.tractor,
        machinery: $scope.vm.machinery,
        irrigation_charge: $scope.vm.irrigation_charge,
        value: $scope.vm.value,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)

    $scope.submit = ->
      farmer.save($scope)

    $scope.cancel = () -> farmer.cancel($scope)
    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]