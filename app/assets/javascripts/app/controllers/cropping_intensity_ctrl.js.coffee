@app.controller 'CroppingIntensityCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/cropping_intensities.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "crops")
    farmer.get($scope, "intercrops")

    $http.get("/survey_numbers.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.survey_numbers = response.data

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(cropping_intensity: {
        farmer_id: $scope.farmer.id,
        survey_number_id: $scope.vm.survey_number.id,
        total_land: $scope.vm.total_land,
        kharif: $scope.vm.kharif,
        rabi: $scope.vm.rabi,
        summer: $scope.vm.summer,
        crop_id: $scope.vm.crop.id,
        intercrop_id: $scope.vm.intercrop.id,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)

    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]