'use strict'

###*
# @ngdoc function
# @name fakeLunchHubApp.controller:UserRegistrationsCtrl
# @description
# # UserRegistrationsCtrl
# Controller of the fakeLunchHubApp
###

@app.controller 'UserRegistrationsCtrl', [
  '$scope'
  '$location'
  '$auth'
  ($scope, $location, $auth) ->
    $scope.handleRegBtnClick = ->
      $auth.submitRegistration($scope.registrationForm).then ->
        $auth.submitLogin
          email: $scope.registrationForm.email
          password: $scope.registrationForm.password

    $scope.$on 'auth:registration-email-error', (ev, reason) ->
      $scope.error = reason
      console.log reason
]