@app.controller 'OtherExpensesCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/pc_other_expenses.json" + "?farmer_id=" + $scope.farmer.id

    $http.get("/production_costs.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.production_costs = response.data


    farmer.index($scope)

    $scope.parameter = () ->
      JSON.stringify(pc_other_expense: {
        farmer_id: $scope.farmer.id,
        production_cost_id: $scope.vm.production_cost.id,
        electricity: $scope.vm.electricity,
        insurance: $scope.vm.insurance,
        irrigated_rental_value: $scope.vm.irrigated_rental_value,
        dryland_rental_value: $scope.vm.dryland_rental_value,
        managerial_cost: $scope.vm.managerial_cost,
        land_revenue: $scope.vm.land_revenue,
        seed_price_main: $scope.vm.seed_price_main,
        seed_price_inter: $scope.vm.seed_price_inter,
        interest_on_variable_cost: $scope.vm.interest_on_variable_cost,
        interest_on_fixed_cost: $scope.vm.interest_on_fixed_cost,
        miscellaneous: $scope.vm.miscellaneous,
        repairs_and_maintenance_charges: $scope.vm.repairs_and_maintenance_charges,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)

    $scope.submit = ()-> farmer.save($scope)
    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]