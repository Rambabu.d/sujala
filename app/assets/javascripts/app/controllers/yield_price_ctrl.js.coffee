@app.controller 'YieldPriceCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/pc_yield_prices.json" + "?farmer_id=" + $scope.farmer.id

    $http.get("/production_costs.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.production_costs = response.data

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(pc_yield_price: {
        farmer_id: $scope.farmer.id,
        production_cost_id: $scope.vm.production_cost.id,
        crop_main_product_yield: $scope.vm.crop_main_product_yield,
        crop_main_product_price: $scope.vm.crop_main_product_price,
        crop_by_product_yield: $scope.vm.crop_by_product_yield,
        crop_by_product_price: $scope.vm.crop_by_product_price,
        intercrop_main_product_yield: $scope.vm.intercrop_main_product_yield,
        intercrop_main_product_price: $scope.vm.intercrop_main_product_price,
        intercrop_by_product_yield: $scope.vm.intercrop_by_product_yield,
        intercrop_by_product_price: $scope.vm.intercrop_by_product_price,
        value: $scope.vm.value,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]
