@app.controller 'IrrigationCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/irrigations.json" + "?farmer_id=" + $scope.farmer.id

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(irrigation: {
        farmer_id: $scope.farmer.id,
        source_of_irrigation: $scope.vm.source_of_irrigation,
        defunct: $scope.vm.defunct,
        working: $scope.vm.working,
        water_depth: $scope.vm.water_depth,
        yield: $scope.vm.yield,
        kharif: $scope.vm.kharif,
        rabi: $scope.vm.rabi,
        summer: $scope.vm.summer,
        perennial_cops: $scope.vm.perennial_cops,
        water_quality: $scope.vm.water_quality,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]