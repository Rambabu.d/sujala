@app.controller 'HorticultureForestryPlantCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  ($scope, $http, $templateCache) ->

    $http(
      method: "get"
      url: "/horticulture_forestry_plants.json"
      cache: $templateCache).then ((response) ->
        $scope.data_list = response.data
    )

    $scope.submit = ->
      parameter = JSON.stringify(horticulture_forestry_plant: {
        horticuluture_plant: $scope.horticuluture_plant_id,
        horti_plant_field: $scope.horti_plant_field,
        horti_plant_backyard: $scope.horti_plant_backyard,
        horticuluture_plant_with_number: $scope.horticuluture_plant_with_number,
        horticuluture_plant_number: $scope.horticuluture_plant_number,
        intrested_in_horti_cultivate: $scope.intrested_in_horti_cultivate,
        forestry_plant: $scope.forestry_plant_id,
        forestry_plant_field: $scope.forestry_plant_field,
        forestry_plant_backyard: $scope.forestry_plant_backyard,
        forestry_plant_with_number: $scope.forestry_plant_with_number,
        forestry_plant_number: $scope.forestry_plant_number,
        intrested_in_forty_cultivate: $scope.intrested_in_forty_cultivate,
        })
      $http(
        method: 'POST'
        url: "/horticulture_forestry_plants.json"
        data: parameter).then (response) ->
          $scope.data_list.push(response.data)

          $scope.horticuluture_plant_id = ''
          $scope.horti_plant_field = ''
          $scope.horti_plant_backyard = ''
          $scope.horticuluture_plant_with_number = ''
          $scope.horticuluture_plant_number = ''
          $scope.intrested_in_horti_cultivate = ''
          $scope.forestry_plant = ''
          $scope.forestry_plant_field = ''
          $scope.forestry_plant_backyard = ''
          $scope.forestry_plant_with_number = ''
          $scope.forestry_plant_number = ''
          $scope.intrested_in_forty_cultivate = ''
]