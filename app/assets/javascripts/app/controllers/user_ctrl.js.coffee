@app.controller 'UserCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  '$location'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, $location, farmer, $cacheFactory, $localStorage) ->

    $scope.url = "/users/index.json"

    #farmer.index($scope) #Data will pulled to variable data_list
    $http.get($scope.url).then (response) ->
        $scope.data_list = response.data

    #$scope.parameter = () ->
    #  JSON.stringify(house: {
    #    farmer_id: $scope.farmer.id,
    #    house_type_id: $scope.vm.house_type.id,
    #    quantity: $scope.vm.quantity,
    #    area: $scope.vm.area
    #  })

    $scope.submit = ->
      farmer.save($scope)

    $scope.get_user = (id) ->
      $location.path("/user_roles/" + id)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
]


