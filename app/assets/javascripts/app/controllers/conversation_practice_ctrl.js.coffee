@app.controller 'ConversationPracticeCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->

    $scope.farmer = $localStorage.farmer

    $scope.url = "/conversation_practices.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "conversation_structures")

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(conversation_practice: {
        farmer_id: $scope.farmer.id,
        conversation_structure_id: $scope.vm.conversation_structure.id,
        year: $scope.vm.year,
        status: $scope.vm.status,
        output_sold: $scope.vm.output_sold,
        implemented_by: $scope.vm.implemented_by,
        name_of_agency: $scope.vm.name_of_agency,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)

    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]