@app.controller 'LabourMigrationsCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/labour_migration_details.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "migration_purposes")
    #farmer.get($scope, "socio_demographic_infos")
    $http.get("/socio_demographic_infos.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.socio_demographic_infos = response.data

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(labour_migration_detail: {
        farmer_id: $scope.farmer.id,
        socio_demographic_info_id: $scope.vm.socio_demographic_info.id,
        gender: $scope.vm.socio_demographic_info.gender.name,
        duration: $scope.vm.duration,
        distance_migrated: $scope.vm.distance_migrated,
        place: $scope.vm.place,
        migration_purpose_id: $scope.vm.migration_purpose.id,
        savings: $scope.vm.savings,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
    $scope.change =  () ->
      $scope.vm.gender = $scope.vm.socio_demographic_info.gender.name
]