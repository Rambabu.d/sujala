@app.controller 'SurveyCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/surveys.json" + "?farmer_id=" + $scope.farmer.id

    $http.get("/survey_numbers.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.survey_numbers = response.data
    #Data will pulled to variable data_list
    farmer.index($scope)
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(survey: {
        farmer_id: $scope.farmer.id,
        survey_number_id: $scope.vm.survey_number.id,
        type_of_land: $scope.vm.type_of_land,
        holding_acres: $scope.vm.holding_acres,
        present_land_value: $scope.vm.present_land_value,
        soil_type: $scope.vm.soil_type,
        quality: $scope.vm.quality,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]