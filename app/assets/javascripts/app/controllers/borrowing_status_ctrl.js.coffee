@app.controller 'BorrowingStatusCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->

    $scope.farmer = $localStorage.farmer

    $scope.url = "/borrowing_statuses.json" + "?farmer_id=" + $scope.farmer.id

    farmer.index($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        if $scope.vm
          $scope.btns(false, true, true)

    $scope.parameter = () ->
      JSON.stringify(borrowing_status: {
        farmer_id: $scope.farmer.id,
        have_bank_account: $scope.vm.have_bank_account,
        have_savings: $scope.vm.have_savings,
        credit_availed: $scope.vm.credit_availed,
        amount: $scope.vm.amount,
        amount_availed_adequate: $scope.vm.amount_availed_adequate
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)

    $scope.submit = ()->
      farmer.save($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        $scope.btns(false, true, true)

    $scope.cancel = () -> farmer.cancel($scope)

    #$scope.delete = (id, url) -> farmer.delete($scope, id, url)
]