@app.controller 'ProductionCostCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/production_costs.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "crop_lists")
    farmer.get($scope, "intercrop_lists")

    #farmer.index($scope) #Data will pulled to variable data_list
    farmer.index($scope)

    $scope.parameter = () -> 
      JSON.stringify(production_cost: {
        farmer_id: $scope.farmer.id,
        crop_number: $scope.vm.crop_number,
        crop_list_id: $scope.vm.crop_list.id,
        intercrop_list_id: $scope.vm.intercrop_list.id,
        extent_of_area: $scope.vm.extent_of_area,
        season: $scope.vm.season,
        type_of_land: $scope.vm.type_of_land,
        irrigation_method: $scope.vm.irrigation_method,
        establishment: $scope.vm.establishment,
        annual_maintenance: $scope.vm.annual_maintenance,
        age_of_garden: $scope.vm.age_of_garden,
        lifespan_of_garden: $scope.vm.lifespan_of_garden,
        seed_qty: $scope.vm.seed_qty,
        seed_price_inter: $scope.vm.seed_price_inter,
        plants_count: $scope.vm.plants_count,
        seeding_count: $scope.vm.seeding_count,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)
    $scope.submit = ()-> farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)

    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()

    $scope.crop_numbers = [1,2,3,4,5,6,7,8];
]