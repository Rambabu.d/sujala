@app.controller 'RoleRactionsCtrl', [
  '$scope'
  '$http'
  '$routeParams'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $routeParams, $templateCache, farmer, $cacheFactory, $localStorage) ->

    #$scope.farmer = $localStorage.farmer
    $http.get("/roles/role_ractions.json" + "?role_id=" + $routeParams.id).then (response) ->
      $scope.role_ractions = response.data

    $http.get("/ractions.json" ).then (response) ->
      $scope.ractions = response.data

    $http.get("roles/role.json?role_id=" + $routeParams.id).then (response) ->
      $scope.role = response.data

    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(role_raction: {
        raction_id: $scope.raction_id,
        role_id: $routeParams.id
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = (id, checked_value)->
      $scope.raction_id = id
      if checked_value is 'true'
        $http.post("/roles/create_role_raction", $scope.parameter()).then (response) ->
          $scope.message =  "Added to " + $scope.role.name
      else if checked_value is 'false'
        $http.post("/roles/delete_role_raction", {role_id: $routeParams.id, raction_id: $scope.raction_id}).then (response) ->   
          $scope.message = "Deleted for " + $scope.role.name
        #$scope.user_roles.push($scope.vm.role.name)
        #$scope.vm = {}
        #self.refresh_cache(scope)
    $scope.checked = (id) ->
      _.includes(_.map($scope.role_ractions, 'raction_id'), id)

    $scope.delete = (role) ->
      #raction_delete = _.find($scope.roles, {name: role}).id
      #$http.post("/roles/delete_role_raction", {user_id: $routeParams.id, role_id: role_delete}).then (response) ->
      #  _.pull($scope.user_roles, role)
]

