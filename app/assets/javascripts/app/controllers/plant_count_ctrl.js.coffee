@app.controller 'PlantCountCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer
    $scope.url = "/plant_counts.json" + "?farmer_id=" + $scope.farmer.id
    farmer.get($scope, "horticulture_plants")
    farmer.index($scope)

    #console.log($scope)
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.filter_list = (item) ->
    #  !_.includes(_.map($scope.data_list, "horticulture_plant.id"), item.id)

    $scope.parameter = () ->
      JSON.stringify(plant_count: {
        farmer_id: $scope.farmer.id,
        horticulture_plant_id: $scope.vm.horticulture_plant.id,
        field: $scope.vm.field,
        backyard: $scope.vm.backyard,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
]

