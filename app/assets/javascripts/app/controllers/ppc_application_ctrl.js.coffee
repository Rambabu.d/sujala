@app.controller 'PpcApplicationCtrl', [
  '$rootScope'
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($rootScope, $scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.is_authenticated = $rootScope.is_authenticated

    $scope.url = "/ppc_details.json" + "?farmer_id=" + $scope.farmer.id

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $http.get("/production_costs.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.production_costs = response.data

    $scope.parameter = () -> 
      JSON.stringify(ppc_detail: {
        farmer_id: $scope.farmer.id,
        production_cost_id: $scope.vm.production_cost.id,
        details_of_ppc_application: $scope.vm.details_of_ppc_application,
        own_men_mandays: $scope.vm.own_men_mandays,
        hired_men_mandays: $scope.vm.hired_men_mandays,
        own_women_mandays: $scope.vm.own_women_mandays,
        hired_women_mandays: $scope.vm.hired_women_mandays,
        own_bullock_days: $scope.vm.own_bullock_days,
        hired_bullock_days: $scope.vm.hired_bullock_days,
        own_tractor_days: $scope.vm.own_tractor_days,
        hired_tractor_days: $scope.vm.hired_tractor_days,
        sprays_count: $scope.vm.sprays_count,
        input: $scope.vm.input,
        amount: $scope.vm.amount,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]