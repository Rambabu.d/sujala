@app.controller 'MicroWaterShedCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/micro_water_sheds.json"

    farmer.get($scope, "sub_water_sheds")

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(micro_water_shed: {
        name: $scope.vm.name,
        sub_water_shed_id: $scope.vm.sub_water_shed.id,
        code: $scope.vm.code,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
]