@app.controller 'MachinesCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/machines.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "machine_types")

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(machine: {
        farmer_id: $scope.farmer.id,
        machine_type_id: $scope.vm.machine_type.id,
        quantity: $scope.vm.quantity,
        present_value: $scope.vm.present_value,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]