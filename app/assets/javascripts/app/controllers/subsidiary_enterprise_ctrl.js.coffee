@app.controller 'SubsidiaryEnterpriseCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/subsidiary_enterprises.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "livestock_names")

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(subsidiary_enterprise: {
        farmer_id: $scope.farmer.id,
        livestock_name_id: $scope.vm.livestock_name.id,
        no_of_animals: $scope.vm.no_of_animals,
        no_of_milching_animals: $scope.vm.no_of_milching_animals,
        present_value_of_animals: $scope.vm.present_value_of_animals,
        monthly_feed_dry: $scope.vm.monthly_feed_dry,
        monthly_feed_green: $scope.vm.monthly_feed_green,
        monthly_feed_conc: $scope.vm.monthly_feed_conc,
        labour_no_of_hrs: $scope.vm.labour_no_of_hrs,
        labour_amount: $scope.vm.labour_amount,
        healthcare_expenses: $scope.vm.healthcare_expenses,
        milk: $scope.vm.milk,
        lacation_period: $scope.vm.lacation_period,
        manure: $scope.vm.manure,
        price_per_unit: $scope.vm.price_per_unit,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
]