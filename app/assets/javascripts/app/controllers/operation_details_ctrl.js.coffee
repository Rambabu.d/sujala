@app.controller 'OperationDetailsCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/pc_operation_details.json" + "?farmer_id=" + $scope.farmer.id

    $http.get("/production_costs.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.production_costs = response.data

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(pc_operation_detail: {
        farmer_id: $scope.farmer.id,
        production_cost_id: $scope.vm.production_cost.id,
        operation_detail: $scope.vm.operation_detail,
        own_men: $scope.vm.own_men,
        own_women: $scope.vm.own_women,
        hired_men: $scope.vm.hired_men,
        hired_women: $scope.vm.hired_women,
        own_bullock: $scope.vm.own_bullock,
        hired_bullock: $scope.vm.hired_bullock,
        own_tractor: $scope.vm.own_tractor,
        hired_tractor: $scope.vm.hired_tractor,
        own_machinery: $scope.vm.own_machinery,
        hired_machinery: $scope.vm.hired_machinery,
        input: $scope.vm.input,
        amount: $scope.vm.amount,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]