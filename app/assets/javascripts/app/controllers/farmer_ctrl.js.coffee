@app.controller 'FarmerCtrl', [
  '$window'
  '$scope'
  'toastr'
  '$http'
  '$location'
  'farmer'
  'user'
  '$cacheFactory'
  '$localStorage'
  '$rootScope'
  '$route'
  ($window,$scope, toastr, $http, $location,farmer, user, $cacheFactory, $localStorage, $rootScope, $route) ->

    if $route.current.params.action == "new_farmer"
      $localStorage.farmer = undefined
      $rootScope.$broadcast("_farmer_")

    $scope.vm = $localStorage.farmer

    $scope.user = user
    $scope.user_params = $localStorage.current_user.id
    $scope.url = "/farmers.json"
    $scope.$on 'pagination:loadPage', (event, status, config) ->
      $scope.page_range = event.targetScope.rangeFrom
      jQuery(".paginate-anything").find(".per-page").hide()

      # status is the HTTP status of the result


    # Farmer Contoller is used twise, meaning it has
    # 2 instaces these lines will help to sync the data
    # between 2/multiple instances of FarmerCtrl
    #
    farmer.get($scope, "smus")
    farmer.get($scope, "topographies")
    farmer.get($scope, "castes")
    farmer.get($scope, "districts")

    $scope.get_sub_water_sheds_index = ()->
      farmer.get_with_params($scope, "sub_water_sheds", "?district_id=" + $scope.vm.district.id)
    $scope.get_micro_water_sheds_index = ()->
      farmer.get_with_params($scope, "micro_water_sheds", "?sub_water_shed_id=" + $scope.vm.sub_water_shed.id)  

    $scope.search_btn = () ->
      $scope.far_url = "/farmers.json" + "?micro_water_shed=" + $scope.vm.micro_water_shed.id + "&user=" + $scope.user_params
      $http.get($scope.far_url).then (far_response) ->
        $scope.data_list = far_response.data

    $scope.get_taluks = () ->
      # This is cached initially in app run
      farmer.get($scope, "taluks").then (response) ->
        $scope.taluks =
          _.filter $scope.taluks, {district_id: $scope.vm.district.id}
      farmer.get($scope, "sub_water_sheds").then (response) ->
        $scope.sub_water_sheds =
          _.filter $scope.sub_water_sheds, {district_id: $scope.vm.district.id}

    $scope.get_hoblis = () ->
      farmer.get($scope, "hoblis").then (response) ->
        $scope.hoblis = _.filter $scope.hoblis, {taluk_id: $scope.vm.taluk.id }

    $scope.get_grampanchayats = () ->
      farmer.get($scope, "grampanchayats").then (response) ->
        $scope.grampanchayats =
          _.filter($scope.grampanchayats,
            {hobli_id: $scope.vm.hobli.id })

    $scope.get_micro_water_sheds = () ->
      farmer.get($scope, "micro_water_sheds").then (response) ->
        $scope.micro_water_sheds =
          _.filter($scope.micro_water_sheds,
          {sub_water_shed_id: $scope.vm.sub_water_shed.id })

    $scope.get_villages = () ->
      farmer.get($scope, "villages").then (response) ->
        $scope.villages =
          _.filter($scope.villages,
            {micro_water_shed_id: $scope.vm.micro_water_shed.id })

    unless $rootScope.net_status
      farmer.index($scope) #Data will be pulled to variable data_list

    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    if $scope.vm
      $scope.get_taluks()
      $scope.get_hoblis()
      $scope.get_grampanchayats()
      $scope.get_micro_water_sheds()
      $scope.get_villages()
      $scope.btns(false, true, true)

    #$scope.data_list = []
    $scope.parameter = () ->
      JSON.stringify(
        farmer: {
          name: $scope.vm.name ,
          father_name: $scope.vm.father_name,
          village_id: $scope.vm.village.id,
          grampanchayat_id: $scope.vm.grampanchayat.id,
          hamlet: $scope.vm.hamlet,
          phone: $scope.vm.phone,
          caste_id: $scope.vm.caste.id,
          smu_id: $scope.vm.smu.id,
          topography_id: $scope.vm.topography.id,
          aadhar: $scope.vm.aadhar,
          user_id: $localStorage.current_user.id
        }
      )

    $scope.submit = ->
      self = @
      farmer.save($scope).then (response) ->
        _farmer = $scope.farmer_data

        unless $rootScope.net_status
          farmer.index_local($scope).then (response) ->
            self.set_farmer(_.last(response))
        else
          self.set_farmer(_farmer)
        #toastr.success('Hello world!', 'FarmerCTRL')

    $scope.get_farmer = (id) ->
      self = @
      _farmer = _.find $scope.data_list, (data) ->
        data.id == id
      self.set_farmer(_farmer)
      $location.path("/1")

    $scope.edit = (id)-> farmer.edit($scope, id)

    $scope.update = () ->
      self = @
      farmer.update($scope).then (response) ->
        self.set_farmer($scope.updated)
        self.btns(false, true, true)
        _val = _.find($scope.data_list, {id: $scope.updated.id})
        _index = _.indexOf($scope.data_list, _val)
        $scope.data_list.splice(_index, 1, $scope.updated)
        farmer.refresh_cache($scope)

    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () ->
      farmer.next()
      #$window.location.assign('/2')

    $scope.prev = () -> farmer.prev()

    $scope.set_farmer = (farmer) ->
      $scope.vm = farmer
      $localStorage.farmer = $scope.vm
      $rootScope.$broadcast("_farmer_")
      cache = $cacheFactory.get("$http")
      cache.put("farmer", $scope.vm)
]