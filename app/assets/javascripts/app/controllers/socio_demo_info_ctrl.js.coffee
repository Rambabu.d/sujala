@app.controller 'SocioDemoInfoCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/socio_demographic_infos.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "educations")
    farmer.get($scope, "occupations")
    farmer.get($scope, "genders")
    farmer.get($scope, "local_organisations")

    #farmer.index($scope) #Data will pulled to variable data_list
    farmer.index($scope).then (response) ->
      if $scope.data_list.length == 0
        $scope.vm = {}
        $scope.vm.farmer_member_name = $scope.farmer.name

    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(
        socio_demographic_info: {
          farmer_id: $scope.farmer.id,
          farmer_member_name: $scope.vm.farmer_member_name ,
          farmer_relation_id: $scope.vm.farmer_relation_id,
          occupation_id: $scope.vm.occupation.id,
          education_id: $scope.vm.education.id,
          age: $scope.vm.age,
          gender_id: $scope.vm.gender.id,
          local_organisation_member: $scope.vm.organisation_member.id,
          local_organisation_id: $scope.vm.local_organisation.id
        }
      )

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = -> farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]