@app.controller 'AppCtrl', [
  'toastr'
  '$http'
  '$scope'
  '$location'
  'offLine'
  '$localStorage'
  'farmer'
  'user'
  '$route'
  '$rootScope'
  (toastr, $http, $scope, $location, offLine, $localStorage, farmer, user, $route, $rootScope) ->

    $scope.$on '_farmer_', () -> 
      $scope.farmer = $localStorage.farmer

    $scope.$on '_user_', () ->
      $scope.current_user = $localStorage.current_user
      $scope.user_actions = $localStorage.user_actions
      #$scope.user = user
    $scope.user = user


    $scope.farmer = $localStorage.farmer
    if $rootScope.is_authenticated == true
      $scope.current_user = $localStorage.current_user

    $scope.pages = farmer.pages()

    $rootScope.$on '$routeChangeSuccess', (e, current, pre) ->
      $scope.valid_form_link = _.find $scope.pages, { 'name': _.replace(current.$$route.originalPath, "/", "") } 

    $scope.isActive = (page) ->
      ( "/" + page.name == $location.path())

    $scope.boolToStr = (arg) ->
      if arg then 'Yes' else 'No'

    # ------------- OFFLINE SYNC---------------------------------

    $scope.show_sync = () ->
      $scope.data_off = false
      if $rootScope.net_status
        offLine.getCount().then (_data) ->
          if _data > 0
            $scope.data_off = true
      else
        $scope.data_off = false

    $scope.show_sync()

    $scope.$on '_offline_', () ->
      $scope.show_sync()

    $scope.sync = () ->
      toastr.info('Sync in process...')
      offLine.getAll().then (_data) ->
        _post_data = _.map _data, (n) ->
          farmer.remove_objects(n)
        $http.post("/offline/sync", _post_data).then (response) ->
          toastr.success('Data is successfully Synced to Server', 'Successfully Synced')
          offLine.clear()
          delete $localStorage.farmer
          #$location.path('/')
          location.reload()
]

