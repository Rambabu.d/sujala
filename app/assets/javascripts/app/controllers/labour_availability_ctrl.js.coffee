@app.controller 'LabourAvailabilityCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  'offLine'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage, offLine) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/labour_availabilities.json" + "?farmer_id=" + $scope.farmer.id

    #farmer.index($scope) #Data will pulled to variable data_list
    farmer.index($scope).then (response) ->
      $scope.vm = _.last $scope.data_list
      if $scope.vm
        $scope.btns(false, true, true)

    $scope.parameter = () ->
      JSON.stringify(labour_availability: {
        farmer_id: $scope.farmer.id,
        family_labour_men: $scope.vm.family_labour_men,
        family_labour_women: $scope.vm.family_labour_women,
        hired_labour_men: $scope.vm.hired_labour_men,
        hired_labour_women: $scope.vm.hired_labour_women,
        family_adequacy: $scope.vm.family_adequacy,
        hired_adequacy: $scope.vm.hired_adequacy,
        has_anyone_migrated: $scope.vm.has_anyone_migrated,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.btns(true, false, true)

    $scope.submit = ->
      farmer.save($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        $localStorage.labor_availability = _.last $scope.data_list
        $scope.btns(false, true, true)

    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()

    $scope.save_migration_option = () ->
      offLine.deleteById($localStorage.labor_availability.id)
      $scope.vm = _.merge($localStorage.labor_availability, $scope.vm)
      $scope.vm.farmer = $localStorage.farmer
      farmer.save($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
]