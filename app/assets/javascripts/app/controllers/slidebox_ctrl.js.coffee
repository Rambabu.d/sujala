###
# Welcome to the new js2coffee 2.0, now
# rewritten to use the esprima parser.
# try it out!
###

###*
* Slidebox Directive
*
* version 0.9
* http://github.com/keithjgrant/slidebox
*
* by Keith J Grant
* http://elucidblue.com
###
@app.controller('SlideboxCtrl', ['$scope', '$element', '$attrs',  ($scope, $element, $attrs) ->
]).directive 'slidebox', ->
  {
    template: '<div class="slidebox-container">' + '<div class="slidebox">' + '<div ng-transclude class="slidebox-content"></div>' + '</div>' + '<div class="slidebox-controls slidebox-left"></div>' + '<div class="slidebox-controls slidebox-right"></div>' + '</div>'
    replace: true
    transclude: true
    restrict: 'AE'
    scope: false
    controller: 'SlideboxCtrl'
    link: (scope, element, attrs) ->
      content = element.children()[0]
      leftEl = element.children()[1]
      rightEl = element.children()[2]
      velocity = 0
      defaultOpacity = Number(getComputedStyle(leftEl).opacity)
      maxVelocity = Number(attrs.speed) or 25
      interval = undefined
      didScroll = true
      # trigger an initial check on load

      startScroll = (isLeft) ->
        # set default velocity for touchdevices
        if isLeft
          velocity = maxVelocity / -2
        else
          velocity = maxVelocity / 2
        interval = setInterval((->
          content.scrollLeft += velocity
          didScroll = true
          return
        ), 50)
        return

      stopScroll = (controlEl) ->
        clearInterval interval
        controlEl.style.opacity = defaultOpacity
        velocity = 0
        return

      touchStart = (controlEl, isLeft) ->
        startScroll()
        if isLeft
          velocity = maxVelocity / -2
        else
          velocity = maxVelocity / 2
        controlEl.style.opacity = 1.0
        return

      ###*
      # Get scalar between 0 and 1 based on mouse position in element
      ###

      getVelocityScalar = (element, clientX) ->
        width = element.offsetWidth
        leftSide = element.getBoundingClientRect().left
        (clientX - leftSide) / width

      updateVelocity = (controlEl, xPos, isLeft) ->
        scale = getVelocityScalar(controlEl, xPos)
        if isLeft
          scale -= 1
        if isLeft
          controlEl.style.opacity = defaultOpacity + scale * -1
        else
          controlEl.style.opacity = defaultOpacity + scale
        velocity = scale * maxVelocity
        return

      ###*
      # Hide control when scrolled all the way to the end
      ###

      updateControlVisability = ->
        if !didScroll
          return
        if content.scrollLeft == 0
          leftEl.style.display = 'none'
        else
          leftEl.style.display = 'block'
        if content.scrollLeft == content.scrollWidth - (content.offsetWidth)
          rightEl.style.display = 'none'
        else
          rightEl.style.display = 'block'
        didScroll = false
        return

      if attrs.contentWidth
        content.children[0].style.width = attrs.contentWidth
      if attrs.contentClass
        content.children[0].className += ' ' + attrs.contentClass
      leftEl.addEventListener 'mouseover', (->
        startScroll()
        return
      ), false
      leftEl.addEventListener 'mousemove', ((event) ->
        updateVelocity leftEl, event.clientX, true
        return
      ), false
      leftEl.addEventListener 'mouseout', (->
        stopScroll leftEl
        return
      ), false
      leftEl.addEventListener 'touchstart', ((event) ->
        event.preventDefault()
        # cancel mouse event
        touchStart leftEl, true
        return
      ), false
      leftEl.addEventListener 'touchend', ((event) ->
        event.preventDefault()
        # cancel mouse event
        stopScroll leftEl
        return
      ), false
      rightEl.addEventListener 'mouseover', ->
        startScroll()
        return
      rightEl.addEventListener 'mousemove', (event) ->
        updateVelocity rightEl, event.clientX, false
        return
      rightEl.addEventListener 'mouseout', (->
        stopScroll rightEl
        return
      ), false
      rightEl.addEventListener 'touchstart', ((event) ->
        event.preventDefault()
        # cancel mouse event
        touchStart rightEl, false
        return
      ), false
      rightEl.addEventListener 'touchend', ((event) ->
        event.preventDefault()
        # cancel mouse event
        stopScroll rightEl
        return
      ), false
      content.addEventListener 'scroll', ->
        didScroll = true
        return
      setInterval updateControlVisability, 250
      return

  }
