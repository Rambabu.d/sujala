@app.controller 'MarketingCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/marketings.json" + "?farmer_id=" + $scope.farmer.id

    # $http.get("/crop_lists.json" + "?farmer_id=" + $scope.farmer.id).then (response) ->
    #   $scope.crops = response.data
    $http.get("/production_costs.json" + "?farmer_id=" + $scope.farmer.id, cache: true).then (response) ->
      $scope.production_costs = response.data

    farmer.index($scope) #Data will pulled to variable data_list
    farmer.get($scope, "crop_lists")
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(marketing: {
        farmer_id: $scope.farmer.id,
        # output_obtained: $scope.vm.output_obtained,
        # output_retained: $scope.vm.output_retained,
        crop_id: $scope.vm.production_cost.crop_list.id,
        output_sold: $scope.vm.output_sold,
        type_of_market: $scope.vm.type_of_market,
        mode_of_transport: $scope.vm.mode_of_transport,
        # price_obtained: $scope.vm.price_obtained,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()

    $scope.getOutputObtained = () ->
      $http.get("/pc_yield_prices.json" + "?farmer_id=" + $scope.farmer.id + "&crop_list_id=" +  $scope.vm.crop.id).then (response) ->
        $scope.output_obtained_value = _.last response.data
        $scope.vm.output_obtained = $scope.output_obtained_value.crop_main_product_yield
]