@app.controller 'SubsidiaryCostCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/subsidiary_costs.json" + "?farmer_id=" + $scope.farmer.id

    farmer.index($scope) #Data will pulled to variable data_list

    farmer.index($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        if $scope.vm
          $scope.btns(false, true, true)

    $scope.parameter = () ->
      JSON.stringify(subsidiary_cost: {
        farmer_id: $scope.farmer.id,
        dry_fodder: $scope.vm.dry_fodder,
        green_fodder: $scope.vm.green_fodder,
        concentrates: $scope.vm.concentrates,
        manure: $scope.vm.manure,
        milk_price: $scope.vm.milk_price,
        lacation_period: $scope.vm.lacation_period,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ()->
      farmer.save($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        $scope.btns(false, true, true)

    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]
