@app.controller 'InstitutionalCreditCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/institutional_credits.json" + "?farmer_id=" + $scope.farmer.id

    # $http.get("/credit_avail_purposes.json" + "?from=" + 'Institutional').then (response) ->
    #   $scope.credit_avail_purposes = response.data

    farmer.get($scope, 'credit_avail_purposes')

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(institutional_credit: {
        farmer_id: $scope.farmer.id,
        source: $scope.vm.source,
        institution_amount: $scope.vm.institution_amount,
        rate_of_interest: $scope.vm.rate_of_interest,
        credit_avail_purpose_id: $scope.vm.credit_avail_purpose.id,
        repayment_status: $scope.vm.repayment_status,
        reactions: $scope.vm.reactions,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
]