@app.controller 'SurveyNumberCtrl', [
  '$location'
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($location, $scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/survey_numbers.json" + "?farmer_id=" + $scope.farmer.id
    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true
    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(survey_number: {
        farmer_id: $scope.farmer.id,
        number: $scope.vm.number,
        hissa_number: $scope.vm.hissa_number,
        grid1: $scope.vm.grid1,
        grid2: $scope.vm.grid2,
        grid3: $scope.vm.grid3,
        grid4: $scope.vm.grid4

      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]