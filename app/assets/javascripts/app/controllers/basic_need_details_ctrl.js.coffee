@app.controller 'BasicNeedDetailsCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->

    $scope.farmer = $localStorage.farmer

    $scope.url = "/basic_need_details.json" + "?farmer_id=" + $scope.farmer.id

    farmer.index($scope).then (response) ->
      $scope.vm = _.last $scope.data_list
      if $scope.vm
        $scope.btns(false, true, true)

    $scope.parameter = () ->
      JSON.stringify(basic_need_detail: {
        farmer_id: $scope.farmer.id,
        light: $scope.vm.light,
        toilet_facility: $scope.vm.toilet_facility,
        health_card: $scope.vm.health_card,
        pds_card: $scope.vm.pds_card,
        work_nrgea: $scope.vm.work_nrgea,
        no_of_persons: $scope.vm.no_of_persons,
        no_of_days: $scope.vm.no_of_days,
        hours_of_electricity: $scope.vm.hours_of_electricity,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ()->
      farmer.save($scope).then (response) ->
        $scope.vm = _.last $scope.data_list
        $scope.btns(false, true, true)

    $scope.cancel = () -> farmer.cancel($scope)

    $scope.delete = (id, url) -> farmer.delete($scope, id, url)

    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]