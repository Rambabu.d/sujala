@app.controller 'UserRolesCtrl', [
  '$scope'
  '$http'
  '$routeParams'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $routeParams, $templateCache, farmer, $cacheFactory, $localStorage) ->
    #$scope.farmer = $localStorage.farmer

    $http.get("/users/user_roles.json" + "?user_id=" + $routeParams.id).then (response) ->
      $scope.user = _.head(response.data)
      $scope.user_roles = _.uniq($scope.user.roles)

    $http.get("/roles.json").then (response) ->
      $scope.roles = response.data

    #farmer.index($scope) #Data will pulled to variable data_list

    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    #$scope.farmer = $cacheFactory.get("$http").get("farmer")

    $scope.parameter = () ->
      JSON.stringify(user_role: {
        role_id: $scope.vm.role.id,
        user_id: $routeParams.id
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      $http.post("/users/create_user_role", $scope.parameter()).then (response) ->
        $scope.user_roles.push($scope.vm.role.name)
        $scope.vm = {}
        #self.refresh_cache(scope)

    $scope.delete = (role) ->
      role_delete = _.find($scope.roles, {name: role}).id
      $http.post("/users/delete_user_role", {user_id: $routeParams.id, role_id: role_delete}).then (response) ->
        _.pull($scope.user_roles, role)
]

