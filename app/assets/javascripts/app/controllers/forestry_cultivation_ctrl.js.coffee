@app.controller 'ForestryCultivationCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->
    $scope.farmer = $localStorage.farmer

    $scope.url = "/forestry_cultivations.json" + "?farmer_id=" + $scope.farmer.id

    farmer.get($scope, "forestry_plants")

    farmer.index($scope) #Data will pulled to variable data_list
    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.parameter = () ->
      JSON.stringify(forestry_cultivation: {
        farmer_id: $scope.farmer.id,
        forestry_plant_id: $scope.vm.forestry_plant.id,
        number: $scope.vm.number,
      })

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.submit = ->
      farmer.save($scope)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
    $scope.next = () -> farmer.next()
    $scope.prev = () -> farmer.prev()
]