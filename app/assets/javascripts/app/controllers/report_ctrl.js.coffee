@app.controller 'ReportCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, farmer, $cacheFactory, $localStorage) ->

    $http.get("/report/report_list.json", cache: true).then (response) ->
      $scope.report_list = response.data

    farmer.get($scope, "districts")

    farmer.get($scope, "crop_lists")
    farmer.get($scope, "intercrop_lists")

    $scope.get_sub_water_sheds = ()->
      farmer.get_with_params($scope, "sub_water_sheds", "?district_id=" + $scope.vm.district.id)
    $scope.get_micro_water_sheds = ()->
      farmer.get_with_params($scope, "micro_water_sheds", "?sub_water_shed_id=" + $scope.vm.sub_water_shed.id)
    #$scope.get_villages = ()->
    #  farmer.get_with_params($scope, "villages", "?micro_water_shed_id=" + $scope.vm.micro_water_shed.id)

    $scope.btn_category = true
    $scope.btn_smu = true
    $scope.btn_topography = true
    # COC cost of cultivation
    $scope.btn_coc = false

    $scope.n_precesion = 0

    $scope.total = () ->
      $scope.show_total = _.includes(['Population characteristics of family members',
        'Educational level of family members', 'Occupation of household heads',
        'Type of house owned', 'Distribution of land(Ha)',
        'Irrigated Area (ha)', 'Cropping pattern',
        'Avg. Annual gross income from all sources',
        'Institutional Participation of household members',
        'Purpose of migration by household members', 'Average Annual gross income', 'Average Annual expenditure'
        'Occupation of family members', 'Age wise classification of family members'],
        $scope.vm.rpt)

    $scope.pop_avg = () ->
      #  ['Population characteristics of family members', 'Average Annual expenditure','Average Annual gross income']
      $scope.population_average = _.includes(['Average Annual expenditure'],
        $scope.vm.rpt)

    $scope.prcnt = () ->
      $scope.hide_percent = _.includes([
        'Status of bore wells',
        'Status of open wells',
        'Avg. value of durable assets owned',
        'Avg. value of farm implements owned',
        'Average Labour availability',
        'Depth of Water (Avg. in meters)',
        'Irrigated Area (ha)',
        'Avg. land value (Rs./ha)',
        'Cropping pattern', 'Avg. Credit amount',
        'Horticulture species grown',
        'Forest species grown',
        'Average distance and duration of migration',
        'Cropping intensity',
        'Fodder availability',
        'Average Annual gross income',
        'Average Annual expenditure',
        'Average annual net income',
        'Average Additional investment capacity'],
        $scope.vm.rpt)

    $scope.first_header = (report) ->
      $scope.n_precesion_correction = {
        'Cropping pattern',
        'Irrigated Area (ha)',
        'Distribution of land(Ha)'
      }
      if (_($scope.vm.rpt).includes('Cropping intensity') || _($scope.vm.rpt).includes('Avg') || _($scope.vm.rpt).includes('Average') || _($scope.n_precesion_correction).includes($scope.vm.rpt))
        $scope.n_precesion = 2
      else
        $scope.n_precesion = 0
      title = {
        'Avg. value of durable assets owned': 'Average Value (Rs.)',
        'Avg. value of farm implements owned': 'Average Value (Rs.)',
        'Avg. Annual gross income from all sources': 'Rs.',
        'Cropping pattern': 'Area (ha)',
        'Irrigated Area (ha)': 'Area (ha)',
        'Fodder availability': 'Q (t)',
        'Distribution of land(Ha)': 'ha'
        'Average Additional investment capacity':  'Rs.',
        'Average Annual gross income':  'Rs.',
        'Average Annual expenditure':  'Rs.',
        'Average annual net income':  'Rs.',
        'Cropping intensity': 'Cropping Intensity (%)'


      }
      $scope.first_header_title = title[report]



    $scope.second_header_title = () ->

    $scope.second_colum = () ->
      $scope.show_second = _.includes(['Horticulture species grown', 'Forest species grown'], 
        $scope.vm.rpt)
      $scope.show_days_column = _.includes(['Fodder availability'], 
        $scope.vm.rpt)

    $scope.get_farmers_count = (type) ->
      $scope.report_name = $scope.vm.rpt
      #$http.get("farmers/count.json?micro_water_shed=" +  $scope.vm.micro_water_shed.id + "&type=" + type).then (response) ->
      $http.get("report/farmer_count.json?micro_water_shed=" +  $scope.vm.micro_water_shed.id + "&type=" + type+ "&report_type=" + $scope.vm.rpt).then (response) ->
        $scope.farmer_count = response.data

    $scope.smu_btn = () ->
      #$http.get("farmers/count.json?micro_water_shed=" + $scope.vm.micro_water_shed.id).then (response) ->
      #  $scope.farmer_count = response.data
      $scope.smu_url = "/report/smu.json" + "?micro_water_shed=" + $scope.vm.micro_water_shed.id + "&report_type=" + $scope.vm.rpt
      $http.get($scope.smu_url).then (smu_response) ->
        $scope.smu_data = smu_response.data
        $scope.get_farmers_count('smu')
        $scope.smu_options =
          data: $scope.smu_data
          dimensions:
            name:
              axis: 'x'
            LMU1: type: 'bar', label: true
            LMU2: type: 'bar', label: true
            LMU3: type: 'bar', label: true
            LMU4: type: 'bar', label: true
            LMU5: type: 'bar', label: true
            LMU6: type: 'bar', label: true
            LMU7: type: 'bar', label: true
            LMU8: type: 'bar', label: true
            LMU9: type: 'bar', label: true
            LMU10: type: 'bar', label: true
        $scope.category = false
        $scope.smu = true
        $scope.topography = false
        $scope.total()
        $scope.prcnt()
        $scope.pop_avg()
        $scope.second_colum()
        $scope.first_header($scope.vm.rpt)
        $scope.cultivation_cost_template = ''

    $scope.category_btn = () ->
      #$http.get("farmers/count.json?micro_water_shed=" + $scope.vm.micro_water_shed.id).then (response) ->
      #  $scope.farmer_count = response.data
      $scope.category_url = "/report/category.json" + "?micro_water_shed=" + $scope.vm.micro_water_shed.id + "&report_type=" + $scope.vm.rpt
      # $http.get($scope.category_url , cache: true).then (category_response) ->
      $http.get($scope.category_url).then (category_response) ->
        $scope.category_data = category_response.data
        $scope.category_options =
          data: $scope.category_data
          dimensions:
            name:
              axis: 'x'
            LL: type: 'bar', label: true
            MF: type: 'bar', label: true
            SF: type: 'bar', label: true
            SMF: type: 'bar', label: true
            MDF: type: 'bar', label: true
            LF: type: 'bar', label: true
        $scope.category = true
        $scope.smu = false
        $scope.topography = false
        $scope.total()
        $scope.prcnt()
        $scope.pop_avg()
        $scope.second_colum()
        $scope.get_farmers_count('category')
        $scope.first_header($scope.vm.rpt)
        $scope.cultivation_cost_template = ''

    $scope.sum = (data, column) ->
      _.sumBy(data, (item) -> item[column])

    $scope.all_cal_topography = (td) ->
      total = parseFloat(td.Landless || 0) + parseFloat(td.Valley || 0) + parseFloat(td.Mid || 0) + parseFloat(td.Ridge || 0)
      if (_($scope.vm.rpt).includes('Avg') || _($scope.vm.rpt).includes('Average') || _($scope.vm.rpt).includes('Cropping intensity'))
        tot = parseFloat(td.all_average)
      else
        tot = total
      return tot

    $scope.all_cal_smu = (cd) ->
      # total = (if cd.Landless == null then  0 else parseFloat(cd.Landless)) +
      #   (if cd.LMU1 == null then 0 else parseFloat(cd.LMU1))  +
      #   (if cd.LMU2 == null then 0 else parseFloat(cd.LMU2)) +
      #   (if cd.LMU3 == null then 0 else parseFloat(cd.LMU3))  +
      #   (if cd.LMU4 == null then 0 else parseFloat(cd.LMU4)) +
      #   (if cd.LMU5 == null then 0 else parseFloat(cd.LMU5))  +
      #   (if cd.LMU6 == null then 0 else parseFloat(cd.LMU6))

      total = parseFloat(cd.Landless || 0) + parseFloat(cd.LMU1 || 0) + parseFloat(cd.LMU2 || 0) + parseFloat(cd.LMU3 || 0) + parseFloat(cd.LMU4 || 0) + parseFloat(cd.LMU5 || 0) + parseFloat(cd.LMU6 || 0)

      if _($scope.vm.rpt).includes('Avg') || _($scope.vm.rpt).includes('Average') || _($scope.vm.rpt).includes('Cropping intensity')

        tot = parseFloat(cd.all_average)
      else
        tot = total
      return tot

    $scope.all_cal_category = (sd) ->
      total = parseFloat(sd.LL || 0) + parseFloat(sd.MF || 0) + parseFloat(sd.SF || 0) + parseFloat(sd.SMF || 0) + parseFloat(sd.MDF || 0) + parseFloat(sd.LF || 0)
      if _($scope.vm.rpt).includes('Avg') || _($scope.vm.rpt).includes('Average') || _($scope.vm.rpt).includes('Cropping intensity')
        tot = parseFloat(sd.all_average)
      else
        tot = total
      return tot
    $scope.all_percent_cal_topography = (td) ->
      if $scope.vm.rpt == 'Households sampled'
        (( (td.Landless || 0) + (td.Valley || 0) + (td.Mid || 0) + (td.Ridge || 0) ) / ( (td.Landless_total || td.Valley_total || td.Mid_total || td.Ridge_total) )) * 100
      else
        (( td.Landless + td.Valley + td.Mid + td.Ridge ) / ( td.Landless_total + td.Ridge_total + td.Mid_total + td.Valley_total )) * 100
    $scope.all_percent_cal_category = (sd) ->
      if $scope.vm.rpt == 'Households sampled'
        ( ((sd.LL || 0) + (sd.MF || 0) + (sd.SF || 0) + (sd.SMF || 0) + (sd.MDF || 0) + (sd.LF || 0)) / (sd.LL_total || sd.MF_total || sd.SF_total || sd.SMF_total || sd.MDF_total || sd.LF_total) ) * 100
      else
        ( (sd.LL + sd.MF + sd.SF + sd.SMF + sd.MDF + sd.LF) / (sd.LL_total + sd.MF_total + sd.SF_total + sd.SMF_total + sd.MDF_total + sd.LF_total) ) * 100
    $scope.all_percent_cal_smu = (cd) ->
      if $scope.vm.rpt == 'Households sampled'
        (((cd.Landless || 0) + (cd.LMU1 || 0)  + (cd.LMU2 || 0) + (cd.LMU3 || 0)  + (cd.LMU4 || 0) + (cd.LMU5 || 0)  + (cd.LMU6 || 0 ) ) / ( (cd.Landless_total || cd.LMU1_total || cd.LMU2_total || cd.LMU3_total || cd.LMU4_total || cd.LMU5_total || cd.LMU6_total) )) * 100 
      else
        ((cd.Landless + cd.LMU1  + cd.LMU2 + cd.LMU3  + cd.LMU4 + cd.LMU5  + cd.LMU6  ) / ( cd.Landless_total + cd.LMU1_total + cd.LMU2_total + cd.LMU3_total + cd.LMU4_total + cd.LMU5_total + cd.LMU6_total )) * 100 

    $scope.topography_btn = () ->
      #$http.get("farmers/count.json?micro_water_shed=" + $scope.vm.micro_water_shed.id).then (response) ->
      #  $scope.farmer_count = response.data
      $scope.topography_url = "/report/topography.json" + "?micro_water_shed=" + $scope.vm.micro_water_shed.id + "&report_type=" + $scope.vm.rpt
      $http.get($scope.topography_url, cache: true ).then (topography_response) ->
        $scope.topography_data = topography_response.data
        $scope.topography_options =
          data: $scope.topography_data
          dimensions:
            name:
              axis: 'x'
            Ridge: type: 'bar', label: true
            Mid: type: 'bar', label: true
            Valley: type: 'bar', label: true
        $scope.category = false
        $scope.smu = false
        $scope.topography = true
        $scope.total()
        $scope.prcnt()
        $scope.pop_avg()
        $scope.second_colum()
        $scope.first_header($scope.vm.rpt)
        $scope.get_farmers_count('topography')
        $scope.cultivation_cost_template = ''


    $scope.report_dropdown = () ->
      if "Cost of Cultivation" == $scope.vm.rpt
        $scope.btn_category = false
        $scope.btn_smu = false
        $scope.btn_topography = false
        $scope.btn_coc = true
        $scope.btn_live_stock = false

        $scope.category = false
        $scope.smu = false
        $scope.topography = false
      else if ("Economics of Livestock" == $scope.vm.rpt ||
      "Actual and potential yield of agri. and horti. crops" == $scope.vm.rpt ||
      $scope.vm.rpt == 'Suggestions for improvement of the micro-watershed' ||
      $scope.vm.rpt == 'Status of soil and water conservation structures' ||
      $scope.vm.rpt == 'Source of additional investment' ||
      'Marketing of the agricultural produce'== $scope.vm.rpt)
        $scope.btn_category = false
        $scope.btn_smu = false
        $scope.btn_topography = false
        $scope.btn_coc = false
        $scope.btn_live_stock = true

        $scope.category = false
        $scope.smu = false
        $scope.topography = false
      else
        $scope.btn_category = true
        $scope.btn_smu = true
        $scope.btn_topography = true
        $scope.btn_coc = false
        $scope.btn_live_stock = false


    $scope.cultivation_cost = () ->
      $scope.cultivation_cost_url = "/report/cultivation_cost" +
        "?micro_water_shed=" + $scope.vm.micro_water_shed.id +
        "&report_type=" + $scope.vm.rpt +
        "&crop_list=" + $scope.vm.crop_list.id +
        "&inter_crop_list=" + $scope.vm.inter_crop_list.id +
        "&farmer_category=" + $scope.vm.farmer_category
      $http.get($scope.cultivation_cost_url).then (cultivation_cost_response) ->
        $scope.cultivation_cost_template = cultivation_cost_response.data

    $scope.live_stock = () ->
      if $scope.vm.rpt == "Actual and potential yield of agri. and horti. crops"
        _link = "/report/main_yield"
      else if $scope.vm.rpt == "Marketing of the agricultural produce"
        _link = '/report/marketing'
      else if $scope.vm.rpt == 'Suggestions for improvement of the micro-watershed'
        _link = '/report/comments'
      else if $scope.vm.rpt == 'Status of soil and water conservation structures'
        _link = '/report/soil_status'
      else if $scope.vm.rpt == 'Source of additional investment'
        _link = '/report/additional_invst_src'
      else
        _link = "/report/live_stock"

      $scope.live_stock_url = _link +
        "?micro_water_shed=" + $scope.vm.micro_water_shed.id +
        "&report_type=" + $scope.vm.rpt
      $http.get($scope.live_stock_url).then (live_stock_response) ->
        $scope.cultivation_cost_template = live_stock_response.data
]