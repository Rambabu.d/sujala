@app.controller 'RoleCtrl', [
  '$scope'
  '$http'
  '$templateCache'
  '$location'
  'farmer'
  '$cacheFactory'
  '$localStorage'
  ($scope, $http, $templateCache, $location, farmer, $cacheFactory, $localStorage) ->
    $scope.url = "/roles.json"

    farmer.index($scope) #Data will pulled to variable data_list

    $scope.btn_add = true
    $scope.btn_update = false
    $scope.btn_cancel = true

    $scope.btns = (_add, _update, _cancel) ->
      $scope.btn_add = _add
      $scope.btn_update = _update
      $scope.btn_cancel = _cancel

    $scope.parameter = () ->
      JSON.stringify(role: {
        name: $scope.vm.name
    })

    $scope.submit = ->
      farmer.save($scope)

    $scope.get_role = (id) ->
      $location.path("/role_ractions/" + id)

    $scope.edit = (id)-> farmer.edit($scope, id)
    $scope.update = () -> farmer.update($scope)
    $scope.cancel = () -> farmer.cancel($scope)
    $scope.delete = (id, url) -> farmer.delete($scope, id, url)
]


