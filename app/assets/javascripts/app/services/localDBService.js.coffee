@app.service 'localDBService', [
  '$q'
  'localDB'
  '$localStorage'
  '$rootScope'
  '$location'
  ($q, localDB, $localStorage, $rootScope, $location) ->
    instance: null
    transactionTypes: {
      readonly: 'readonly'
      readwrite: 'readwrite'
    }

    _error = {
      setErrorHandlers: (request, errorHandler) ->
        if (typeof request != 'undefined')
          if ('onerror' in request)
            request.onerror = errorHandler
          if ('onblocked' in request)
            request.onblocked = errorHandler
          if ('onabort' in request)
            request.onabort = errorHandler
    }

    open: (dbModel) ->
      self = @
      deffered = $q.defer()
      request = localDB.open(dbModel.name, dbModel.version)
      _error.setErrorHandlers(request, deffered.reject)
      request.onupgradeneeded = dbModel.upgrade
      request.onsuccess = (e) ->
        self.instance = e.target.result
        _error.setErrorHandlers(self.instance, deffered.reject)
        deffered.resolve()
      deffered.promise

    requireOpenDB: (storeName, deffered) ->
      self = @
      if self.instance == null
        deffered.reject("Object store " + storeName + "is not open")

    getStore: (storeName, mode) ->
      self = @
      mode = mode || 'readonly'
      txn = self.instance.transaction(storeName, mode)
      store = txn.objectStore(storeName)

    requireStoreName: (storeName, mode) ->
      if ( typeof(storeName) == 'undefined' ||
      !storeName || storeName.length == 0 )
        deffered("StoreName is required")

    getCount: (storeName) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName)
      request = store.count()
      request.onsuccess = (e) ->
        deffered.resolve(e.target.result)
      deffered.promise

    insert: (storeName, data, keyName) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName, 'readwrite')
      _id = (new Date()).valueOf()
      data[keyName] = _id
      request = store.add(data)

      request.onsuccess = () ->
        deffered.resolve(data)
      deffered.promise

    update: (storeName, data, keyName) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName, 'readwrite')
      _id = (new Date()).valueOf()
      data[keyName] = _id
      request = store.put(data)

      request.onsuccess = () ->
        deffered.resolve(data)
      deffered.promise

    getAll: (storeName) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName)
      data = []

      cursor = store.openCursor()
      cursor.onsuccess = (e) ->
        result = e.target.result
        if (result && result != null)
          data.push(result.value)
          result.continue()
        else
          deffered.resolve(data)
      deffered.promise

    clear: (storeName) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName, 'readwrite')
      request = store.clear()
      request.onsuccess = () ->
        deffered.resolve
      deffered.promise

    getById: (storeName, key) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName)
      request = store.get(key)
      request.onsuccess = deffered.resolve
      deffered.promise

    deleteById: (storeName, key) ->
      self = @
      deffered = $q.defer()
      self.requireStoreName(storeName, deffered)
      self.requireOpenDB(storeName, deffered)

      store = self.getStore(storeName, 'readwrite')
      request = store.delete(key)
      request.onsuccess = deffered.resolve
      deffered.promise
]