@app.service 'farmer', [
  '$http'
  'toastr'
  '$cacheFactory'
  'offLine'
  '$localStorage'
  '$route'
  '$rootScope'
  '$location'
  'ngDexie'
  ($http, toastr, $cacheFactory, offLine, $localStorage, $route, $rootScope, $location, ngDexie) ->

    pages = [
      {name: '1', label: 'General Information'}
      {name: '2', label: 'Survey Numbers'}
      {name: '3', label: 'Sociological & Demographic Information'}
      {name: '4', label: 'Houses'}
      {name: '5', label: 'Durable Assets'}
      {name: '6', label: 'Farm Machinaries & Implements'}
      {name: '7', label: 'Livestock/Animal Possession'}
      {name: '8', label: 'Labour availability and migration details'}
      {name: '9', label: 'Land Holding with survey Numbers'}
      {name: '10', label: 'Source of Irrigation & Water Quality'}
      {name: '11', label: 'Cropping pattern and intensity'}
      {name: '12', label: 'Borrowing Status'}
      {name: '13', label: 'Cost of Production'}
      {name: '13-1', label: '(Cost of Production) Details of Operation'}
      {name: '13-2', label: '(Cost of Production) Other Expenses'}
      {name: '13-3', label: '(Cost of Production) Yield and Price'}
      {name: '13-4', label: '(Cost of Production) Details of PPC Application'}
      {name: '13-5', label: '(Cost of Production) Details of Fertilizer Application'}
      {name: '13-6', label: '(Cost of Production) Wage Rate'}
      {name: '14', label: 'Cost of production on subsidiary enterprises'}
      {name: '15', label: 'Fodder availability'}
      {name: '16', label: 'Family Annual income'}
      {name: '17', label: 'Horticulture & Forest Species'}
      {name: '18', label: 'Additional Investment Capacity'}
      {name: '19', label: 'Utilisation of Produce'}
      {name: '20', label: 'Watershed Awareness'}
      {name: '21', label: 'Soil and Water Conservation Practices'}
      {name: '22', label: 'Access to Basic Needs'}
      {name: '23', label: 'Farming Constraints'}
      {name: '24', label: 'Adequacy &amp; Self Sufficiency'}
      {name: '25', label: 'Watershed Improvement Suggestions'}
      {name: '26', label: 'Watershed Comments'}
    ];

    pages: () ->
      pages 

    current: () ->
      self = @
      page_name  = _.replace($location.path(), "/", "")
      _.findIndex(pages, ['name', page_name])

    next: () ->
      self = @
      n = _.nth(pages, self.current() + 1)
      $location.path('/' + n.name)

    prev: () ->
      self = @
      p = _.nth(pages, self.current() - 1)
      $location.path('/' + p.name)


    # get: (scope, name) ->
    #   $http.get("/" + name + ".json", cache: true).then (response) ->
    #     _.set(scope, name, response.data)

    get: (scope, name) ->
      ngDexie.list(name).then (data) ->
        _.set(scope, name, data)

    get_with_params: (scope, name, params) ->
      $http.get("/" + name + ".json" + params, cache: true).then (response) ->
        _.set(scope, name, response.data)

    index: (scope) ->
      self = @
      if $rootScope.net_status
        return self.index_remote(scope)
      else
        return self.index_local(scope)

    index_remote: (scope) ->
      $http.get(scope.url, params: {user: $localStorage.current_user.id}).then (response) ->
        # console.log response.data
        scope.data_list = response.data

    idMapping: () ->
      id_mapping = {
        'purpose_id': 'purposes'
        'conversation_structure_id': 'conversation_structures'
        'crop_id' : 'crop_lists'
        'intercrop_id': 'intercrops'
        'forestry_plant_id': 'forestry_plants'
        'asset_type_id': 'asset_types'
        'income_source_id': 'income_sources'
        'constraint_id': 'constraints'
        'fertilizer_id': 'fertilizers'
        'food_material_id': 'food_materials'
        'house_type_id': 'house_types'
        'suggestion_paramater_id': 'suggestion_paramaters'
        'migration_purpose_id': 'migration_purposes'
        'livestock_type_id': 'livestock_types'
        'machine_type_id': 'machine_types'
        'migration_positive_consequence_id': 'migration_positive_consequences'
        'migration_negative_consequence_id': 'migration_negative_consequences'
        'horticulture_plant_id': 'horticulture_plants'
        'crop_list_id': 'crop_lists'
        'intercrop_list_id': 'intercrop_lists'
        'education_id': 'educations'
        'occupation_id': 'occupations'
        'gender_id': 'genders'
        'local_organisation_id': 'local_organisations'
        'livestock_name_id': 'livestock_names'
        'particular_id': 'particulars'
        'credit_avail_purpose_id': 'credit_avail_purposes'
        'organisation_member': 'local_organisations'
        'district_id': 'districts'
        'village_id': 'villages'
        'smu_id': 'smus'
        'topography_id': 'topographies'
        'caste_id': 'castes'
        'grampanchayat_id': 'grampanchayats'
        'hobli_id': 'hoblis'
        'taluk_id': 'taluks'
        'micro_water_shed_id': 'micro_water_sheds'
        'sub_water_shed_id': 'sub_water_sheds'
        'survey_number_id': 'survey_numbers'
        'production_cost_id': 'production_costs'
        'socio_demographic_info_id': 'socio_demographic_infos'
      }

    getElementWithId: (scope, key, value) ->
      self = @
      _list = _.get(scope, self.idMapping()[key])
      _element = _.filter(_list, {id: value})


    index_local: (scope) ->
      self = @
      offLine.getIndex(scope.url).then (response) ->
        scope.data_list = _.map response, (n) ->
          _params = JSON.parse(n.params)
          _key = Object.keys(_params)[0]
          _params[_key]['id'] = n.id
          _params[_key]

    save: (scope) ->
      self = @
      # console.log($rootScope.net_status)
      if $rootScope.net_status
        return self.save_remote(scope)
      else
        return self.save_local(scope)

    save_remote: (scope) ->
      self = @
      $http.post(scope.url, scope.parameter()).then (response) ->
        # Commenting for pagination
        if scope.url == "/farmers.json"
          scope.farmer_data = response.data
        else
          scope.data_list.push(response.data)
        toastr.success('Sucessfully saved')
        scope.vm = {}
        self.refresh_cache(scope)

    add_objects: (scope) ->
      self = @
      _params = JSON.parse(scope.parameter())
      _key = Object.keys(_params)[0]
      _value = _params[_key]
      _.forEach _.keys(_value), (_attrib) ->
        if (_.includes(_.keys(self.idMapping()), _attrib))
          _value[_.replace(_attrib, '_id', '')] =
            self.getElementWithId(scope, _attrib, _value[_attrib])[0]
        if(_attrib == 'local_organisation_member')
          _value['organisation_member'] = self.getElementWithId(scope, 'organisation_member', _value['local_organisation_member'])[0]
        if(_attrib == 'production_cost_id')
          _value['crop_list'] =  _params[_key]["production_cost"]["crop_list"]
          _value['intercrop_list'] =  _params[_key]["production_cost"]["intercrop_list"]
        if(_attrib == 'crop_id')
          _value['crop'] =  self.getElementWithId(scope, 'crop_id', _value['crop_id'])[0]

      if (_key == 'farmer')
        _value['micro_water_shed'] = self.getElementWithId(scope, 'micro_water_shed_id', _value.village.micro_water_shed_id)[0]
        _value['sub_water_shed'] = self.getElementWithId(scope, 'sub_water_shed_id', _value.micro_water_shed.sub_water_shed_id)[0]
        _value['hobli'] = self.getElementWithId(scope, 'hobli_id', _value.grampanchayat.hobli_id)[0]
        _value['taluk'] = self.getElementWithId(scope, 'taluk_id', _value.hobli.taluk_id)[0]
        _value['district'] = self.getElementWithId(scope, 'district_id', _value.taluk.district_id)[0]
      _params[_key] = _value
      _params

    remove_objects: (data) ->
      self = @
      #q = _.map data_list, (data) ->
      _params = JSON.parse(data.params)
      _key = Object.keys(_params)[0]
      delete _params[_key]['id']

      _value = _params[_key]
      _.forEach _.keys(_value), (_attrib) ->
        if (_.includes(_.keys(self.idMapping()), _attrib))
          delete _value[_.replace(_attrib, '_id', '')]
        if(_attrib == 'local_organisation_member')
          delete _value['organisation_member']
      _farmer = data.farmer
      _.forEach _.keys(_farmer), (_attrib) ->
        if (_.includes(_.keys(self.idMapping()), _attrib))
          delete _farmer[_.replace(_attrib, '_id', '')]
      _v = ['micro_water_shed','sub_water_shed','hobli','taluk','district']
      if _key == 'farmer'
        _.forEach _v, (_l) ->
          delete _value[_l]
      else
        _.forEach _v, (_l) ->
          delete _farmer[_l]

      _params[_key] = _value
      data.params = JSON.stringify(_params)
      data.farmer = _farmer
      data

    save_local: (scope) ->
      self = @
      _params = self.add_objects(scope)
      data = {url: scope.url, params: JSON.stringify(_params)}

      _top_key = Object.keys(_params)[0]
      if (_top_key != 'farmer')
        data['farmer'] = scope.farmer

      offLine.save(data).then (_data) ->
        d_json = JSON.parse(data.params)
        data_json = d_json[Object.keys(d_json)[0]]
        data_json['id'] = _data.id
        if ( typeof(scope.data_list) == 'undefined' )
          scope.data_list = []
        toastr.success('Sync to the server when you are online', 'Sucessfully saved - Locally')

        scope.data_list.push(data_json)
        scope.vm = {}
        self.refresh_cache(scope)

    edit: (scope, id) ->
      scope.vm = _.find scope.data_list, (data) ->
        data.id == id
      scope.btns(false, true, true)

    update: (scope) ->
      self = @
      # console.log($rootScope.net_status)
      if $rootScope.net_status
        return self.update_remote(scope)
      else
        return self.update_local(scope)

    update_remote: (scope) ->
      self = @
      $http.put(scope.vm.url, scope.parameter()).then (response) ->
        scope.updated = response.data
        scope.vm = {}
        self.refresh_cache(scope)
        toastr.success('Sucessfully updated')
        scope.btns(true, false, true)

    update_local: (scope) ->
      self = @
      offLine.deleteById(scope.vm.id)
      _.remove scope.data_list, (list) ->
        list.id == scope.vm.id
      self.save_local(scope)
      self.index_local(scope)
      toastr.success('Sucessfully Updated - Locally')
      scope.btns(true, false, true)


    cancel: (scope) ->
      scope.vm = {}
      scope.btns(true, false, true)

    delete: (scope, id, url) ->
      self = @
      if $rootScope.net_status
        return self.delete_remote(scope, id, url)
      else
        return self.delete_local(scope, id, url)

    delete_remote: (scope, id, url) ->
      self = @
      $http.delete(url).then (response) ->
        _.remove scope.data_list, (list) ->
          list.id == id
        self.refresh_cache(scope)
        toastr.success('Sucessfully deleted')

    delete_local: (scope, id, url) ->
      self = @
      offLine.deleteById(id)
      _.remove scope.data_list, (list) ->
        list.id == id
      self.refresh_cache(scope)
      toastr.success('Be sure to delete the dependency records as well', 'Sucessfully Deleted - Locally')
      scope.btns(true, false, true)

    refresh_cache: (scope) ->
      cache = $cacheFactory.get("$http")
      cache.remove(scope.url)
      cache.put(scope.url, JSON.stringify(scope.data_list))
]