@app.service 'user', [
  '$http'
  '$cacheFactory'
  '$localStorage'
  '$route'
  '$rootScope'
  '$location'
  ($http, $cacheFactory, $localStorage, $route, $rootScope, $location) ->

    is_allowed: (action) ->
      _.includes($localStorage.user_actions, action)
]
