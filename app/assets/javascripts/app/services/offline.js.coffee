@app.service 'offLine', [
  'localDBService'
  'dbModel'
  '$q'
  '$rootScope'
  (localDBService, dbModel, $q, $rootScope) ->
    save: (data) ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.insert(dbModel.storeName, data, 'id').then (_data) ->
          deffered.resolve(_data)
          $rootScope.$broadcast("_offline_")
      , deffered.reject
      )
      deffered.promise

    update: (data) ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.update(dbModel.storeName, data, 'id').then (_data) ->
          deffered.resolve(_data)
          $rootScope.$broadcast("_offline_")
      , deffered.reject
      )
      deffered.promise

    getCount: () ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.getCount(dbModel.storeName).then (_data) ->
          deffered.resolve(_data)
      , deffered.reject
      )
      deffered.promise

    getAll: () ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.getAll(dbModel.storeName).then (_data) ->
          deffered.resolve(_data)
      , deffered.reject
      )
      deffered.promise

    getIndex: (scope_url) ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.getAll(dbModel.storeName).then (_data) ->
          resopsne_data = _.filter(_data, {url: scope_url})
          deffered.resolve(resopsne_data)
      , deffered.reject
      )
      deffered.promise

    clear: () ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.clear(dbModel.storeName).then () ->
          deffered.resolve
          $rootScope.$broadcast("_offline_")
      , deffered.reject
      )
      deffered.promise

    getById: (key) ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.getById(dbModel.storeName, key).then (response) ->
          deffered.resolve(response.target.result['params'])
      , deffered.reject
      )
      deffered.promise

    deleteById: (key) ->
      deffered = $q.defer()
      localDBService.open(dbModel).then( (e) ->
        localDBService.deleteById(dbModel.storeName, key).then (response) ->
          deffered.resolve(response)
          # deffered.resolve(response.target.result['params'])
      , deffered.reject
      )
      deffered.promise
]