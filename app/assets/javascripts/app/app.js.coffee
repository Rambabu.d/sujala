@app = angular.module('sujala', ['ngdexie',
  'ngdexie.ui',
  'ng-token-auth',
  'ngRoute',
  'restangular',
  'ngStorage',
  'angularChart',
  'ngSanitize',
  'toastr',
  'bgf.paginateAnything',
  'ng-token-auth'])

@app.config [
  '$authProvider'
  ($authProvider) ->
    $authProvider.configure
      apiUrl: "http://ameyasujala.com"
      #apiUrl: 'http://localhost:3000'
      emailRegistrationPath: '/auth'
      confirmationSuccessUrl: '/'
      tokenValidationPath:     '/auth/validate_token'
      signOutUrl:              '/auth/sign_out'
      accountUpdatePath:       '/auth'
      accountDeletePath:       '/auth'
      passwordResetPath:       '/auth/password'
      passwordUpdatePath:      '/auth/password'
      passwordResetSuccessUrl: '/'
      emailSignInPath:         '/auth/sign_in'
      storage:                 'localStorage'
      forceValidateToken:      true
      validateOnPageLoad:      true

      #  "Authorization": "token={{ token }} expiry={{ expiry }} uid={{ uid }}"
]

# Dexie Local Storage wrapper
@app.config [
  'ngDexieProvider'
  '$provide'
  (ngDexieProvider, $provide) ->
    column_types = ["purposes", "conversation_structures",
      "crops", "intercrops", "forestry_plants",
      "asset_types", "income_sources", "smus",
      "topographies", "castes",
      "constraints", "fertilizers", "food_materials",
      "house_types", "suggestion_paramaters",
      "migration_purposes", "livestock_types",
      "machine_types", "migration_positive_consequences",
      "migration_negative_consequences",
      "horticulture_plants", "crop_lists", "intercrop_lists",
      "districts", "educations", "occupations",
      "genders", "local_organisations", "livestock_names",
      "particulars", "villages", "grampanchayats", "hoblis",
      "taluks", "micro_water_sheds", "sub_water_sheds", 'credit_avail_purposes', 'credit_sources']
    $provide.constant('cTypes', column_types)

    ngDexieProvider.setOptions
      name: 'typesDB'
      debug: false
    ngDexieProvider.setConfiguration (db) ->
      db.version(1).stores
        #tables: '++id, name'
        villages: 'id'
        grampanchayats: 'id'
        hoblis: 'id'
        taluks: 'id'
        micro_water_sheds: 'id'
        sub_water_sheds: 'id'
        smus: 'id'
        topographies: 'id'
        castes: 'id'
        districts: 'id'
        purposes: 'id'
        conversation_structures: 'id'
        crops: 'id'
        intercrops: 'id'
        forestry_plants: 'id'
        asset_types: 'id'
        income_sources: 'id'
        constraints: 'id'
        fertilizers: 'id'
        food_materials: 'id'
        house_types: 'id'
        suggestion_paramaters: 'id'
        migration_purposes: 'id'
        livestock_types: 'id'
        machine_types: 'id'
        migration_positive_consequences: 'id'
        migration_negative_consequences: 'id'
        horticulture_plants: 'id'
        crop_lists: 'id'
        intercrop_lists: 'id'
        educations: 'id'
        occupations: 'id'
        genders: 'id'
        local_organisations: 'id'
        livestock_names: 'id'
        particulars: 'id'
        credit_avail_purposes: 'id'
        credit_sources: 'id'
]

@app.run [
  '$rootScope'
  'toastr'
  '$location'
  '$localStorage'
  'cTypes'
  '$http'
  '$templateCache'
  '$route'
  'ngDexie'
  ($rootScope, toastr, $location, $localStorage, cTypes, $http, $templateCache, $route, ngDexie)->
    # -------------- OFFLINE -------------------
    typesDb = ngDexie

    $rootScope.net_status = false
    Offline.on 'confirmed-down', () ->
      $rootScope.net_status = false
      $rootScope.$broadcast("_offline_")
      toastr.error('Offline')
      #console.log "CONNECTION DOWN >>>>"

    Offline.on 'confirmed-up', () ->
      $rootScope.net_status = true
      $rootScope.$broadcast("_offline_")
      toastr.success('Online')
      #console.log "CONNECTION UP <<<<<"
    # --------------OFFLINE TEMPLATES-----------

    url = undefined
    for index of $route.routes
      if url = $route.routes[index].templateUrl
        $http.get url, cache: $templateCache

    if navigator.onLine
      _tables = cTypes
      for index of _tables
        _table_name = _tables[index]
        $http.get("/" + _table_name + ".json", cache: true).then (response) ->
          _r_table_name = response.config.url.replace('.json', '').replace('/','')
          typesDb.getDb (db) ->
            db.table(_r_table_name).clear().then () ->
              #console.log "Cleared"
            angular.forEach response.data, (item) ->
              typesDb.add(_r_table_name, item).then () ->
                # console.log 'Added'

    # --------------OFFLINE END ----------------
    $rootScope.location = $location
    # console.log(window.localStorage.auth_headers)
    Offline.check()
    #console.log navigator.onLine
    if(!navigator.onLine && $localStorage.current_user != undefined)
      $location.path '/'
      $rootScope.is_authenticated = true
    else if (window.localStorage.auth_headers != undefined)
      #auhd = JSON.parse(window.localStorage.auth_headers)
      #$http.defaults.headers.common['access-token'] = auhd["access-token"]
      #$http.defaults.headers.common['token-type'] = auhd["token-type"]
      #$http.defaults.headers.common['client'] = auhd["client"]
      #$http.defaults.headers.common['expiry'] = auhd["expiry"]
      #$http.defaults.headers.common['uid'] = auhd["uid"]
      $rootScope.$broadcast("_user_")
      $rootScope.is_authenticated = true
    else
      $location.path '/signin'
      $rootScope.is_authenticated = false

    $rootScope.$on 'auth:login-success', (e, user)->
      $localStorage.current_user = user
      toastr.success('Authentication sucessful', 'Welcome...!!!')

      $http.get("/users/actions.json?user=" + user.id, cache: true).then (response) ->
        $localStorage.user_actions = response.data
      #auhd = JSON.parse(window.localStorage.auth_headers)
      #$http.defaults.headers.common['access-token'] = auhd["access-token"]
      #$http.defaults.headers.common['token-type'] = auhd["token-type"]
      #$http.defaults.headers.common['client'] = auhd["client"]
      #$http.defaults.headers.common['expiry'] = auhd["expiry"]
      #$http.defaults.headers.common['uid'] = auhd["uid"]
      $rootScope.is_authenticated = true
      $location.path '/'
      $rootScope.$broadcast("_user_")
    $rootScope.$on 'auth:logout-success', (e, user)->
      toastr.success('Sucessfully Logged out')
      $rootScope.is_authenticated = false
      $location.path '/signin'
    $rootScope.$on 'auth:login-error', (e, reason)->  
      toastr.error(reason.errors[0],'Authentication Failed')
]

# for compatibility with Rails CSRF protection
@app.config [
  '$httpProvider'
  ($httpProvider)->
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
]

@app.config [
  '$locationProvider'
  '$routeProvider'
  '$provide'
  ($locationProvider, $routeProvider, $provide) ->
    $provide.constant('localDB', window.indexedDB)
    $provide.value('dbModel', {
      name: 'locals'
      version: '1'
      instance: null
      storeName: 'sujala'
      objectStoreName: 'sujala'
      keyName: 'id'
      upgrade: (e) ->
        db = e.target.result
        if (!db.objectStoreNames.contains('sujala'))
          db.createObjectStore('sujala', { keyPath: 'id' })
    })
    #$locationProvider.hashPrefix '!'
    $routeProvider
      .when('/',
        templateUrl: '/farmers' )
      .when('/register',
        templateUrl: '/users/register',
        controller: 'UserRegistrationsCtrl')
      .when('/signin',
        templateUrl: '/users/signin',
        controller: 'UserSessionsCtrl')
      .when('/1',
        templateUrl: '/farmers/new' )
      .when('/2',
        templateUrl: '/survey_numbers',
        controller: 'SurveyNumberCtrl' )
      .when('/3',
        templateUrl: '/socio_demographic_infos',
        controller: 'SocioDemoInfoCtrl')
      .when('/4',
        templateUrl: '/houses',
        controller: 'HousesCtrl')
      .when('/5',
        templateUrl: '/durable_assets',
        controller: 'DurableAssetsCtrl')
      .when('/6',
        templateUrl: '/machines',
        controller: 'MachinesCtrl')
      .when('/7',
        templateUrl: '/livestocks',
        controller: 'LivestockCtrl')
      .when('/8',
        templateUrl: '/labour_availabilities',
        controller: 'LabourAvailabilityCtrl')
      .when('/9',
        templateUrl: '/surveys',
        controller: 'SurveyCtrl')
      .when('/10',
        templateUrl: '/irrigations',
        controller: 'IrrigationCtrl')
      .when('/11',
        templateUrl: '/cropping_intensities',
        controller: 'CroppingIntensityCtrl')
      .when('/12',
        templateUrl: '/borrowing_statuses',
        controller: 'BorrowingStatusCtrl')
      .when('/13',
        templateUrl: '/production_costs',
        controller: 'ProductionCostCtrl')
      .when('/13-1',
        templateUrl: '/pc_operation_details',
        controller: 'OperationDetailsCtrl')
      .when('/13-2',
        templateUrl: '/pc_other_expenses',
        controller: 'OtherExpensesCtrl')
      .when('/13-3',
        templateUrl: '/pc_yield_prices',
        controller: 'YieldPriceCtrl')
      .when('/13-4',
        templateUrl: '/ppc_details',
        controller: 'PpcApplicationCtrl')
      .when('/13-5',
        templateUrl: '/fertilizer_details',
        controller: 'FertilizerAppCtrl')
      .when('/13-6',
        templateUrl: '/pc_wage_rates',
        controller: 'WageRatesCtrl')
      .when('/14',
        templateUrl: '/subsidiary_enterprises',
        controller: 'SubsidiaryEnterpriseCtrl')
      .when('/15',
        templateUrl: '/fodder_availabilities',
        controller: 'FodderAvailabilityCtytrl')
      .when('/16',
        templateUrl: '/family_annual_incomes',
        controller: 'FamilyAnnualIncomeCtrl')
      .when('/17',
        templateUrl: '/plant_counts',
        controller: 'PlantCountCtrl')
      .when('/18',
        templateUrl: '/additional_investment_capacities',
        controller: 'AdditionalInvestmentCapacityCtrl')
      .when('/19',
        templateUrl: '/marketings',
        controller: 'MarketingCtrl')
      .when('/20',
        templateUrl: '/watershed_awarenesses',
        controller: 'WatershedAwarenessCtrl')
      .when('/21',
        templateUrl: '/conversation_practices',
        controller: 'ConversationPracticeCtrl')
      .when('/22',
        templateUrl: '/basic_needs',
        controller: 'BasicNeedCtrl')
      .when('/23',
        templateUrl: '/farming_constraints',
        controller: 'FarmingConstraintCtrl')
      .when('/24',
        templateUrl: '/household_food_materials',
        controller: 'HouseholdFoodMaterialCtrl')
      .when('/25',
        templateUrl: '/improvement_suggestions',
        controller: 'ImprovementSuggestionCtrl')
      .when('/26',
        templateUrl: '/watershed_comments',
        controller: 'WatershedCommentCtrl')
      .when('/reports',
        templateUrl: '/report/index',
        controller: 'ReportCtrl')
      .when('/users',
        templateUrl: '/users/template',
        controller: 'UserCtrl')
      .when('/user_roles/:id',
        templateUrl: '/users/user_roles_template',
        controller: 'UserRolesCtrl')
      .when('/roles',
        templateUrl: '/roles',
        controller: 'RoleCtrl')
      .when('/role_ractions/:id',
        templateUrl: '/roles/role_ractions_template',
        controller: 'RoleRactionsCtrl')
      .when('/asset_types',
        templateUrl: '/asset_types',
        controller: 'AssetTypeCtrl')
      .when('/purposes',
        templateUrl: '/purposes',
        controller: 'PurposeCtrl')
      .when('/conversation_structures',
        templateUrl: '/conversation_structures',
        controller: 'ConversationStructureCtrl')
      .when('/crops',
        templateUrl: '/crops',
        controller: 'CropCtrl')
      .when('/intercrops',
        templateUrl: '/intercrops',
        controller: 'IntercropCtrl')
      .when('/forestry_plants',
        templateUrl: '/forestry_plants',
        controller: 'ForestryPlantCtrl')
      .when('/income_sources',
        templateUrl: '/income_sources',
        controller: 'IncomeSourceCtrl')
      .when('/smus',
        templateUrl: '/smus',
        controller: 'SmuCtrl')
      .when('/topographies',
        templateUrl: '/topographies',
        controller: 'TopographyCtrl')
      .when('/castes',
        templateUrl: '/castes',
        controller: 'CasteCtrl')
      .when('/constraints',
        templateUrl: '/constraints',
        controller: 'ConstraintCtrl')
      .when('/fertilizers',
        templateUrl: '/fertilizers',
        controller: 'FertilizerCtrl')
      .when('/food_materials',
        templateUrl: '/food_materials',
        controller: 'FoodMaterialCtrl')
      .when('/house_types',
        templateUrl: '/house_types',
        controller: 'HouseTypeCtrl')
      .when('/suggestion_paramaters',
        templateUrl: '/suggestion_paramaters',
        controller: 'SuggestionParamaterCtrl')
      .when('/migration_purposes',
        templateUrl: '/migration_purposes',
        controller: 'MigrationPurposeCtrl')
      .when('/livestock_types',
        templateUrl: '/livestock_types',
        controller: 'LivestockTypeCtrl')
      .when('/machine_types',
        templateUrl: '/machine_types',
        controller: 'MachineTypeCtrl')
      .when('/migration_positive_consequences',
        templateUrl: '/migration_positive_consequences',
        controller: 'MigrationPositiveConsequenceCtrl')
      .when('/migration_negative_consequences',
        templateUrl: '/migration_negative_consequences',
        controller: 'MigrationNegativeConsequenceCtrl')
      .when('/horticulture_plants',
        templateUrl: '/horticulture_plants',
        controller: 'HorticulturePlantCtrl')
      .when('/crop_lists',
        templateUrl: '/crop_lists',
        controller: 'CropListCtrl')
      .when('/intercrop_lists',
        templateUrl: '/intercrop_lists',
        controller: 'IntercropListCtrl')
      .when('/districts',
        templateUrl: '/districts',
        controller: 'DistrictCtrl')
      .when('/educations',
        templateUrl: '/educations',
        controller: 'EducationCtrl')
      .when('/occupations',
        templateUrl: '/occupations',
        controller: 'OccupationCtrl')
      .when('/genders',
        templateUrl: '/genders',
        controller: 'GenderCtrl')
      .when('/local_organisations',
        templateUrl: '/local_organisations',
        controller: 'LocalOrganisationCtrl')
      .when('/livestock_names',
        templateUrl: '/livestock_names',
        controller: 'LivestockNameCtrl')
      .when('/particulars',
        templateUrl: '/particulars',
        controller: 'ParticularCtrl')
      .when('/villages',
        templateUrl: '/villages',
        controller: 'VillageCtrl')
      .when('/grampanchayats',
        templateUrl: '/grampanchayats',
        controller: 'GramapanchayatCtrl')
      .when('/hoblis',
        templateUrl: '/hoblis',
        controller: 'HobliCtrl')
      .when('/taluks',
        templateUrl: '/taluks',
        controller: 'TalukCtrl')
      .when('/micro_water_sheds',
        templateUrl: '/micro_water_sheds',
        controller: 'MicroWaterShedCtrl')
      .when('/sub_water_sheds',
        templateUrl: '/sub_water_sheds',
        controller: 'SubWaterShedCtrl')
      .when('/credit_avail_purposes',
        templateUrl: '/credit_avail_purposes',
        controller: 'CreditAvailPurposeCtrl')
      .when('/credit_sources',
        templateUrl: '/credit_sources',
        controller: 'CreditSourceCtrl')
      .when('/error',
        templateUrl: '/404.html' )

      #.otherwise('/error' )

    $locationProvider.html5Mode(true)

]
