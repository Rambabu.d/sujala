class SocioDemographicInfo < ApplicationRecord
	belongs_to :education
	belongs_to :occupation
	belongs_to :local_organisation
	belongs_to :gender
	belongs_to :organisation_member, foreign_key: :local_organisation_member, class_name: "LocalOrganisation"

	belongs_to :farmer

	include Reportable

	def self.data()
	  return super if @level.blank?

	  self.frmr(@type, @micro_water_shed)
	    .where("farmers.name = socio_demographic_infos.farmer_member_name")
	    .joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").count

  end

end
