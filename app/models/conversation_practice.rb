class ConversationPractice < ApplicationRecord
  belongs_to :conversation_structure
  belongs_to :farmer

  include Reportable

  def self.data()
    return super if @report['column'] == 'conversation_structure'
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
  end

  def self.init_data()
    return super if @report['column'] == 'conversation_structure'
    hard_values.product(names(@type))
  end

  def self.hard_values 
  	["Own", "NGO", "Govt.", "Farmer organization", "Other"]
  end

  def self.destination_data()
    return super if @report['column'] == 'conversation_structure'
    hard_values.collect{|col_name| {"name" => col_name}}
  end 
end
