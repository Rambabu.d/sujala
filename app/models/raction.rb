class Raction < ApplicationRecord
	has_many :role_ractions
	has_many :roles, through: :role_ractions
end
