class Migration < ApplicationRecord
	belongs_to :farmer

  include Reportable
  
  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:migration_family_count)
  end
 
  def self.init_data()
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    self.pluck(@column).uniq.collect{|col_name| {"name" => col_name}}
  end

  def self.farmer_count(type, mws, report)
    self.joins(:farmer => [:village, type.to_sym])
    .where("villages.micro_water_shed_id" => mws)
    .group("#{plural(type)}.name")
    .sum('migrations.family_count')
  end

end