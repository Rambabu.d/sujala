class LabourMigrationDetail < ApplicationRecord
	belongs_to :migration_purpose
	belongs_to :socio_demographic_info
	belongs_to :farmer

	include Reportable

	def self.farmer_count(type, mws, report)
    self.joins(:farmer => [:village, type.to_sym])
    .where("villages.micro_water_shed_id" => mws)
    .group("#{plural(type)}.name")
    .count
  end
end
