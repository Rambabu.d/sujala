class Farmer < ApplicationRecord
	belongs_to :village
	belongs_to :grampanchayat
	belongs_to :caste
	belongs_to :smu
	belongs_to :topography
	has_one :category
	
	has_many :socio_demographic_infos, dependent: :destroy
	has_many :additional_investment_capacities, dependent: :destroy
	has_many :basic_needs, dependent: :destroy
	has_many :basic_need_details, dependent: :destroy
	has_many :borrowing_statuses, dependent: :destroy
  has_many :conversation_practices, dependent: :destroy
  has_many :conversation_structures, dependent: :destroy
  has_many :credit_avail_purposes, dependent: :destroy
  has_many :credit_sources, dependent: :destroy
  has_many :cropping_intensities, dependent: :destroy
  has_many :cropping_paterns, dependent: :destroy
  has_many :cultivate_horticultures, dependent: :destroy
  has_many :durable_assets, dependent: :destroy
  has_many :family_annual_incomes, dependent: :destroy
  has_many :farming_constraints, dependent: :destroy
  has_many :fertilizer_details, dependent: :destroy
  has_many :fodder_availabilities, dependent: :destroy
  has_many :food_materials, dependent: :destroy
  has_many :forestry_cultivations, dependent: :destroy
  has_many :forestry_plants, dependent: :destroy
  has_many :forestry_species, dependent: :destroy
  has_many :horticulture_plants, dependent: :destroy
  has_many :household_food_materials, dependent: :destroy
  has_many :houses, dependent: :destroy
  has_many :improvement_suggestions, dependent: :destroy
  has_many :income_sources, dependent: :destroy
  has_many :institutional_credits, dependent: :destroy
  has_many :irrigations, dependent: :destroy
  has_many :labour_availabilities, dependent: :destroy
  has_many :labour_migration_details, dependent: :destroy
  has_many :livestocks, dependent: :destroy
  has_many :machines, dependent: :destroy
  has_many :marketings, dependent: :destroy
  has_many :migration_consequences, dependent: :destroy
  has_many :particulars, dependent: :destroy
  has_many :pc_operation_details, dependent: :destroy
  has_many :pc_other_expenses, dependent: :destroy
  has_many :pc_wage_rates, dependent: :destroy
  has_many :pc_yield_prices, dependent: :destroy
  has_many :plant_counts, dependent: :destroy
  has_many :ppc_details, dependent: :destroy
  has_many :private_credits, dependent: :destroy
  has_many :production_costs, dependent: :destroy
  has_many :socio_demographic_infos, dependent: :destroy
  has_many :subsidiary_costs, dependent: :destroy
  has_many :subsidiary_enterprises, dependent: :destroy
  has_many :suggestion_paramaters, dependent: :destroy
  has_many :survey_numbers, dependent: :destroy
  has_many :surveys, dependent: :destroy
  has_many :watershed_awarenesses, dependent: :destroy
  has_many :watershed_comments, dependent: :destroy

	

  include Reportable

  def self.topography(report_key, micro_water_shed)
    set_up(report_key, micro_water_shed, "topography")
		@farmer = farmer_count("topography", micro_water_shed, @report)
	  [farmer_total_values(@farmer)]
	end

	def self.smu(report_key, micro_water_shed)
    set_up(report_key, micro_water_shed, "smu")
	  @farmer = farmer_count("smu", micro_water_shed, @report)
		[farmer_total_values(@farmer)]
	end

	def self.category(report_key, micro_water_shed)
    set_up(report_key, micro_water_shed, "category")
		@farmer = farmer_count( "category", micro_water_shed, @report)
		[farmer_total_values(@farmer)]
	end

	def self.farmer_total_values(farmer)
		farmer_total = farmer.values.sum
		farmer.keys.each do |attrib|
      farmer[attrib + "_total"] = farmer_total
    end
		farmer['name'] = "Farmers"
		farmer
	end
end
