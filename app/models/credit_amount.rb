class CreditAmount < ApplicationRecord
	belongs_to :farmer

	include Reportable

	def self.data()

    self.frmr(@type, @micro_water_shed).group("#{plural(@type)}.name").sum(:amount)
  end

  def self.init_data()
    ["Average Credit"].product(names(@type))
  end

  def self.destination_data()
    ["Average Credit"].collect{|col_name| {"name" => col_name}}
  end

  def self.farmer_count(type, mws, report)
    BorrowingStatus.frmr(type, mws).where('borrowing_statuses.credit_availed = 1').group("#{plural(type)}.name").count
  end

    def self.report_data()
      _dest_data = destination_data()
      _data = data()
      @farmer_count = farmer_count(@type, @micro_water_shed, @report)
      data_merge().each do |key, value| 
        
        item = _dest_data.find{| _item | _item["name"] == "Average Credit"}
        item[key] = (value / @farmer_count[key]) unless @farmer_count[key].blank?
        if @label.include?('Avg')
          item['all_average'] = data_average() / @farmer_count.values.sum
        end
      end
      _dest_data
    end

    def self.data_average()
      self.frmr(@type, @micro_water_shed).sum(:amount)
    end

end