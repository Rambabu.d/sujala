class FarmingConstraint < ApplicationRecord
  belongs_to :constraint

  belongs_to :farmer

  include Reportable

  def self.data()
  	self.frmr(@type, @micro_water_shed).where("response = 'Experienced'").joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").count
  end
end
