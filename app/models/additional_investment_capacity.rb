class AdditionalInvestmentCapacity < ApplicationRecord
  belongs_to :purpose
  belongs_to :farmer

  include Reportable

   def self.data()
    if @column == 'purpose'
      self.frmr(@type, @micro_water_shed).joins(@column.to_sym)
      .group("#{plural(@column)}.name", "#{plural(@type)}.name")
      .sum(:extent_of_investment)

  
      else
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
    end
  end

  def self.init_data()
    return super if @column == 'purpose'
    hard_values.product(names(@type))
  end

  def self.hard_values 
    return super if @column == 'purpose'
  	["Own funds", "Soft loan", "Loan from bank", "Asset selling", "Pawn broking", "Government subsidy"]
  end

  def self.destination_data()
    return super if @column == 'purpose'
    hard_values.collect{|aa| {"name" => aa}}
  end

  def self.report_data()
  # return super unless @column == 'purpose'
      _dest_data = destination_data()

      _data = data()
      _data_farmer = farmer_count(@type, @micro_water_shed, @report)

  	  data_merge().each do |key, value| 
  	  	item = _dest_data.find{| _item | _item["name"] == key[0]}
        _farmer_total = farmer_total(_data_farmer, key[1])
        item[key[1]] = value.blank? ? 0 : (value / _farmer_total) if _farmer_total > 0
        if @label.include?('Average')
          __val = data_average()[key[0]]
          __farmer_count = _data_farmer.values.sum
          item['all_average'] = (__val.blank? ? 0 : __val)  / (__farmer_count.blank? ? 1 : __farmer_count)
        end
  	  	#item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key[1])
  	  end
  
  	  _dest_data
  end

    def self.data_average()
      self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").sum(:extent_of_investment)
   
    end

    
end
