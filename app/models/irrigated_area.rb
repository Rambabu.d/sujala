class IrrigatedArea < ApplicationRecord
	belongs_to :farmer

	include Reportable

	def self.data()

    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:cnt)
  end

  def self.init_data()
  	
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    JSON.parse(IrrigatedArea.group(@column).select(@column).to_json)
  end

    def self.report_data()

      _dest_data = destination_data()

      _data = data()
      _data_farmer = farmer_count(@type, @micro_water_shed, @report)
  #byebug
      data_merge().each do |key, value| 
        item = _dest_data.find{| item | item["name"] == key[0]}
        item[key[1]] = (value/2.47)
        item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key[1])
      end
      _dest_data
    end

end