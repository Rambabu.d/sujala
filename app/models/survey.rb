class Survey < ApplicationRecord
	belongs_to :survey_number
	belongs_to :farmer

	include Reportable

	def self.data()
    #self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").average(:present_land_value) 
    @distribution_land = self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:holding_acres)
    @average_land = self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:present_land_value)

    # @average_land.collect{|k,v| @average_land[k] = (( v / @distribution_land[k] ) * 0.4047)}
    # @distribution_land.collect{|k,v| @distribution_land[k] = (v * 0.4047)}
    @average_land.collect{|k,v| @average_land[k] = (( v / @distribution_land[k] ) * 2.47)}
    @distribution_land.collect{|k,v| @distribution_land[k] = (v * 0.4047)}
    @label == "Avg. land value (Rs./ha)" ? @average_land : @distribution_land
  end

  def self.init_data()
    self.pluck(:type_of_land).uniq.product(names(@type))
  end

  def self.destination_data()
    JSON.parse(Survey.group(:type_of_land).select(:type_of_land).to_json)
  end

  def self.report_data()
    _dest_data = destination_data()

    data_merge().each do |key, value| 
      item = _dest_data.find{| _item | _item["type_of_land"] == key[0]}
      item["name"] = item["type_of_land"]
      item[key[1]] = value.to_f
      item[key[1] + "_total"] = total(data(), key[1]).to_f
      if @label.include?('Avg')
        item['all_average'] = data_average()[key[0]]
      end
    end
    _dest_data.reject{|_row| _row['type_of_land'].blank?}
  end

  def self.data_average()

    @distribution_land = self.frmr(@type, @micro_water_shed).group(@column).sum(:holding_acres)
    @average_land = self.frmr(@type, @micro_water_shed).group(@column).sum(:present_land_value)
    @average_land.collect{|k,v| @average_land[k] = (( v / @distribution_land[k] ) * 2.47)}

    @average_land
  end

end
