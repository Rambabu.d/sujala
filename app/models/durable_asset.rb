class DurableAsset < ApplicationRecord
  belongs_to :asset_type
  belongs_to :farmer

  include Reportable

  def self.data()
    return super if @level == "farmer"

    @assets_owned = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").sum(:quantity)
    @assets_average = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").sum(:present_value)
    @assets_average.collect{|k,v| @assets_average[k] = (v / @assets_owned[k]).to_i}
    #@label == "Durable Assets owned" ?  : @assets_average

    @assets_average
  end

  def self.data_average()

    @assets_owned = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").sum(:quantity)
    @assets_average = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").sum(:present_value)
    @assets_average.collect{|k,v| @assets_average[k] = (v / @assets_owned[k]).to_i}

    @assets_average
  end
end