class WatershedComment < ApplicationRecord

  belongs_to :farmer

  include Reportable

   def self.data(micro_water_shed)
     joins(:farmer => {:village => :micro_water_shed}).where("villages.micro_water_shed_id" => micro_water_shed)
      
    end
end
