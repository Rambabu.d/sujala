class Labour < ApplicationRecord
	belongs_to :farmer

  include Reportable
  
  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").average(:cnt)

    #  @machines_owned = self..sum(:quantity)
    #   @machines_average = self..sum(:present_value)

    #   @machines_average.collect{|k,v| @machines_average[k] = (v / @machines_owned[k]).to_i}
    #   #@label == "Farm Implements owned" ? @machines_owned : @machines_average
    #   @machines_average
  end
 
  def self.init_data()
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    self.pluck(@column).uniq.collect{|col_name| {"name" => col_name}}
  end

  def self.data_average()

    self.frmr(@type, @micro_water_shed).group(@column).average(:cnt)
  end

end
  