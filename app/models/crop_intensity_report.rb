class CropIntensityReport < ApplicationRecord
	belongs_to :farmer

  include Reportable
  
  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:total_cultivated_area)
  end
 
  def self.init_data()
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    self.pluck(@column).uniq.collect{|col_name| {"name" => col_name}}
  end

   def self.report_data()

      _dest_data = destination_data()

      _data = data()
      #_data_farmer = farmer_count(@type, @micro_water_shed)
    @total_land = self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:total_land) 
      data_merge().each do |key, value| 
        item = _dest_data.find{| _item| _item["name"] == key[0]}

        item[key[1]] = @total_land[key].blank? ? 0 : ((value) / @total_land[key]) * 100
        #item[key[1] + "_total"] = @total_land[key].blank? ? 0 : ((value/2.47) / @total_land[key])
        item['all_average'] = (data_average()[key[0]] / @total_land.collect{|_key, _value| _value}.sum ) * 100
      end
      _dest_data
    end

    def self.data_average()
     self.frmr(@type, @micro_water_shed).group(@column).sum(:total_cultivated_area)
   end

end