class MigrationConsequence < ApplicationRecord
	belongs_to :migration_positive_consequence
	belongs_to :migration_negative_consequence
	belongs_to :farmer


	include Reportable
	def self.farmer_count(type, mws, report)
     @farmer_count = {}
     __data = Migration.frmr(type, mws).group('name', "#{plural(type)}.name").sum(:migration_family_count)
     __data.collect{|k,v| @farmer_count[k[1]] =  (@farmer_count[k[1]].blank? ? 0 : @farmer_count[k[1]]).to_i + v}
    # # self.joins(:farmer => [:village, type.to_sym]).where("villages.micro_water_shed_id" => mws).group("#{plural(type)}.name").count
    @farmer_count
  end
end
