class MigrationDistance < ApplicationRecord
	belongs_to :farmer

  include Reportable
  
  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").average(:cnt)
  end
 
  def self.init_data()
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    self.pluck(@column).uniq.collect{|col_name| {"name" => col_name}}
  end

  def report_data()

      _dest_data = destination_data()

      _data = data()
      _data_farmer = farmer_count(@type, @micro_water_shed)
      #byebug
  	  data_merge().each do |key, value| 
  	  	item = _dest_data.find{| _item | _item["name"] == key[0]}
  	  	item[key[1]] = value/farmer_total(_data_farmer, key[1])
  	  	# item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key[1])
  	  end
  	  _dest_data
    end

  def self.farmer_count(type, mws, report)
    @cnt = self.joins(:farmer => [:village, type.to_sym])
    .where("villages.micro_water_shed_id" => mws)
    .group("#{plural(type)}.name")
    .sum('migration_distances.migration_family_count')
    @cnt.collect{|k,v| @cnt[k] = v/2 }
    @cnt
  end

  def self.data_average()

   self.frmr(@type, @micro_water_shed).group(@column).average(:cnt)
  end

end