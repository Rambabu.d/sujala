class CultivateHorticulture < ApplicationRecord
  belongs_to :horticulture_plant
  belongs_to :farmer

  include Reportable

  def self.data()
    #self.frmr(@type, @micro_water_shed).group("#{plural(@type)}.name").distinct.count
    self.distinct.frmr(@type, @micro_water_shed).select("DISTINCT farmers.id, #{plural(@type)}.name").group("#{plural(@type)}.name").count
  end

  def self.init_data()
    names(@type)
  end

  def self.destination_data()
    ['Interested towards cultivation of horticulture crops'].collect{|col_name| {"name" => col_name}}
  end

  def self.report_data()
    _dest_data = destination_data()
     _data_farmer = farmer_count(@type, @micro_water_shed, @report)
    data_merge().each do |key, value|
      item = _dest_data.find{| _item | _item[@column] == 'Interested towards cultivation of horticulture crops'}
      item[key] = value
      item[key + "_total"] = farmer_total(_data_farmer, key)
    end
    _dest_data
  end


end
