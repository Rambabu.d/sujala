class Role < ApplicationRecord
	has_many :user_roles
	has_many :users, through: :user_roles

	has_many :role_ractions
	has_many :ractions, through: :role_ractions
end
