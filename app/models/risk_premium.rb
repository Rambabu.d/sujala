class RiskPremium < ApplicationRecord
	belongs_to :farmer
  belongs_to :production_cost


  def self.data(micro_water_shed_id, farmer_category, main_crop, inter_crop)
    if farmer_category == 'All'
      @data = self.joins(:farmer => [:village, :category])
        .where("villages.micro_water_shed_id = #{micro_water_shed_id}")
        .joins(:production_cost)
        .where("production_costs.crop_list_id = #{main_crop}")
        .where("production_costs.intercrop_list_id = #{inter_crop}")
    else 
      @data = self.joins(:farmer => [:village, :category])
      .where("villages.micro_water_shed_id = #{micro_water_shed_id}")
      .where("categories.name = '#{farmer_category}'") 
      .joins(:production_cost)
      .where("production_costs.crop_list_id = #{main_crop}")
      .where("production_costs.intercrop_list_id = #{inter_crop}")    
    end
    @data.select("sum(unit) as unit, sum(unit_cost) as unit_cost,sum(area) as area, risk_premia.name as name")
      .group(:name)
  end

end