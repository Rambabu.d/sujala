class Marketing < ApplicationRecord
  belongs_to :crop, foreign_key: "crop_id", class_name: "CropList"
  belongs_to :farmer

  include Reportable

   
  def self.data()
    if @column == 'type_of_market'
      _data_ = self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count(:type_of_market)
    else
      _data_ = self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
    end
     _data_
  end

  #  def self.marketing(micro_water_shed)
  #    @marketing = Marketing.joins(:farmer => {:village => :micro_water_shed}).where("villages.micro_water_shed_id" => micro_water_shed)
  #   @op_obtained = @marketing.joins(:crop).group("crop_lists.name").sum(:output_obtained)
  #   @op_retained = @marketing.joins(:crop).group("crop_lists.name").sum(:output_retained)
  #   @op_sold = @marketing.joins(:crop).group("crop_lists.name").sum(:output_sold)
  #   @price = @marketing.joins(:crop).group("crop_lists.name").sum(:price_obtained)
  #   @fcount = @marketing.joins(:crop).group("crop_lists.name").count

  #   end

  def self.init_data()
    hard_values.product(names(@type))
  end

  def self.hard_values 
  	if @column == 'type_of_market'
	   ary = ["Agent/Traders", 
	  		"Local/village Merchant",
	  		"Regulated Market",
	  		"Cooperative marketing Society",
	  		"Contract marketing arrangement",
	  		"Outside the State",
	  		"Outside the country"]
	elsif @column == 'mode_of_transport'
		ary = ["Head Load", "Cart", "Tractor", "Bus", "Truck", "Train", "Flight"]
	else 
	end
	  ary 			  		

  end

  def self.destination_data()
    hard_values.collect{|col_name| {"name" => col_name}}
  end 

end
