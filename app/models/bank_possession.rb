class BankPossession < ApplicationRecord
	belongs_to :farmer

  include Reportable

  def self.data()

    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:cnt)
  end

  def self.init_data()
  	
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    JSON.parse(BankPossession.group(@column).select(@column).to_json)
  end

end
