class HouseholdFoodMaterial < ApplicationRecord
  belongs_to :food_material

  belongs_to :farmer

  include Reportable

  def self.data()
    self.frmr(@type, @micro_water_shed).where("status = '#{@report['filter']}'").joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").count
  end
end
