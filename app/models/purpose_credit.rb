class PurposeCredit < ApplicationRecord
	belongs_to :farmer
  belongs_to :credit_avail_purpose

	include Reportable

	def self.farmer_count(type, mws, report)
    BorrowingStatus.frmr(type, mws).where('borrowing_statuses.credit_availed = 1').group("#{plural(type)}.name").count
  end

end