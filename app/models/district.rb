class District < ApplicationRecord
	has_many :taluks
	has_many :sub_water_sheds
end
