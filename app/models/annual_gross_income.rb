class AnnualGrossIncome < ApplicationRecord
	belongs_to :farmer

  include Reportable
  
  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:cost)
  end
 
  def self.init_data()
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    self.pluck(@column).uniq.collect{|col_name| {"name" => col_name}}
  end

  def self.data_average()
    # self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:cost)
    self.frmr(@type, @micro_water_shed).group(@column).average(:cost)
  end

  

end