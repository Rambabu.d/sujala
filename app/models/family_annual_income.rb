class FamilyAnnualIncome < ApplicationRecord
  belongs_to :income_source
  belongs_to :farmer

  include Reportable

  def self.data()
    if(@label == "Average Annual gross income")
      _totals = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").sum(:total_income)
      _data_farmer = farmer_count(@type, @micro_water_shed, @report)
      _totals.each{|k, v| _totals[k] = v/_data_farmer[k[1]]}
      _totals
    elsif(@label == "Average Annual expenditure")
      self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").average(:total_expenditure)
    end
  end

  def self.data_average()
    if(@label == "Average Annual gross income")
      @assets_average = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").average(:total_income)
    elsif(@label == "Average Annual expenditure")
      @assets_average = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").average(:total_expenditure)
    end
  end

  def self.report_data()
    _dest_data = destination_data()
    _data = data()
    _data_farmer = farmer_count(@type, @micro_water_shed, @report)
    data_merge().reject{|a|a[0].blank?}.each do |key, value|
      item = _dest_data.find{| _item | _item["name"].strip == key[0].strip}
      item[key[1]] = value.blank? ? 0 : value.to_f
      item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key[1])
      if @label.include?('Avg') || @label.include?('Average') 
        item['all_average'] = (total_data_average()[key[0]] || 0) / @farmer_count.values.sum
        #item['all_average'] = data_average()[key[0]]
      end
    end
    _dest_data
  end

  def self.total_data_average
    if(@label == "Average Annual gross income")
      @assets_average = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").sum(:total_income)
    elsif(@label == "Average Annual expenditure")
      @assets_average = self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name").sum(:total_expenditure)
    end
  end
end
