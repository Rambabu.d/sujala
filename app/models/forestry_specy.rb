class ForestrySpecy < ApplicationRecord
	belongs_to :forestry_plant
	belongs_to :farmer

  include Reportable

  def self.data()
    self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").sum(:field)
  end

  def self.data_backyard()
  	self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").sum(:backyard)
  end

  def self.report_data()

      _dest_data = destination_data()

      _data = data()
      _data_farmer = farmer_count(@type, @micro_water_shed, @report)

_backyard_data = init().merge(data_backyard())

  	  data_merge().each do |key, value| 
  	  	item = _dest_data.find{| _item | _item["name"] == key[0]}
  	  	item[key[1]] = value
  	  	item[key[1] + "_total"] = _backyard_data[key]
  	  end
  	  _dest_data
  	end
end
