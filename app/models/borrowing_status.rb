class BorrowingStatus < ApplicationRecord
	  belongs_to :farmer

  include Reportable

  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
  end

  def self.init_data()
    [true].product(names(@type))
  end

  def self.destination_data()
    JSON.parse(BorrowingStatus.group(@column).select(@column).to_json)
  end

  def self.report_data()
    _dest_data = destination_data()
     _data_farmer = farmer_count(@type, @micro_water_shed, @report)
    data_merge().reject{|k,v| k[0] == false}.each do |key, value| 
      item = _dest_data.find{| _item | _item[@column] == true}
      item["name"] = "Credit Availed"

      item[key[1]] = value
      item[key[1] + "_total"] = farmer_total(_data_farmer, key[1])
    end
    _dest_data.reject{|k,v | k["credit_availed"] != true}
  end
end
