class CultivationCost < ApplicationRecord
	belongs_to :farmer
  belongs_to :production_cost


  def self.section_one(micro_water_shed_id, farmer_category, main_crop, inter_crop)
    if farmer_category == 'All'
      @data = CultivationCost.joins(:farmer => [:village, :category])
        .where("villages.micro_water_shed_id = #{micro_water_shed_id}")
        .joins(:production_cost)
        .where("production_costs.crop_list_id = #{main_crop}")
        .where("production_costs.intercrop_list_id = #{inter_crop}")
    else 
      @data = CultivationCost.joins(:farmer => [:village, :category])
      .where("villages.micro_water_shed_id = #{micro_water_shed_id}")
      .where("categories.name = '#{farmer_category}'") 
      .joins(:production_cost)
      .where("production_costs.crop_list_id = #{main_crop}")
      .where("production_costs.intercrop_list_id = #{inter_crop}")    
    end

    @data.select("sum(unit) as unit, sum(unit_cost) as unit_cost, cultivation_costs.area, cultivation_costs.name as name").group(:name, :area)
  end

end
