class Livestock < ApplicationRecord
	belongs_to :livestock_type
	belongs_to :farmer

	include Reportable
end
