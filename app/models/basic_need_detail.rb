class BasicNeedDetail < ApplicationRecord
  belongs_to :farmer

  include Reportable

	

  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
  end

  def self.init_data()
    if  ['work_nrgea', 'toilet_facility'].include? @report['column']
      ['Yes'].product(names(@type))
    else
      hard_values.product(names(@type))
    end
  end

  def self.hard_values
    if @report['column'] == 'light' 
  	  vals = ["Kerosene Lamp", "Solar Lamp", "Electricity", "Gas light"] 
  	elsif @report['column'] == 'pds_card'
  	  vals = ["APL","BPL", "Not Possessed"]
    
  	else
      vals =[]
  	end
  	vals
  end

  def self.destination_data()
    if  ['work_nrgea', 'toilet_facility'].include? @report['column'] 
      JSON.parse(self.group(@column).select(@column).to_json)
    else
      hard_values.collect{|col_name| {"name" => col_name}}
    end
  end

  def self.report_data()
    return super unless ['work_nrgea', 'toilet_facility'].include? @report['column'] 
    _dest_data = destination_data()
     _data_farmer = farmer_count(@type, @micro_water_shed, @report)

    if @report['column'] == 'toilet_facility'
      @name = 'Sanitary toilet facility'
      # @col = 'toilet_facility'
    else
      @name = 'Participation in NREGA programme'
      # @col = 'work_nrgea'
    end
    data_merge().each do |key, value| 
      item = _dest_data.find{| _item | _item[@column] == 'Yes'}
      item["name"] = @name
      
      item[key[1]] = value
      item[key[1] + "_total"] = farmer_total(_data_farmer, key[1])
    end
    _dest_data.reject{|k,v | k[@column] != 'Yes'}
  end 
end
