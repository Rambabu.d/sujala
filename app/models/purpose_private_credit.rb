class PurposePrivateCredit < ApplicationRecord
	belongs_to :farmer
  belongs_to :credit_avail_purpose

	include Reportable

  def self.destination_data()
    JSON.parse(to_model(@column).where(:credit_source_id => 2).to_json)
  end

end