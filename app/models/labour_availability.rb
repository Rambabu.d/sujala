class LabourAvailability < ApplicationRecord
  	belongs_to :farmer

	include Reportable

	# Household Migration: 
 #  ctrl: "LabourAvailability"
 #  column: "has_anyone_migrated"
  
  def self.data()
	self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count(:hired_adequacy)
  end

  def self.init_data()
    self.pluck("hired_adequacy").uniq.product(names(@type))
  end

  def self.destination_data()
    JSON.parse(LabourAvailability.group("hired_adequacy").select("hired_adequacy").to_json)
  end

  def self.report_data()
    _dest_data = destination_data()
    _data_farmer = farmer_count(@type, @micro_water_shed, @report)
    data_merge().reject{|a|a[0].blank?}.each do |key, value|
      item = _dest_data.find{| _item | _item["hired_adequacy"] == key[0]}
      item["name"] = item["hired_adequacy"]
      item[key[1]] = value.blank? ? 0 : value.to_f
      item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key[1])
    end
    _dest_data.reject{|rec| rec['hired_adequacy'].blank?}
  end

end
