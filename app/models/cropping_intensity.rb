class CroppingIntensity < ApplicationRecord
	belongs_to :survey_number
	belongs_to :crop
    belongs_to :intercrop
end
