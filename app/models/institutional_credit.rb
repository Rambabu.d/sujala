class InstitutionalCredit < ApplicationRecord
	belongs_to :credit_avail_purpose

	belongs_to :farmer

	include Reportable

  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
  end
 
  def self.init_data()
    self.pluck(@column).compact.uniq.product(names(@type))
  end

  def self.destination_data()
    self.pluck(@column).uniq.collect{|col_name| {"name" => col_name}}.reject{|a| a['name'].blank?}
  end

  # def self.farmer_count(type, mws, report)
  #   BorrowingStatus.frmr(type, mws).where('borrowing_statuses.credit_availed = 1').group("#{plural(type)}.name").count
  # end

  def self.farmer_count(type, mws, report)
    if report["total"] == 'individual'
      @farmer_count = {}
      #__data = self.frmr(type, mws).joins(report['column'].to_sym).group("#{plural(report['column'])}.name", "#{plural(type)}.name").count
      __data = self.frmr(type, mws).group("#{report['column']}", "#{plural(type)}.name").count
      __data.collect{|k,v| @farmer_count[k[1]] =  (@farmer_count[k[1]].blank? ? 0 : @farmer_count[k[1]]).to_i + v}
    else
      @farmer_count = Farmer.joins(:village).where("villages.micro_water_shed_id" => mws).joins(type.to_sym).group("#{plural(type)}.name").count
    end
    @farmer_count
  end

end
