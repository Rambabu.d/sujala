class BasicNeed < ApplicationRecord

	belongs_to :farmer

	include Reportable

	

	def self.data()
    self.frmr(@type, @micro_water_shed).where("minimum_need = '#{@report['filter']}'").group(@column, "#{plural(@type)}.name").count
  end

  def self.init_data()
    hard_values.product(names(@type))
  end

  def self.hard_values
    if @report['filter'] == 'Fuel for domestic use' 
  	  vals = ["Dung Cake","Fire Wood","Kerosene","Biogas","LPG"] 
  	elsif @report['filter'] == 'Drinking water'	
  	  vals = ["Piped supply","Bore Well", "Open well", "Lake/ Tank", "Canal/Nala", "RO water"]
  	else
      vals =[]
  	end

  	vals	  		

  end

  def self.destination_data()
    hard_values.collect{|col_name| {"name" => col_name}}
  end 
end
