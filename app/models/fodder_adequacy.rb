class FodderAdequacy < ApplicationRecord
	belongs_to :farmer

	include Reportable

  def self.data()
      self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
  end

  def self.init_data()
    ['Adequate-Dry Fodder', 'Inadequate-Dry Fodder', 'Adequate-Green Fodder', 'Inadequate-Green Fodder'].product(names(@type))
  end

  def self.destination_data()
    ['Adequate-Dry Fodder', 'Inadequate-Dry Fodder', 'Adequate-Green Fodder', 'Inadequate-Green Fodder'].collect{|col_name| {"name" => col_name}}
  end

end
