module Reportable
  extend ActiveSupport::Concern

  included do
    # attr_accessor :column, :type, :micro_water_shed, :label, :level
    scope :frmr, -> (type, micro_water_shed) { joins(:farmer => {:village => :micro_water_shed}).where("villages.micro_water_shed_id" => micro_water_shed).joins(:farmer => type) }
   # scope :farmers_action, -> (column, type) {frmr(type).joins(column.to_sym)}
   # scope :cnt, -> (column, type) { farmers_action(column, type).group("#{plural(column)}.name", "#{plural(type)}.name").count}
  end
 
  class_methods do

    def set_up(report_key, micro_water_shed, type)
      @type             = type
      @micro_water_shed = micro_water_shed
      @label            = report_key
      @report           = REPORT[report_key]
      @column           = REPORT[report_key]["column"]
      @level            = REPORT[report_key]["level"]
      @total            = REPORT[report_key]["total"]
      @filter           = REPORT[report_key]["filter"]
    end

    def topography(report_key, micro_water_shed)
      set_up(report_key, micro_water_shed, "topography")
      report_data()
    end

    def smu(report_key, micro_water_shed)
      set_up(report_key, micro_water_shed, "smu")
      report_data()
    end

    def category(report_key, micro_water_shed)
      set_up(report_key, micro_water_shed, "category")
      report_data()
    end

    def data()
      self.frmr(@type, @micro_water_shed).joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").count
    end

    def init_data()
      names(@column).product(names(@type))
    end

    def init()
      @list_product = init_data()
	    @hash = Hash.new
	    @list_product.each{|list| @hash[list] = 0}
      @hash 
    end

    def data_merge()
      init().merge(data())
    end

  
    # type_item's are the actual items of the type 
    # Example: 
    # If type is Topography, then the type_items are "Ridge", "Mid" and "Valley"
    def total(_data, type_item)
      @total_value = _data.collect{ |key, value|  key[1] == type_item ? value.to_f : 0 }.reduce(:+)
    end

    #     def total(_data, type_item)

    #   if @total == 'individual'
    # 	  @total_value = _data.collect{ |key, value|  key[1] == type_item ? value.to_i : 0 }.reduce(:+)
    #   else
    #     @f_count = farmer_count()

    #     @total_value = @f_count.keys.include?(type_item) ? @f_count[type_item] : 0 
    #   end
    #   @total_value
    # end

    def farmer_total(f_count, type_item)
      f_count.keys.include?(type_item) ? f_count[type_item] : 0 
    end

    def farmer_count(type, mws, report)
      if report["total"] == 'individual'
        @farmer_count = {}
        __data = self.frmr(type, mws).joins(report['column'].to_sym).group("#{plural(report['column'])}.name", "#{plural(type)}.name").count
        __data.collect{|k,v| @farmer_count[k[1]] =  (@farmer_count[k[1]].blank? ? 0 : @farmer_count[k[1]]).to_i + v}
      else
        @farmer_count = Farmer.joins(:village).where("villages.micro_water_shed_id" => mws).joins(type.to_sym).group("#{plural(type)}.name").count
      end
      @farmer_count
    end


    # column is the column name for the x-axis in the chart 
    # Example: 
    # 	Gender - with table name 'genders', containing master data of the genders table/ Gender model class. 
    #      Identimws
    # 	Education - with table educations, contianing master data of the educations table/ Education model class
    #      Identified as education_id
    #
    # type - Type is the type of report. There are 3 types of reports
    #   
    #   1. Topography
    #   2. SMU
    #   3. Category - Farmer category based on the acers of land owned.
    #
    def report_data()

      _dest_data = destination_data()

      _data = data()
      _data_farmer = farmer_count(@type, @micro_water_shed, @report)
      data_merge().reject{|a|a[0].blank?}.each do |key, value| 
        item = _dest_data.find{| _item | _item["name"].strip == key[0].strip}
        item[key[1]] = value.blank? ? 0 : value
        item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key[1])
        if @label.include?('Avg') || @label.include?('Average') || @label.include?('Cropping intensity')
          item['all_average'] = data_average()[key[0]]
        end
      end
      _dest_data
    end

    def destination_data()
      JSON.parse(to_model(@column).all.to_json)
    end

    def plural name
  		name.pluralize
    end

  	def to_model(model_name)
  		model_name.classify.constantize
  	end

  	def names(column)
  		to_model(column).pluck(:name).uniq
  	end
  end
end