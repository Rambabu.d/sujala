class House < ApplicationRecord
	belongs_to :house_type
	belongs_to :farmer

	include Reportable
end
