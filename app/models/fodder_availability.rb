class FodderAvailability < ApplicationRecord
	belongs_to :farmer

	include Reportable

  def self.data()
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum('quantity')
  end	

  def self.init_data()
    ['Dry Fodder', 'Green Fodder'].product(names(@type))
  end

  def self.destination_data()
    ['Dry Fodder', 'Green Fodder'].collect{|col_name| {"name" => col_name}}
  end

  def self.farmer_total(f_count, type_item)
    _duration = self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum('duration')
    f_count.keys.include?(type_item[1]) ? (_duration[type_item] / f_count[type_item[1]]) : 0 
  end

  def self.report_data()

      _dest_data = destination_data()

      _data = data()
      _data_farmer = farmer_count(@type, @micro_water_shed, @report)
      #byebug
      data_merge().reject{|a| a[0].blank?}.each do |key, value| 
        item = _dest_data.find{| _item | _item["name"] == key[0]}
        item[key[1]] = value.blank? ? 0 : value
        item[key[1] + "_total"] = @total == 'individual' ? total(_data, key[1]) : farmer_total(_data_farmer, key)
        if @label.include?('Avg') || @label.include?('Average') || @label.include?('Cropping intensity')
          item['all_average'] = data_average()[key[0]]
        end
      end
      _dest_data
    end
    

 
end
