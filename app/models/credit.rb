class Credit < ApplicationRecord
	belongs_to :farmer

	include Reportable

	def self.data()

    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").count
  end

  def self.init_data()
    self.pluck(@column).compact.uniq.product(names(@type))
  end

  def self.destination_data()
    JSON.parse(Credit.group(@column).select(@column).to_json).reject{|a| a['name'].blank?}
  end

  def self.farmer_count(type, mws, report)
    BorrowingStatus.frmr(type, mws).where('borrowing_statuses.credit_availed = 1').group("#{plural(type)}.name").count
  end



end