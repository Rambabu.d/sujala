class Irrigation < ApplicationRecord

   belongs_to :farmer

  include Reportable

  def self.data()
    if @report['filter'] == 'water_depth'
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:water_depth)
    else
    self.frmr(@type, @micro_water_shed).group(@column, "#{plural(@type)}.name").sum(:working)
  end
  end

  def self.init_data()
    self.pluck(@column).uniq.product(names(@type))
  end

  def self.destination_data()
    JSON.parse(Irrigation.group(@column).select(@column).to_json)
  end

  def self.report_data()
    _dest_data = destination_data()

    _data_farmer =  farmer_count(@type, @micro_water_shed, @report)
    # farmer_count_irrigation = self.frmr(@type, @micro_water_shed).group("#{plural(@type)}.name").count(:id)
    _data_average = data_average()
    total_farmer_count_irrigation = _data_farmer.values.sum
    # total_farmer_count_irrigation = self.frmr(@type, @micro_water_shed).count(:id)
    data_merge().each do |key, value| 
      item = _dest_data.find{| itm | itm[@column] == key[0]}
      item["name"] = item[@column]
      item[key[1]] = @report['filter'] == 'water_depth' ? ( _data_farmer[key[1]].blank? ? 0 : (value / _data_farmer[key[1]] ) * 0.3048 ): value.to_f
      item[key[1] + "_total"] = farmer_total(_data_farmer, key[1]).to_f
      if @report['filter'] == 'water_depth'
          item['all_average'] =  ((_data_average[key[0]].blank? ? 0 : _data_average[key[0]]  / total_farmer_count_irrigation) * 0.3048 ) unless total_farmer_count_irrigation.blank?
        end
    end

    
    _dest_data
  end

  def self.data_average()
    self.frmr(@type, @micro_water_shed).group(@column).sum(:water_depth)
  end
end
