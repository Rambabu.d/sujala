class Village < ApplicationRecord
	has_many :farmers
	belongs_to :micro_water_shed
end
