class ProductionCost < ApplicationRecord
	belongs_to :crop_list
	belongs_to :intercrop_list
	has_many :pc_other_expenses
	has_many :pc_yield_prices
	belongs_to :farmer
	include Reportable 
end
