class WatershedAwareness < ApplicationRecord
  belongs_to :particular
  belongs_to :farmer

  include Reportable

	def self.par_id
		@particular_id = Particular.find_by_name(@report["particular_id"]).id
	end

  def self.data()
  	#@particular_id = Particular.find_by_name(@report["particular_id"]).id

    self.frmr(@type, @micro_water_shed)
    .where("watershed_awarenesses.particular_id = #{par_id}")
    .where("watershed_awarenesses.awareness like 'Yes'")
    .joins(@column.to_sym).group("#{plural(@column)}.name", "#{plural(@type)}.name").count
  end

    def self.init_data()
      [@report["particular_id"]].product(names(@type))
    end

   def self.destination_data()
       
      JSON.parse(to_model(@column).where("id = #{par_id}").to_json)
    end

    



end
