class LiveStockReport < ApplicationRecord
	belongs_to :farmer


  def self.data(micro_water_shed_id)
    
    self.joins(:farmer => [:village, :category]).includes(:farmer => :category).where("villages.micro_water_shed_id = #{micro_water_shed_id}")
      
     
  end

end
   
    