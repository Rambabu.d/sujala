json.extract! village, :id, :name, :micro_water_shed, :micro_water_shed_id, :is_active, :created_at, :updated_at
json.url village_url(village, format: :json)