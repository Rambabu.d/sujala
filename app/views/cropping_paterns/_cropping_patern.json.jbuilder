json.extract! cropping_patern, :id, :farmer_id, :survey_number, :total_land, :crop, :intercrop, :season, :created_at, :updated_at
json.url cropping_patern_url(cropping_patern, format: :json)