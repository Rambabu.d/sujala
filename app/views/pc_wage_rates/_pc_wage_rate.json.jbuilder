json.extract! pc_wage_rate, :id, :male, :female, :bullock_pair, :tractor, :machinery, :production_cost, :farmer_id, :user_id, :created_at, :updated_at, :irrigation_charge

json.crop_list pc_wage_rate.production_cost.crop_list

json.url pc_wage_rate_url(pc_wage_rate, format: :json)