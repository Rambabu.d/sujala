json.extract! taluk, :id, :name, :district, :district_id, :created_at, :updated_at
json.url taluk_url(taluk, format: :json)