json.extract! hobli, :id, :name, :taluk, :taluk_id, :created_at, :updated_at
json.url hobli_url(hobli, format: :json)