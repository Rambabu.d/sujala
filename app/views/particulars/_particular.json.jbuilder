json.extract! particular, :id, :name, :created_at, :updated_at
json.url particular_url(particular, format: :json)