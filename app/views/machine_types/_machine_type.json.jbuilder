json.extract! machine_type, :id, :name, :is_active, :created_at, :updated_at
json.url machine_type_url(machine_type, format: :json)