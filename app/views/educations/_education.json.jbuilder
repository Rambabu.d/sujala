json.extract! education, :id, :name, :is_active, :created_at, :updated_at
json.url education_url(education, format: :json)