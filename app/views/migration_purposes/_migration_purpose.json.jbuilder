json.extract! migration_purpose, :id, :name, :created_at, :updated_at
json.url migration_purpose_url(migration_purpose, format: :json)