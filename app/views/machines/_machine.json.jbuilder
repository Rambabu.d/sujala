json.extract! machine, :id, :machine_type, :quantity, :present_value, :created_at, :updated_at
json.url machine_url(machine, format: :json)