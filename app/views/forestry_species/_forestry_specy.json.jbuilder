json.extract! forestry_specy, :id, :farmer_id, :forestry_plant, :field, :backyard, :created_at, :updated_at
json.url forestry_specy_url(forestry_specy, format: :json)