json.extract! fertilizer, :id, :name, :created_at, :updated_at
json.url fertilizer_url(fertilizer, format: :json)