json.extract! livestock, :id, :farmer_id, :livestock_type, :quantity, :present_value, :created_at, :updated_at
json.url livestock_url(livestock, format: :json)