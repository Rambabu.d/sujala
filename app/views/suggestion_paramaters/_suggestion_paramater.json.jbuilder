json.extract! suggestion_paramater, :id, :name, :created_at, :updated_at
json.url suggestion_paramater_url(suggestion_paramater, format: :json)