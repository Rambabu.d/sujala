json.extract! ppc_detail, :id, :details_of_ppc_application, :own_men_mandays, :hired_men_mandays, :own_women_mandays, :hired_women_mandays, :own_bullock_days, :hired_bullock_days, :own_tractor_days, :hired_tractor_days, :input, :amount, :created_at,:production_cost, :farmer_id, :user_id, :updated_at, :sprays_count

json.crop_list ppc_detail.production_cost.crop_list

json.url ppc_detail_url(ppc_detail, format: :json)