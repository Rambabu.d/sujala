json.extract! user, :id, :name, :email

json.roles user.roles.pluck(:name)
