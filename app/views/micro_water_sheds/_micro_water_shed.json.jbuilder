json.extract! micro_water_shed, :id, :name, :code, :sub_water_shed, :sub_water_shed_id, :created_at, :updated_at
json.url micro_water_shed_url(micro_water_shed, format: :json)