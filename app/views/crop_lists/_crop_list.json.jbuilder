json.extract! crop_list, :id, :name, :created_at, :updated_at
json.url crop_list_url(crop_list, format: :json)