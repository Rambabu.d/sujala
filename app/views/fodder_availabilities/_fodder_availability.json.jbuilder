json.extract! fodder_availability, :id, :farmer_id, :fodder_type, :quantity, :duration, :adequacy, :created_at, :updated_at
json.url fodder_availability_url(fodder_availability, format: :json)