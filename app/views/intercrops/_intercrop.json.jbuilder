json.extract! intercrop, :id, :title, :created_at, :updated_at
json.url intercrop_url(intercrop, format: :json)