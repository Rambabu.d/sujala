json.extract! conversation_structure, :id, :name, :created_at, :updated_at
json.url conversation_structure_url(conversation_structure, format: :json)