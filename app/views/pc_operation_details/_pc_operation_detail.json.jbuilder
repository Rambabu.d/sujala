json.extract! pc_operation_detail, :id, :production_cost, :farmer_id, :user_id, :operation_detail, :own_men, :own_women, :hired_men, :hired_women, :own_bullock, :hired_bullock, :own_tractor, :hired_tractor, :own_machinery, :hired_machinery, :input, :amount, :created_at, :updated_at

json.crop_list pc_operation_detail.production_cost.crop_list

json.url pc_operation_detail_url(pc_operation_detail, format: :json)