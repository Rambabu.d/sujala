json.extract! basic_need, :id, :farmer_id, :response, :source, :minimum_need, :distance, :created_at, :updated_at
json.url basic_need_url(basic_need, format: :json)