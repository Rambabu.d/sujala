json.extract! constraint, :id, :name, :created_at, :updated_at
json.url constraint_url(constraint, format: :json)