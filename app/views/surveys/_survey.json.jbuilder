json.extract! survey, :id, :farmer_id, :survey_number, :type_of_land, :holding_acres, :present_land_value, :soil_type, :quality, :created_at, :updated_at
json.url survey_url(survey, format: :json)