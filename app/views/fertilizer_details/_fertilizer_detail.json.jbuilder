json.extract! fertilizer_detail, :id, :fertilizer, :own_men_mandays, :hired_men_mandays, :own_women_mandays, :hired_women_mandays, :own_bullock_days, :hired_bullock_days, :own_tractor_days, :hired_tractor_days, :input, :amount, :created_at, :updated_at, :production_cost, :farmer_id, :user_id, :no_of_times

json.crop_list fertilizer_detail.production_cost.crop_list
json.url fertilizer_detail_url(fertilizer_detail, format: :json)