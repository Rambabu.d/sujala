json.extract! credit_source, :id, :name, :created_at, :updated_at
json.url credit_source_url(credit_source, format: :json)