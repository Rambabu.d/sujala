json.extract! forestry_plant, :id, :name, :created_at, :updated_at
json.url forestry_plant_url(forestry_plant, format: :json)