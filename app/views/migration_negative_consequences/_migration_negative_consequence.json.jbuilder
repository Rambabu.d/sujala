json.extract! migration_negative_consequence, :id, :name, :created_at, :updated_at
json.url migration_negative_consequence_url(migration_negative_consequence, format: :json)