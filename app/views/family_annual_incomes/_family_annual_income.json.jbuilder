json.extract! family_annual_income, :id, :farmer_id, :income_source, :total_income, :total_expenditure, :created_at, :updated_at
json.url family_annual_income_url(family_annual_income, format: :json)