json.extract! plant_count, :id, :horticulture_plant, :field, :backyard, :created_at, :updated_at
json.url plant_count_url(plant_count, format: :json)