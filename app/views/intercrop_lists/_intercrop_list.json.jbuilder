json.extract! intercrop_list, :id, :name, :created_at, :updated_at
json.url intercrop_list_url(intercrop_list, format: :json)