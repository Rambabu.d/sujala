json.extract! crop_type, :id, :name, :created_at, :updated_at
json.url crop_type_url(crop_type, format: :json)