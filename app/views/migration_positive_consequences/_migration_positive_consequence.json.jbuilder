json.extract! migration_positive_consequence, :id, :name, :created_at, :updated_at
json.url migration_positive_consequence_url(migration_positive_consequence, format: :json)