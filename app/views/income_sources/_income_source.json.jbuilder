json.extract! income_source, :id, :name, :created_at, :updated_at
json.url income_source_url(income_source, format: :json)