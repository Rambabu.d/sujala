json.extract! socio_demographic_info, :id, :farmer_id, :farmer_member_name, :occupation, :education, :age, :gender, :local_organisation_member, :local_organisation, :created_at, :updated_at

json.organisation_member socio_demographic_info.organisation_member

json.url socio_demographic_info_url(socio_demographic_info, format: :json)