json.extract! housetype, :id, :name, :is_active, :created_at, :updated_at
json.url housetype_url(housetype, format: :json)