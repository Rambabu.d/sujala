json.extract! topography, :id, :name, :is_active, :created_at, :updated_at
json.url topography_url(topography, format: :json)