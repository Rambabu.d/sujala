json.cache_collection! @farmers, expires_in: 10.minutes do |farmer|
  json.partial! 'farmer', :farmer => farmer
end