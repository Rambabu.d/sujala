json.cache! ['v1', @farmer], expires_in: 10.minutes do
  json.partial! "farmers/farmer", farmer: @farmer
end