json.extract! farmer, :id, :name, :village, :hamlet, :caste, :aadhar, :father_name, :phone, :smu, :topography, :created_at, :updated_at, :grampanchayat

json.hobli farmer.grampanchayat.hobli
json.taluk farmer.grampanchayat.hobli.taluk
json.district farmer.grampanchayat.hobli.taluk.district

json.micro_water_shed farmer.village.micro_water_shed
json.sub_water_shed farmer.village.micro_water_shed.sub_water_shed

json.url farmer_url(farmer, format: :json)
