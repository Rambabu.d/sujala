json.extract! production_cost, :id, :crop_type,:crop_number,:crop_list,:intercrop_list,:extent_of_area,:season,:type_of_land,:irrigation_method,:establishment,:annual_maintenance,:age_of_garden,:lifespan_of_garden,:farmer_id, :created_at, :updated_at, :seed_qty, :plants_count, :seeding_count, :seed_price_inter

json.url production_cost_url(production_cost, format: :json)