json.extract! cultivate_horticulture, :id, :farmer_id, :horticulture_plant, :number, :created_at, :updated_at
json.url cultivate_horticulture_url(cultivate_horticulture, format: :json)