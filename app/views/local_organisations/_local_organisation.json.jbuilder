json.extract! local_organisation, :id, :name, :is_active, :created_at, :updated_at
json.url local_organisation_url(local_organisation, format: :json)