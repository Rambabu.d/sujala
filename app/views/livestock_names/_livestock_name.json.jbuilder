json.extract! livestock_name, :id, :name, :created_at, :updated_at
json.url livestock_name_url(livestock_name, format: :json)