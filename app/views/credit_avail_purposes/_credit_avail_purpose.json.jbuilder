json.extract! credit_avail_purpose, :id, :name, :credit_source, :created_at, :updated_at
json.url credit_avail_purpose_url(credit_avail_purpose, format: :json)