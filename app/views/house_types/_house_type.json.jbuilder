json.extract! house_type, :id, :name, :is_active, :created_at, :updated_at
json.url house_type_url(house_type, format: :json)