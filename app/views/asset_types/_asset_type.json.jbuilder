json.extract! asset_type, :id, :name, :is_active, :created_at, :updated_at
json.url asset_type_url(asset_type, format: :json)