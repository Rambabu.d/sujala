json.extract! watershed_comment, :id, :farmer_id, :comments, :created_at, :updated_at
json.url watershed_comment_url(watershed_comment, format: :json)