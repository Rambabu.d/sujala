json.extract! sub_water_shed, :id, :name, :code, :district_id, :district, :created_at, :updated_at
json.url sub_water_shed_url(sub_water_shed, format: :json)