json.extract! pc_other_expense, :id, :production_cost, :farmer_id, :user_id, :electricity, :insurance, :irrigated_rental_value, :dryland_rental_value, :managerial_cost, :land_revenue, :interest_on_variable_cost, :interest_on_fixed_cost, :miscellaneous, :created_at, :updated_at, :repairs_and_maintenance_charges, :seed_price_main, :seed_price_inter

json.crop_list pc_other_expense.production_cost.crop_list
json.url pc_other_expense_url(pc_other_expense, format: :json)

