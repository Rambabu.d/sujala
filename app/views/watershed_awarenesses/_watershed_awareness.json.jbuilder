json.extract! watershed_awareness, :id, :farmer_id, :particular, :awareness, :created_at, :updated_at
json.url watershed_awareness_url(watershed_awareness, format: :json)