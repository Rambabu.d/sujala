json.extract! improvement_suggestion, :id, :farmer_id, :suggestion_paramater, :suggestions, :created_at, :updated_at
json.url improvement_suggestion_url(improvement_suggestion, format: :json)