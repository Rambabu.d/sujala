json.extract! forestry_cultivation, :id, :farmer_id, :forestry_plant, :number, :created_at, :updated_at
json.url forestry_cultivation_url(forestry_cultivation, format: :json)