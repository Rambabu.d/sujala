json.extract! household_food_material, :id, :food_material, :status, :created_at, :updated_at
json.url household_food_material_url(household_food_material, format: :json)