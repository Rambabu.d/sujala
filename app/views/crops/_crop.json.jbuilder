json.extract! crop, :id, :title, :created_at, :updated_at
json.url crop_url(crop, format: :json)