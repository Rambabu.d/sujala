json.extract! farming_constraint, :id, :farmer_id, :constraint, :response, :created_at, :updated_at
json.url farming_constraint_url(farming_constraint, format: :json)