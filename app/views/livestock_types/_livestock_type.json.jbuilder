json.extract! livestock_type, :id, :name, :is_active, :created_at, :updated_at
json.url livestock_type_url(livestock_type, format: :json)