json.extract! food_material, :id, :name, :created_at, :updated_at
json.url food_material_url(food_material, format: :json)