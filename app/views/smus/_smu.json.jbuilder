json.extract! smu, :id, :name, :is_active, :created_at, :updated_at
json.url smu_url(smu, format: :json)