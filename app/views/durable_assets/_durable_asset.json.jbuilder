json.extract! durable_asset, :id, :asset_type, :farmer_id, :quantity, :present_value, :created_at, :updated_at
json.url durable_asset_url(durable_asset, format: :json)