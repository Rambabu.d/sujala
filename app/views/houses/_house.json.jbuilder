json.extract! house, :id, :house_type, :farmer_id, :quantity, :area, :created_at, :updated_at
json.url house_url(house, format: :json)