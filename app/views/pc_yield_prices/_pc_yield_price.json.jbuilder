json.extract! pc_yield_price, :id, :crop_main_product_yield, :crop_main_product_price, :crop_by_product_yield, :crop_by_product_price, :intercrop_main_product_yield, :intercrop_main_product_price, :intercrop_by_product_yield, :intercrop_by_product_price, :created_at, :updated_at, :production_cost, :farmer_id, :user_id, :value

json.crop_list pc_yield_price.production_cost.crop_list
json.intercrop_list pc_yield_price.production_cost.intercrop_list

json.url pc_yield_price_url(pc_yield_price, format: :json)