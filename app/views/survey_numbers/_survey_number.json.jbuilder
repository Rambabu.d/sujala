json.extract! survey_number, :id, :number, :hissa_number, :grid1, :grid2, :grid3, :grid4, :farmer_id, :created_at, :updated_at
json.url survey_number_url(survey_number, format: :json)