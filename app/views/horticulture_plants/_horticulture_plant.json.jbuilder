json.extract! horticulture_plant, :id, :name, :created_at, :updated_at
json.url horticulture_plant_url(horticulture_plant, format: :json)