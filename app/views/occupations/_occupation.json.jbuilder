json.extract! occupation, :id, :name, :is_active, :created_at, :updated_at
json.url occupation_url(occupation, format: :json)