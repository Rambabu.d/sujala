class AddColumnHasAnyoneMigratedToLabourAvailability < ActiveRecord::Migration[5.0]
  def change
    add_column :labour_availabilities, :has_anyone_migrated, :boolean, :default => false
  end
end
