class CreateFamilyAnnualIncomes < ActiveRecord::Migration[5.0]
  def change
    create_table :family_annual_incomes do |t|
      t.integer :farmer_id
      t.integer :income_source_id
      t.float :total_income
      t.float :total_expenditure

      t.timestamps
    end
  end
end
