class AddColumnRepairsAndMaintenanceChargesToPcOtherExpense < ActiveRecord::Migration[5.0]
  def change
    add_column :pc_other_expenses, :repairs_and_maintenance_charges, :float
  end
end
