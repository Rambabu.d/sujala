class CreateFertilizers < ActiveRecord::Migration[5.0]
  def change
    create_table :fertilizers do |t|
      t.string :name

      t.timestamps
    end
  end
end
