class CreateSocioDemographicInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :socio_demographic_infos do |t|
      t.integer :farmer_id
      t.string  :farmer_member_name
      t.string  :farmer_relation_id
      t.integer :occupation_id
      t.integer :education_id
      t.integer :age
      t.integer :gender_id
      t.boolean :local_organisation_member
      t.integer :local_organisation_id

      t.timestamps
    end
  end
end
