class CreateSubWaterSheds < ActiveRecord::Migration[5.0]
  def change
    create_table :sub_water_sheds do |t|
      t.string :name
      t.string :code
      t.integer :district_id

      t.timestamps
    end
  end
end
