class CreateSubsidiaryEnterprises < ActiveRecord::Migration[5.0]
  def change
    create_table :subsidiary_enterprises do |t|
      t.integer :livestock_name_id
      t.integer :no_of_animals
      t.integer :no_of_milching_animals
      t.float :present_value_of_animals
      t.float :monthly_feed_dry
      t.float :monthly_feed_green
      t.float :monthly_feed_conc
      t.integer :labour_no_of_hrs
      t.float :labour_amount
      t.float :healthcare_expenses
      t.float :milk
      t.float :lacation_period
      t.float :manure
      t.float :price_per_unit

      t.timestamps
    end
  end
end
