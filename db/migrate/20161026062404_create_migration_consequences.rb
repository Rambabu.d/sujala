class CreateMigrationConsequences < ActiveRecord::Migration[5.0]
  def change
    create_table :migration_consequences do |t|
      t.integer :migration_positive_consequence_id
      t.integer :migration_negative_consequence_id
      t.integer :farmer_id
      t.integer :user_id

      t.timestamps
    end
  end
end
