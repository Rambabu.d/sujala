class CreateHoblis < ActiveRecord::Migration[5.0]
  def change
    create_table :hoblis do |t|
      t.string :name
      t.integer :taluk_id

      t.timestamps
    end
  end
end
