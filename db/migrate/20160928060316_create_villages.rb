class CreateVillages < ActiveRecord::Migration[5.0]
  def change
    create_table :villages do |t|
      t.string :name
      t.integer :micro_water_shed_id
      t.boolean :is_active

      t.timestamps
    end
  end
end
