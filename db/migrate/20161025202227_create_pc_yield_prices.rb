class CreatePcYieldPrices < ActiveRecord::Migration[5.0]
  def change
    create_table :pc_yield_prices do |t|
      t.integer :crop_main_product_yield
      t.float :crop_main_product_price
      t.integer :crop_by_product_yield
      t.float :crop_by_product_price
      t.integer :intercrop_main_product_yield
      t.float :intercrop_main_product_price
      t.integer :intercrop_by_product_yield
      t.float :intercrop_by_product_price
      t.integer :production_cost_id
      t.integer :farmer_id
      t.integer :user_id
      t.float :value

      t.timestamps
    end
  end
end
