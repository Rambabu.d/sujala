class CreatePcOtherExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :pc_other_expenses do |t|
      t.integer :production_cost_id
      t.integer :farmer_id
      t.integer :user_id
      t.float :electricity
      t.float :insurance
      t.float :irrigated_rental_value
      t.float :dryland_rental_value
      t.float :managerial_cost
      t.float :land_revenue
      t.float :interest_on_variable_cost
      t.float :interest_on_fixed_cost
      t.float :miscellaneous

      t.timestamps
    end
  end
end
