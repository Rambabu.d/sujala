class CreateLocalOrganisations < ActiveRecord::Migration[5.0]
  def change
    create_table :local_organisations do |t|
      t.string :name
      t.boolean :is_active

      t.timestamps
    end
  end
end
