class AddSpraysCountToPpcDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :ppc_details, :sprays_count, :integer
  end
end
