class CreateWatershedComments < ActiveRecord::Migration[5.0]
  def change
    create_table :watershed_comments do |t|
      t.text :comments

      t.timestamps
    end
  end
end
