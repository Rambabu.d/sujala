class CreateBorrowingStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :borrowing_statuses do |t|
      t.boolean :have_bank_account
      t.boolean :have_savings
      t.boolean :credit_availed
      t.float :amount
      t.boolean :amount_availed_adequate

      t.timestamps
    end
  end
end
