class CreatePpcDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :ppc_details do |t|
      t.string :details_of_ppc_application
      t.integer :own_men_mandays
      t.integer :hired_men_mandays
      t.integer :own_women_mandays
      t.integer :hired_women_mandays
      t.integer :own_bullock_days
      t.integer :hired_bullock_days
      t.integer :own_tractor_days
      t.integer :hired_tractor_days
      t.integer :input
      t.float :amount
      t.integer :production_cost_id
      t.integer :farmer_id
      t.integer :user_id
      
      t.timestamps
    end
  end
end
