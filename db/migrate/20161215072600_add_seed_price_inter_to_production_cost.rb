class AddSeedPriceInterToProductionCost < ActiveRecord::Migration[5.0]
  def change
    add_column :production_costs, :seed_price_inter, :integer
  end
end
