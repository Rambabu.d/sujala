class CreateSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :surveys do |t|
      t.integer :farmer_id
      t.integer :survey_number_id
      t.string :type_of_land
      t.integer :holding_acres
      t.float :present_land_value
      t.string :soil_type
      t.string :quality

      t.timestamps
    end
  end
end
