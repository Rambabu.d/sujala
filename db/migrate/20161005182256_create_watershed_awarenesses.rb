class CreateWatershedAwarenesses < ActiveRecord::Migration[5.0]
  def change
    create_table :watershed_awarenesses do |t|
      t.integer :particular_id
      t.boolean :awareness

      t.timestamps
    end
  end
end
