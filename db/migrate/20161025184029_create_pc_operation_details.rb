class CreatePcOperationDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :pc_operation_details do |t|
      t.integer :production_cost_id
      t.integer :farmer_id
      t.integer :user_id
      t.string :operation_detail
      t.integer :own_men
      t.integer :own_women
      t.integer :hired_men
      t.integer :hired_women
      t.integer :own_bullock
      t.integer :hired_bullock
      t.integer :own_tractor
      t.integer :hired_tractor
      t.integer :own_machinery
      t.integer :hired_machinery
      t.integer :input
      t.float :amount

      t.timestamps
    end
  end
end
