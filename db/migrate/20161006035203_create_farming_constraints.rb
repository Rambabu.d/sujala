class CreateFarmingConstraints < ActiveRecord::Migration[5.0]
  def change
    create_table :farming_constraints do |t|
      t.integer :constraint_id
      t.string :response

      t.timestamps
    end
  end
end
