class CreateParticulars < ActiveRecord::Migration[5.0]
  def change
    create_table :particulars do |t|
      t.string :name

      t.timestamps
    end
  end
end
