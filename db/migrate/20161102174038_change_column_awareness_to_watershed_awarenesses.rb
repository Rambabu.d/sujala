class ChangeColumnAwarenessToWatershedAwarenesses < ActiveRecord::Migration[5.0]
  def change
  	change_column :watershed_awarenesses, :awareness, :string
  end
end
