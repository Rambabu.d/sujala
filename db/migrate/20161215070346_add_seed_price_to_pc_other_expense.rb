class AddSeedPriceToPcOtherExpense < ActiveRecord::Migration[5.0]
  def change
    add_column :pc_other_expenses, :seed_price_main, :integer
    add_column :pc_other_expenses, :seed_price_inter, :integer
  end
end
