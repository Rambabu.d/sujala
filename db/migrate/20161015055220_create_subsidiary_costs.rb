class CreateSubsidiaryCosts < ActiveRecord::Migration[5.0]
  def change
    create_table :subsidiary_costs do |t|
      t.float :dry_fodder
      t.float :green_fodder
      t.float :concentrates
      t.float :manure
      t.float :milk_price
      t.float :lacation_period

      t.timestamps
    end
  end
end
