class CreateBasicNeeds < ActiveRecord::Migration[5.0]
  def change
    create_table :basic_needs do |t|
      t.string :fuel_use_response
      t.string :drinking_water_response
      t.string :fodder_response
      t.text :fuel_source
      t.integer :fuel_distance
      t.text :water_source
      t.integer :water_distance
      t.text :fodder_source
      t.integer :fodder_distance
      t.string :light
      t.string :toilet_facility
      t.string :health_card
      t.string :pds_card
      t.string :work_nrgea
      t.integer :no_of_persons
      t.integer :no_of_days

      t.timestamps
    end
  end
end
