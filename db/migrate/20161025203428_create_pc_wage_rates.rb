class CreatePcWageRates < ActiveRecord::Migration[5.0]
  def change
    create_table :pc_wage_rates do |t|
      t.float :male
      t.float :female
      t.float :bullock_pair
      t.float :tractor
      t.float :machinery
      t.integer :production_cost_id
      t.integer :farmer_id
      t.integer :user_id

      t.timestamps
    end
  end
end
