class CreateRoleRactions < ActiveRecord::Migration[5.0]
  def change
    create_table :role_ractions do |t|
      t.integer :role_id
      t.integer :raction_id

      t.timestamps
    end
  end
end
