class CreateMarketings < ActiveRecord::Migration[5.0]
  def change
    create_table :marketings do |t|
      t.integer :crop_id
      t.float :output_obtained
      t.float :output_retained
      t.float :output_sold
      t.string :type_of_market
      t.string :mode_of_transport
      t.float :price_obtained

      t.timestamps
    end
  end
end
