class AddNoOfTimesToFertilizerDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :fertilizer_details, :no_of_times, :integer
  end
end
