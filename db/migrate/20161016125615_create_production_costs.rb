class CreateProductionCosts < ActiveRecord::Migration[5.0]
  def change
    create_table :production_costs do |t|
      t.string :crop_type
      t.integer :crop_number
      t.integer :crop_list_id
      t.integer :intercrop_list_id
      t.float :extent_of_area
      t.string :season
      t.string :type_of_land
      t.string :irrigation_method
      t.float :establishment
      t.float :annual_maintenance
      t.float :age_of_garden
      t.float :lifespan_of_garden
      t.integer :farmer_id


      t.timestamps
    end
  end
end
