class CreateRactions < ActiveRecord::Migration[5.0]
  def change
    create_table :ractions do |t|
      t.string :name

      t.timestamps
    end
  end
end
