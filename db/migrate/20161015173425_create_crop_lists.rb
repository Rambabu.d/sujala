class CreateCropLists < ActiveRecord::Migration[5.0]
  def change
    create_table :crop_lists do |t|
      t.string :name

      t.timestamps
    end
  end
end
