class CreateLivestocks < ActiveRecord::Migration[5.0]
  def change
    create_table :livestocks do |t|
      t.integer :livestock_type_id
      t.integer :farmer_id
      t.integer :quantity
      t.integer :present_value
      

      t.timestamps
    end
  end
end
