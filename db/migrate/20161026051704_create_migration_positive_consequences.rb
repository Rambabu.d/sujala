class CreateMigrationPositiveConsequences < ActiveRecord::Migration[5.0]
  def change
    create_table :migration_positive_consequences do |t|
      t.string :name

      t.timestamps
    end
  end
end
