class AddFarmerIdToAllTables < ActiveRecord::Migration[5.0]
  def change
   tables = ["surveys", "irrigations", "cropping_paterns", "cropping_intensities", "fodder_availabilities", "income_sources", "family_annual_incomes", "additional_investment_capacities", "marketings", "particulars", "watershed_awarenesses", "conversation_structures", "conversation_practices", "farming_constraints", "food_materials", "household_food_materials", "suggestion_paramaters", "improvement_suggestions", "watershed_comments", "labour_availabilities", "basic_needs", "horticulture_plants", "forestry_plants", "credit_sources", "borrowing_statuses", "credit_avail_purposes", "institutional_credits", "private_credits", "subsidiary_enterprises", "subsidiary_costs", "plant_counts", "cultivate_horticultures", "forestry_species", "forestry_cultivations", "ppc_details", "fertilizer_details" ]
   tables.each do |table|
     if not ActiveRecord::Base.connection.column_exists?(table, :farmer_id)
       add_column table, :farmer_id, :integer
     end
   end
  end
end
