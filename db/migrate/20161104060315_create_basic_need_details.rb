class CreateBasicNeedDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :basic_need_details do |t|
      t.string :light
      t.string :toilet_facility
      t.string :health_card
      t.string :pds_card
      t.string :work_nrgea
      t.integer :no_of_persons
      t.integer :no_of_days
      t.integer :hours_of_electricity
      t.integer :farmer_id
      t.integer :user_id

      t.timestamps
    end
  end
end
