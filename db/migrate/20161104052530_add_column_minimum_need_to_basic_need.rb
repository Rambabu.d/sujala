class AddColumnMinimumNeedToBasicNeed < ActiveRecord::Migration[5.0]
  def change
    add_column :basic_needs, :minimum_need, :string
    add_column :basic_needs, :response, :string
    add_column :basic_needs, :source, :string
    add_column :basic_needs, :distance, :float
    remove_column :basic_needs, :fuel_use_response, :string
    remove_column :basic_needs, :drinking_water_response, :string
    remove_column :basic_needs, :fodder_response, :string
    remove_column :basic_needs, :fuel_source, :text
    remove_column :basic_needs, :fuel_distance, :integer
    remove_column :basic_needs, :water_source, :text
    remove_column :basic_needs, :water_distance, :integer
    remove_column :basic_needs, :fodder_source, :text
    remove_column :basic_needs, :fodder_distance, :integer
    remove_column :basic_needs, :light, :string
    remove_column :basic_needs, :toilet_facility, :string
    remove_column :basic_needs, :health_card, :string
    remove_column :basic_needs, :pds_card, :string
    remove_column :basic_needs, :work_nrgea, :string
    remove_column :basic_needs, :no_of_persons, :integer
    remove_column :basic_needs, :no_of_days, :integer
    
  end
end
