class CreateAdditionalInvestmentCapacities < ActiveRecord::Migration[5.0]
  def change
    create_table :additional_investment_capacities do |t|
      t.integer :purpose_id
      t.float :extent_of_investment
      t.string :source_of_funds

      t.timestamps
    end
  end
end
