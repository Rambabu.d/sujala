class CreateIrrigations < ActiveRecord::Migration[5.0]
  def change
    create_table :irrigations do |t|
      t.integer :farmer_id
      t.string :source_of_irrigation
      t.integer :defunct
      t.integer :working
      t.integer :water_depth
      t.integer :yield
      t.integer :kharif
      t.integer :rabi
      t.integer :summer
      t.integer :perennial_cops
      t.string :water_quality

      t.timestamps
    end
  end
end
