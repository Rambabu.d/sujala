class AddUserIdToAllTables < ActiveRecord::Migration[5.0]
  def change
    tables = ActiveRecord::Base.connection.tables - ["schema_migrations", "ar_internal_metadata"]
    tables.each do |table|
      if not ActiveRecord::Base.connection.column_exists?(table, :user_id)
        add_column table, :user_id, :integer
      end
    end
  end
end
