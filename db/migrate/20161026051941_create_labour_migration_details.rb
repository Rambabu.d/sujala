class CreateLabourMigrationDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :labour_migration_details do |t|
      t.integer :socio_demographic_info_id
      t.string :gender
      t.integer :duration
      t.integer :distance_migrated
      t.string :place
      t.integer :migration_purpose_id
      t.float :savings
      t.integer :farmer_id
      t.integer :user_id

      t.timestamps
    end
  end
end
