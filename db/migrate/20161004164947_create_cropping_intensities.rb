class CreateCroppingIntensities < ActiveRecord::Migration[5.0]
  def change
    create_table :cropping_intensities do |t|
      t.integer :farmer_id
      t.integer :survey_number_id
      t.float :total_land
      t.integer :kharif
      t.integer :rabi
      t.integer :summer
      t.integer :crop_id
      t.integer :intercrop_id

      t.timestamps
    end
  end
end
