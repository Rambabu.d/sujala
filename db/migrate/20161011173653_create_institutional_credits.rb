class CreateInstitutionalCredits < ActiveRecord::Migration[5.0]
  def change
    create_table :institutional_credits do |t|
      t.string :source
      t.float :institution_amount
      t.float :rate_of_interest
      t.integer :credit_avail_purpose_id
      t.string :repayment_status
      t.string :reactions

      t.timestamps
    end
  end
end
