class CreateConversationStructures < ActiveRecord::Migration[5.0]
  def change
    create_table :conversation_structures do |t|
      t.string :name

      t.timestamps
    end
  end
end
