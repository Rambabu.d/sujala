class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :houses do |t|
      t.integer :farmer_id
      t.string :house_type_id
      t.boolean :is_active
      t.integer :quantity
      t.integer :area

      t.timestamps
    end
  end
end
