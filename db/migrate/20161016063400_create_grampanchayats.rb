class CreateGrampanchayats < ActiveRecord::Migration[5.0]
  def change
    create_table :grampanchayats do |t|
      t.string :name
      t.integer :hobli_id

      t.timestamps
    end
  end
end
