class ChangeLocalOrganisationMemberDataTypeToSocioDemographicInfo < ActiveRecord::Migration[5.0]
  def change
  	change_column :socio_demographic_infos, :local_organisation_member, :integer
  end
end
