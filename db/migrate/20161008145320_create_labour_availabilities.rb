class CreateLabourAvailabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :labour_availabilities do |t|
      t.integer :farmer_id
      t.integer :family_labour_men
      t.integer :family_labour_women
      t.string :family_adequacy
      t.integer :hired_labour_men
      t.integer :hired_labour_women
      t.string :hired_adequacy

      t.timestamps
    end
  end
end
