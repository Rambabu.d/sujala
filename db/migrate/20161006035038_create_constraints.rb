class CreateConstraints < ActiveRecord::Migration[5.0]
  def change
    create_table :constraints do |t|
      t.string :name

      t.timestamps
    end
  end
end
