class CreateImprovementSuggestions < ActiveRecord::Migration[5.0]
  def change
    create_table :improvement_suggestions do |t|
      t.integer :suggestion_paramater_id
      t.text :suggestions

      t.timestamps
    end
  end
end
