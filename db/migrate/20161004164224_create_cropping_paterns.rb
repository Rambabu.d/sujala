class CreateCroppingPaterns < ActiveRecord::Migration[5.0]
  def change
    create_table :cropping_paterns do |t|
      t.integer :farmer_id
      t.integer :survey_number_id
      t.float :total_land
      t.integer :crop_id
      t.integer :intercrop_id
      t.string :season

      t.timestamps
    end
  end
end
