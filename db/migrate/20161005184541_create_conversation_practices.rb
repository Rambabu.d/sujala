class CreateConversationPractices < ActiveRecord::Migration[5.0]
  def change
    create_table :conversation_practices do |t|
      t.integer :conversation_structure_id
      t.integer :year
      t.string :status
      t.string :implemented_by
      t.string :name_of_agency

      t.timestamps
    end
  end
end
