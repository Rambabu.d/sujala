class CreateMicroWaterSheds < ActiveRecord::Migration[5.0]
  def change
    create_table :micro_water_sheds do |t|
      t.string :name
      t.string :code
      t.integer :sub_water_shed_id

      t.timestamps
    end
  end
end
