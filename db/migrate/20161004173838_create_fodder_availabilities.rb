class CreateFodderAvailabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :fodder_availabilities do |t|
      t.integer :farmer_id
      t.string :fodder_type
      t.float :quantity
      t.integer :duration
      t.string :adequacy

      t.timestamps
    end
  end
end
