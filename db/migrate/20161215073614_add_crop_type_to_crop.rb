class AddCropTypeToCrop < ActiveRecord::Migration[5.0]
  def change
    add_column :crops, :crop_type_id, :integer
  end
end
