class CreateDurableAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :durable_assets do |t|
      t.integer :farmer_id
      t.integer :asset_type_id
      t.integer :quantity
      t.integer :present_value
      

      t.timestamps
    end
  end
end
