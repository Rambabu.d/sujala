class AddColumnToProductionCost < ActiveRecord::Migration[5.0]
  def change
    add_column :production_costs, :seed_qty, :float
    add_column :production_costs, :plants_count, :integer
    add_column :production_costs, :seeding_count, :integer
  end
end
