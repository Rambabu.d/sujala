class CreatePlantCounts < ActiveRecord::Migration[5.0]
  def change
    create_table :plant_counts do |t|
      t.integer :horticulture_plant_id
      t.integer :field
      t.integer :backyard

      t.timestamps
    end
  end
end
