class CreateFarmers < ActiveRecord::Migration[5.0]
  def change
    create_table :farmers do |t|
      t.string :name
      t.integer :village_id
      t.string :hamlet
      t.integer :caste_id
      t.string :aadhar
      t.string :father_name
      t.string :phone
      t.integer :smu_id
      t.integer :topography_id
      t.integer :grampanchayat_id


      t.timestamps
    end
  end
end
