class AddIrrigationChargeToPcWageRate < ActiveRecord::Migration[5.0]
  def change
    add_column :pc_wage_rates, :irrigation_charge, :integer
  end
end
