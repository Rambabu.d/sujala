class CreateHouseholdFoodMaterials < ActiveRecord::Migration[5.0]
  def change
    create_table :household_food_materials do |t|
      t.integer :food_material_id
      t.string :status

      t.timestamps
    end
  end
end
