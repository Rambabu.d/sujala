class CreateHorticulturePlants < ActiveRecord::Migration[5.0]
  def change
    create_table :horticulture_plants do |t|
      t.string :name

      t.timestamps
    end
  end
end
