class CreateMigrationPurposes < ActiveRecord::Migration[5.0]
  def change
    create_table :migration_purposes do |t|
      t.string :name

      t.timestamps
    end
  end
end
