class CreateLivestockNames < ActiveRecord::Migration[5.0]
  def change
    create_table :livestock_names do |t|
      t.string :name

      t.timestamps
    end
  end
end
