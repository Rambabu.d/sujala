class CreateForestryCultivations < ActiveRecord::Migration[5.0]
  def change
    create_table :forestry_cultivations do |t|
      t.integer :forestry_plant_id
      t.integer :number

      t.timestamps
    end
  end
end
