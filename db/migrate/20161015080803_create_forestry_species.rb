class CreateForestrySpecies < ActiveRecord::Migration[5.0]
  def change
    create_table :forestry_species do |t|
      t.integer :forestry_plant_id
      t.integer :field
      t.integer :backyard

      t.timestamps
    end
  end
end
