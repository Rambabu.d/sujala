class AddCropTypeToProductionCost < ActiveRecord::Migration[5.0]
  def change
    add_column :production_costs, :crop_type_id, :integer
  end
end
