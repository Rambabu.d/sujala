class CreateSuggestionParamaters < ActiveRecord::Migration[5.0]
  def change
    create_table :suggestion_paramaters do |t|
      t.string :name

      t.timestamps
    end
  end
end
