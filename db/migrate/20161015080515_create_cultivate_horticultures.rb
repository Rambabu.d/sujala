class CreateCultivateHorticultures < ActiveRecord::Migration[5.0]
  def change
    create_table :cultivate_horticultures do |t|
      t.integer :horticulture_plant_id
      t.integer :number

      t.timestamps
    end
  end
end
