class ChangeColumnDatatypesToTables < ActiveRecord::Migration[5.0]
  def change
  	change_column :surveys, :holding_acres, :float
  	change_column :irrigations, :water_depth, :float
  	change_column :irrigations, :yield, :float
  	change_column :irrigations, :kharif, :float
  	change_column :irrigations, :rabi, :float
  	change_column :irrigations, :summer, :float
  	change_column :irrigations, :perennial_cops, :float
  	change_column :cropping_intensities, :kharif, :float
  	change_column :cropping_intensities, :rabi, :float
  	change_column :cropping_intensities, :summer, :float
  end
end
