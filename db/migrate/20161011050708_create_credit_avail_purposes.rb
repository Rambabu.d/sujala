class CreateCreditAvailPurposes < ActiveRecord::Migration[5.0]
  def change
    create_table :credit_avail_purposes do |t|
      t.integer :credit_source_id
      t.string :name

      t.timestamps
    end
  end
end
