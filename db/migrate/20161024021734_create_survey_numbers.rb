class CreateSurveyNumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :survey_numbers do |t|
      t.string :number
      t.string :hissa_number
      t.string :grid1
      t.string :grid2
      t.string :grid3
      t.string :grid4
      t.integer :farmer_id

      t.timestamps
    end
  end
end
