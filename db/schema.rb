# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161215073614) do

  create_table "actions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "additional_investment_capacities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "purpose_id"
    t.float    "extent_of_investment", limit: 24
    t.string   "source_of_funds"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "ar_internal_metadata", primary_key: "key", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "asset_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "basic_need_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "light"
    t.string   "toilet_facility"
    t.string   "health_card"
    t.string   "pds_card"
    t.string   "work_nrgea"
    t.integer  "no_of_persons"
    t.integer  "no_of_days"
    t.integer  "hours_of_electricity"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "basic_needs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.string   "minimum_need"
    t.string   "response"
    t.string   "source"
    t.float    "distance",     limit: 24
  end

  create_table "borrowing_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.boolean  "have_bank_account"
    t.boolean  "have_savings"
    t.boolean  "credit_availed"
    t.float    "amount",                  limit: 24
    t.boolean  "amount_availed_adequate"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "castes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "constraints", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "conversation_practices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "conversation_structure_id"
    t.integer  "year"
    t.string   "status"
    t.string   "implemented_by"
    t.string   "name_of_agency"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "conversation_structures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "credit_avail_purposes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "credit_source_id"
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "credit_sources", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "crop_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "crop_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cropping_intensities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "survey_number_id"
    t.float    "total_land",       limit: 24
    t.float    "kharif",           limit: 24
    t.float    "rabi",             limit: 24
    t.float    "summer",           limit: 24
    t.integer  "crop_id"
    t.integer  "intercrop_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
  end

  create_table "cropping_paterns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "survey_number_id"
    t.float    "total_land",       limit: 24
    t.integer  "crop_id"
    t.integer  "intercrop_id"
    t.string   "season"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
  end

  create_table "crops", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
    t.integer  "crop_type_id"
  end

  create_table "cultivate_horticultures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "horticulture_plant_id"
    t.integer  "number"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "districts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "durable_assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "asset_type_id"
    t.integer  "quantity"
    t.integer  "present_value"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
  end

  create_table "educations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "family_annual_incomes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "income_source_id"
    t.float    "total_income",      limit: 24
    t.float    "total_expenditure", limit: 24
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "user_id"
  end

  create_table "farmers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "village_id"
    t.string   "hamlet"
    t.integer  "caste_id"
    t.string   "aadhar"
    t.string   "father_name"
    t.string   "phone"
    t.integer  "smu_id"
    t.integer  "topography_id"
    t.integer  "grampanchayat_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "user_id"
  end

  create_table "farming_constraints", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "constraint_id"
    t.string   "response"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "fertilizer_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "fertilizer_id"
    t.integer  "own_men_mandays"
    t.integer  "hired_men_mandays"
    t.integer  "own_women_mandays"
    t.integer  "hired_women_mandays"
    t.integer  "own_bullock_days"
    t.integer  "hired_bullock_days"
    t.integer  "own_tractor_days"
    t.integer  "hired_tractor_days"
    t.integer  "input"
    t.float    "amount",              limit: 24
    t.integer  "production_cost_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "no_of_times"
  end

  create_table "fertilizers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "fodder_availabilities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.string   "fodder_type"
    t.float    "quantity",    limit: 24
    t.integer  "duration"
    t.string   "adequacy"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id"
  end

  create_table "food_materials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "forestry_cultivations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "forestry_plant_id"
    t.integer  "number"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "forestry_plants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "forestry_species", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "forestry_plant_id"
    t.integer  "field"
    t.integer  "backyard"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "genders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "grampanchayats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "hobli_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "hoblis", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "taluk_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "horticulture_plants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "house_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "household_food_materials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "food_material_id"
    t.string   "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "houses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.string   "house_type_id"
    t.boolean  "is_active"
    t.integer  "quantity"
    t.integer  "area"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
  end

  create_table "housetypes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "improvement_suggestions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "suggestion_paramater_id"
    t.text     "suggestions",             limit: 65535
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "income_sources", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "institutional_credits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "source"
    t.float    "institution_amount",      limit: 24
    t.float    "rate_of_interest",        limit: 24
    t.integer  "credit_avail_purpose_id"
    t.string   "repayment_status"
    t.string   "reactions"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "intercrop_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "intercrops", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "irrigations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.string   "source_of_irrigation"
    t.integer  "defunct"
    t.integer  "working"
    t.float    "water_depth",          limit: 24
    t.float    "yield",                limit: 24
    t.float    "kharif",               limit: 24
    t.float    "rabi",                 limit: 24
    t.float    "summer",               limit: 24
    t.float    "perennial_cops",       limit: 24
    t.string   "water_quality"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "user_id"
  end

  create_table "labour_availabilities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "family_labour_men"
    t.integer  "family_labour_women"
    t.string   "family_adequacy"
    t.integer  "hired_labour_men"
    t.integer  "hired_labour_women"
    t.string   "hired_adequacy"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "user_id"
    t.boolean  "has_anyone_migrated", default: false
  end

  create_table "labour_migration_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "socio_demographic_info_id"
    t.string   "gender"
    t.integer  "duration"
    t.integer  "distance_migrated"
    t.string   "place"
    t.integer  "migration_purpose_id"
    t.float    "savings",                   limit: 24
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "livestock_names", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "livestock_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "livestocks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "livestock_type_id"
    t.integer  "farmer_id"
    t.integer  "quantity"
    t.integer  "present_value"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "user_id"
  end

  create_table "local_organisations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "machine_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "machines", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "machine_type_id"
    t.integer  "quantity"
    t.integer  "present_value"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
  end

  create_table "marketings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "crop_id"
    t.float    "output_obtained",   limit: 24
    t.float    "output_retained",   limit: 24
    t.float    "output_sold",       limit: 24
    t.string   "type_of_market"
    t.string   "mode_of_transport"
    t.float    "price_obtained",    limit: 24
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "micro_water_sheds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "sub_water_shed_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "user_id"
  end

  create_table "migration_consequences", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "migration_positive_consequence_id"
    t.integer  "migration_negative_consequence_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "migration_negative_consequences", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "migration_positive_consequences", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "migration_purposes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "occupations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "particulars", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "pc_operation_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "production_cost_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.string   "operation_detail"
    t.integer  "own_men"
    t.integer  "own_women"
    t.integer  "hired_men"
    t.integer  "hired_women"
    t.integer  "own_bullock"
    t.integer  "hired_bullock"
    t.integer  "own_tractor"
    t.integer  "hired_tractor"
    t.integer  "own_machinery"
    t.integer  "hired_machinery"
    t.integer  "input"
    t.float    "amount",             limit: 24
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "pc_other_expenses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "production_cost_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.float    "electricity",                     limit: 24
    t.float    "insurance",                       limit: 24
    t.float    "irrigated_rental_value",          limit: 24
    t.float    "dryland_rental_value",            limit: 24
    t.float    "managerial_cost",                 limit: 24
    t.float    "land_revenue",                    limit: 24
    t.float    "interest_on_variable_cost",       limit: 24
    t.float    "interest_on_fixed_cost",          limit: 24
    t.float    "miscellaneous",                   limit: 24
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.float    "repairs_and_maintenance_charges", limit: 24
    t.integer  "seed_price_main"
    t.integer  "seed_price_inter"
  end

  create_table "pc_wage_rates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "male",               limit: 24
    t.float    "female",             limit: 24
    t.float    "bullock_pair",       limit: 24
    t.float    "tractor",            limit: 24
    t.float    "machinery",          limit: 24
    t.integer  "production_cost_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "irrigation_charge"
  end

  create_table "pc_yield_prices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "crop_main_product_yield"
    t.float    "crop_main_product_price",      limit: 24
    t.integer  "crop_by_product_yield"
    t.float    "crop_by_product_price",        limit: 24
    t.integer  "intercrop_main_product_yield"
    t.float    "intercrop_main_product_price", limit: 24
    t.integer  "intercrop_by_product_yield"
    t.float    "intercrop_by_product_price",   limit: 24
    t.integer  "production_cost_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.float    "value",                        limit: 24
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "plant_counts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "horticulture_plant_id"
    t.integer  "field"
    t.integer  "backyard"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "ppc_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "details_of_ppc_application"
    t.integer  "own_men_mandays"
    t.integer  "hired_men_mandays"
    t.integer  "own_women_mandays"
    t.integer  "hired_women_mandays"
    t.integer  "own_bullock_days"
    t.integer  "hired_bullock_days"
    t.integer  "own_tractor_days"
    t.integer  "hired_tractor_days"
    t.integer  "input"
    t.float    "amount",                     limit: 24
    t.integer  "production_cost_id"
    t.integer  "farmer_id"
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "sprays_count"
  end

  create_table "private_credits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "source"
    t.float    "private_amount",          limit: 24
    t.float    "rate_of_interest",        limit: 24
    t.integer  "credit_avail_purpose_id"
    t.string   "repayment_status"
    t.string   "reactions"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "production_costs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "crop_type"
    t.integer  "crop_number"
    t.integer  "crop_list_id"
    t.integer  "intercrop_list_id"
    t.float    "extent_of_area",     limit: 24
    t.string   "season"
    t.string   "type_of_land"
    t.string   "irrigation_method"
    t.float    "establishment",      limit: 24
    t.float    "annual_maintenance", limit: 24
    t.float    "age_of_garden",      limit: 24
    t.float    "lifespan_of_garden", limit: 24
    t.integer  "farmer_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id"
    t.float    "seed_qty",           limit: 24
    t.integer  "plants_count"
    t.integer  "seeding_count"
    t.integer  "seed_price_inter"
    t.integer  "crop_type_id"
  end

  create_table "purposes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "ractions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "role_ractions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "role_id"
    t.integer  "raction_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scaffolds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "smus", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "socio_demographic_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.string   "farmer_member_name"
    t.string   "farmer_relation_id"
    t.integer  "occupation_id"
    t.integer  "education_id"
    t.integer  "age"
    t.integer  "gender_id"
    t.integer  "local_organisation_member"
    t.integer  "local_organisation_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id"
  end

  create_table "sub_water_sheds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "district_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
  end

  create_table "subsidiary_costs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "dry_fodder",      limit: 24
    t.float    "green_fodder",    limit: 24
    t.float    "concentrates",    limit: 24
    t.float    "manure",          limit: 24
    t.float    "milk_price",      limit: 24
    t.float    "lacation_period", limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "subsidiary_enterprises", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "livestock_name_id"
    t.integer  "no_of_animals"
    t.integer  "no_of_milching_animals"
    t.float    "present_value_of_animals", limit: 24
    t.float    "monthly_feed_dry",         limit: 24
    t.float    "monthly_feed_green",       limit: 24
    t.float    "monthly_feed_conc",        limit: 24
    t.integer  "labour_no_of_hrs"
    t.float    "labour_amount",            limit: 24
    t.float    "healthcare_expenses",      limit: 24
    t.float    "milk",                     limit: 24
    t.float    "lacation_period",          limit: 24
    t.float    "manure",                   limit: 24
    t.float    "price_per_unit",           limit: 24
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "suggestion_paramaters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "survey_numbers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.string   "hissa_number"
    t.string   "grid1"
    t.string   "grid2"
    t.string   "grid3"
    t.string   "grid4"
    t.integer  "farmer_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "surveys", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "farmer_id"
    t.integer  "survey_number_id"
    t.string   "type_of_land"
    t.float    "holding_acres",      limit: 24
    t.float    "present_land_value", limit: 24
    t.string   "soil_type"
    t.string   "quality"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id"
  end

  create_table "taluks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "district_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
  end

  create_table "topographies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "user_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "provider",                             default: "email", null: false
    t.string   "uid",                                  default: "",      null: false
    t.string   "encrypted_password",                   default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  create_table "villages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "micro_water_shed_id"
    t.boolean  "is_active"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
  end

  create_table "watershed_awarenesses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "particular_id"
    t.string   "awareness"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end

  create_table "watershed_comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "comments",   limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "farmer_id"
    t.integer  "user_id"
  end


  create_view :categories,  sql_definition: <<-SQL
      select `SQ_FF`.`farmer_id` AS `farmer_id`,(case when (`SQ_FF`.`ha` <= 0) then 'LL' when (`SQ_FF`.`ha` between 0.1 and 2.47) then 'MF' when (`SQ_FF`.`ha` between 2.48 and 4.94) then 'SF' when (`SQ_FF`.`ha` between 4.95 and 9.88) then 'SMF' when (`SQ_FF`.`ha` between 9.89 and 24.7) then 'MDF' else 'LF' end) AS `name` from (`farmers` `ff` join `category_subs` `SQ_FF` on((`ff`.`id` = `SQ_FF`.`farmer_id`)))
  SQL

  create_view :category_subs,  sql_definition: <<-SQL
      select `s`.`type_of_land` AS `type_of_land`,`s`.`farmer_id` AS `farmer_id`,sum((case when ((`s`.`type_of_land` = 'Dry') or (`s`.`type_of_land` = 'Permanent Fallow')) then `s`.`holding_acres` when (`s`.`type_of_land` = 'Irrigated') then (`s`.`holding_acres` * 2) else 0 end)) AS `ha` from (`farmers` `f` left join `surveys` `s` on((`f`.`id` = `s`.`farmer_id`))) group by `f`.`id`
  SQL
end
