SELECT type_of_land
	 , farmer_id
	 , SUM(CASE WHEN type_of_land = 'Dry' || type_of_land = 'Permanent Fallow' THEN holding_acres  
		  WHEN type_of_land = 'Irrigated' THEN holding_acres * 2 
          ELSE 0 END) AS ha
	FROM farmers f
	LEFT OUTER JOIN surveys s
	  ON f.id = s.farmer_id
	GROUP BY f.id