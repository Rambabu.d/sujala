SELECT SQ_FF.farmer_id
  , CASE 
    WHEN SQ_FF.ha <= 0 then 'LL'
    WHEN SQ_FF.ha BETWEEN 0.1 AND 2.47 THEN 'MF'
    WHEN SQ_FF.ha BETWEEN 2.48 AND 4.94 THEN 'SF'
    WHEN SQ_FF.ha BETWEEN 4.95 AND 9.88 THEN 'SMF'
    WHEN SQ_FF.ha BETWEEN 9.89 AND 24.7 THEN 'MDF'
    ELSE 'LF' 
  END  AS name
FROM farmers ff
JOIN category_subs SQ_FF ON ff.id = SQ_FF.farmer_id; 