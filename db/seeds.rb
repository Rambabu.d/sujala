# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

education_list = [ "Illiterate",
  "Functional Literate",
  "Primary School",
  "Middle School",
  "High School",
  "PUC",
  "Diploma",
  "ITI",
  "Degree",
  "Masters",
  "Ph.D"]
education_list.each do |name|
  Education.where( name: name, is_active: true ).first_or_create
end

caste_list = [ "SC",
  "ST",
  "OBC",
  "General"]
caste_list.each do |name|
  Caste.where( name: name, is_active: true ).first_or_create
end

topography_list = [ "Ridge",
  "Mid",
  "Valley",
  "Not Available"]
topography_list.each do |name|
  Topography.where( name: name, is_active: true ).first_or_create
end

smu_list = [ "LMU1",
  "LMU2",
  "LMU3",
  "LMU4",
  "LMU5",
  "LMU6",
  "LMU7",
  "LMU8",
  "LMU9",
  "LMU10",
   "Not Available"]
smu_list.each do |name|
  Smu.where( name: name, is_active: true ).first_or_create
end

occupation_list = [ "Agriculture",
  "Agricultural Labour",
  "General Labour",
  "Household industry",
  "Artisans",
  "Government Service",
  "Private Service",
  "Trade & Business",
  "Retired",
  "Student",
  "Others"]
occupation_list.each do |name|
  Occupation.where( name: name, is_active: true ).first_or_create
end


Gender.where( name: "Male").first_or_create
Gender.where( name: "Female").first_or_create


local_organisation_list = ["Gram Panchayat",
  "Taluk Panchayat",
  "Zilla Panchayat",
  "Sthree Shakthi Sangha",
  "Self Help Group",
  "User Group",
  "CIGs",
  "NGOs",
  "Dairy Cooperative",
  "Raitha Sangha",
  "No"]
local_organisation_list.each do |name|
  LocalOrganisation.where( name: name, is_active: true ).first_or_create
end

house_type_list = ["Thatched",
  "Katcha",
  "Pucca/RCC"]
house_type_list.each do |name|
  HouseType.where( name: name, is_active: true ).first_or_create
end

asset_type_list = ["Radio",
  "Television",
  "DVD/VCD Player",
  "Mixer/Grinder",
  "Refrigerator",
  "Microwave Oven",
  "Bicycle",
  "Motor Cycle",
  "Auto",
  "Tempo",
  "Car/Four Wheeler",
  "Heavy Vehicles",
  "Landline Phone",
  "Mobile Phone",
  "Computer/Laptop"]
asset_type_list.each do |name|
  AssetType.where( name: name, is_active: true ).first_or_create
end

machine_type_list = ["Bullock Cart",
  "Plough",
  "Seed/Fertilizer Drill",
  "Transplanter/Grinder",
  "Refrigerator",
  "Irrigation Pump",
  "Power Tiller ",
  "Tractor",
  "Sprayer",
  "Sprinkler",
  "Weeder",
  "Harvester",
  "Thresher",
  "Groundnut Decorticator",
  "Maize Huller",
  "Chaff Cutter",
  "JCB/Hitachi"]
machine_type_list.each do |name|
  MachineType.where( name: name, is_active: true ).first_or_create
end

livestock_type_list = ["Bullock",
  "Local cow",
  "Crossbred cow",
  "Buffalo ",
  "Sheep ",
  "Goat ",
  "Pigs",
  "Poultry birds"]
livestock_type_list.each do |name|
  LivestockType.where( name: name, is_active: true ).first_or_create
end

crops_list = ["Anthorium","Ash gourd","Bangalore brinjal (Seeme badane)","Beet root ","Bengal gram ","Bird of paradise ","Bitter gourd ","Black gram ","Bottle gourd ","Brinjal ","Cabbage ","Capsicum ","Carrot ","Castor ","Cauliflower","Chick pea ","Chilly","China aster ","Chrysanthemum ","Cluster bean ","Coriander ","Cotton","Cow pea","Crossandra ","Cucumber ","Curry leaf ","Drumstick ","Field bean","Gaillardia","Garlic","Gerbera ","Gherkin ","Ginger","Gladiolus ","Golden rod ","Ground nut ","Halikonia ","Haraka (Kodo Millet )","Horse gram ","Horse gram ","Ivy gourd (Thonde)","Jasmine ","Jowar ","Knole cole ","Ladies finger ","Maize ","Marry gold ","Menthya ","Musk melon ","Navane (Fox Millet) ","Niger (Huchellu)","Onion ","Orchids ","Paddy ","Palak ","Pea","Pearl  millet (Sajje)","Peas","Pepper","Potato ","Pumpkin ","Radish ","Ragi ","Red gram (togari)","Ridge gourd ","Rose ","Saame (little millet)","Sesamum (yellu)","Soyabean ","Sugandaraja ","Sugarcane ","Sunflower ","Sweet potato ","Tobacco","Tomato ","Turmeric","Water melon ","Wheat ","yelachi","Amla ","Anjura ","Arecanut ","Beetle leaf ","Cashew","Cocoa","Coconut","Coffee","Custard apple ","Grapes ","Guava","Jack fruit ","Jamun ","Lemon ","Mandarin ","Mango ","Orange ","papaya","Pine apple ","Pomegranate ","Rubber ","Sapota"]
crops_list.each do |name|
  Crop.where(title: name).first_or_create
  Intercrop.where(title: name).first_or_create
end

income_source_list = ["Dry land",
"Irrigated",
"Sericulture",
"Dairy",
"Poultry",
"Sheep/goat",
"Piggery",
"Drought animal",
"Service/salary",
"Business",
"Wage"]
income_source_list.each do |name|
  IncomeSource.where(name: name).first_or_create
end

purpose_list = ["Land development",
"Irrigation facility",
"Improved crop production",
"Improved livestock management",
"Orchard development/ maintenance",
"Subsidiary enterprises"]

purpose_list.each do |name|
  Purpose.where(name: name).first_or_create
end

particulars_list = ["Soil and water erosion problems in the farm",
"Status of soil fertility of the farm ",
"Soil testing done earlier ",
"Interest in soil test ",
"Adoption of soil and water conservation practices "]
particulars_list.each do |name|
  Particular.where(name: name).first_or_create
end

conversation_structure_list = ["Field Bunding",
"Contour Bund",
"Graded Bund",
"Loose Border Checks",
"Vegetative Bund",
"Water Ways",
"Farm Pond",
"Bore Well Recharge Pit",
"Summer Ploughing",
"Dead Furrow",
"Mulching",
"Contour Cultivation",
"Combination of deep and shallow root crops"]

conversation_structure_list.each do |name|
  ConversationStructure.where(name: name).first_or_create
end

constraints_list = ["Lower fertility status of the soil",
"Wild animal menace on farm field",
"Frequent incidence of pest and diseases",
"Inadequacy of irrigation water",
"High cost of Fertilizers and plant protection chemicals",
"High rate of interest on credit",
"Low price for the agricultural commodities",
"Lack of marketing facilities in the area ",
"Inadequate extension services",
"Lack of transport for safe transport of the Agril produce to the market."]
 constraints_list.each do |name|
  Constraint.where(name: name).first_or_create
 end

 food_materials_list = ["Cereals ",
"Pulses",
"Oilseed",
"Vegetables",
"Fruits",
"Milk ",
"Egg",
"Meat"]
food_materials_list.each do |name|
  FoodMaterial.where(name: name).first_or_create
end

suggestion_parameters_list = ["Natural resource management",
"Social life",
"Livelihood Aspects",
"Crop Production Aspects"]
suggestion_parameters_list.each do |name|
  SuggestionParamater.where(name: name).first_or_create
end

horticulture_plants = ["Amla",
"Anjura",
"Arecanut",
"Beetle leaf",
"Cashew",
"Cocoa",
"Coconut",
"Coffee",
"Custard apple",
"Grapes",
"Guava",
"Jack fruit",
"Jamun",
"Lemon",
"Mandarin",
"Mango",
"Orange",
"papaya",
"Pine apple",
"Pomegranate",
"Rubber",
"Sapota"]

horticulture_plants.each do |name|
  HorticulturePlant.where(name: name).first_or_create
end

forestry_list = ["Eucalyptus",
"Cashew",
"Teak",
"Neem",
"Tamarind",
"Pongamia",
"Acacia",
"Banyan",
"Pupil tree"]

forestry_list.each do |name|
  ForestryPlant.where(name: name).first_or_create
end


credit_source_list = ["Institutional", "Private"]

credit_source_list.each do |name|
  CreditSource.where(name: name).first_or_create
end

institutional_credit_avail_purpose_list = ["Agriculture production",
"Animal husbandry",
"Bore well/irrigation related equipments",
"Income generating activities",
"Purchase-land",
"Purchase–agricultural implements/ farm machinery",
"Purchase-vehicle",
"Construction-house, Construction-cattle shed",
"Household consumption",
"Education",
"Healthcare",
"Social functions like marriage",
"Other"]

institutional_credit_avail_purpose_list.each do |name|
  CreditAvailPurpose.where(name: name, credit_source_id: CreditSource.where(name: "Institutional").first.id).first_or_create
end

private_credit_avail_purpose = ["Agriculture production",
"Animal husbandry",
"Bore well/irrigation related equipments",
"Income generating activities",
"Purchase-land",
"Purchase–agricultural implements/ farm machinery",
"Purchase-vehicle",
"Construction-house, Construction-cattle shed",
"Household consumption",
"Education",
"Healthcare",
"Social functions like marriage",
"Other"]

private_credit_avail_purpose.each do |name|
  CreditAvailPurpose.where(name: name, credit_source_id: CreditSource.where(name: "Private").first.id).first_or_create
end

livestock_names_list = ["Sheep",
"Goat",
"Cow Local",
"Cow crossbred",
"Buffalo",
"Bullock",
"Poultry"]

livestock_names_list.each do |name|
  LivestockName.where(name: name).first_or_create
end

crop_list = ["Arecanut",
"Ash Gourd",
"Baby Corn",
"Bajra",
"Banana",
"Beetroot",
"Bengal Gram (Kadale)",
"Bitter Gourd",
"Black Gram",
"Bottle Gourd",
"Brinjal (Round)",
"Brinjal (White)",
"Cabbage",
"Capsicum",
"Carrot",
"Cashew",
"Castor",
"Cauliflower",
"Chilly",
"Cocoa",
"Coconut",
"Coffee",
"Cotton",
"Cotton + Marigold ",
"Cow Pea (Alasande)",
"Cucumber",
"Custard Apple",
"Drumstick",
"Elachi",
"Field Bean (Avare)",
"Garlic",
"Gherkin",
"Ginger",
"Grapes",
"Green Gram",
"Ground Nut",
"Guava",
"Foxtail millet (Haraka)",
"Horse Gram (Hurali)",
"Jack fruit",
"Jamun",
"Knol Khol",
"Kusume",
"Ladies finger (Okra)",
"Lemon",
"Lime",
"Maize",
"Mango",
"Musk melon",
"Mustard",
"Navane",
"Onion",
"Orange",
"Paddy",
"Papaya",
"Pea",
"Pepper",
"Pineapple",
"Pomegranate",
"Pop Corn",
"Potato",
"Pumpkin",
"Radish",
"Ragi",
"Red Gram",
"Ridge Gpurd",
"Saame",
"Sapota",
"Seeme Badane",
"Sesamum",
"Sorghum",
"Soya Bean",
"Sugarcane",
"Sunflower",
"Thale",
"Thondekaayi",
"Tobacco",
"Tomato",
"Turmeric",
"Water Melon",
"Wheat"]

crop_list.each do |crop|
  CropList.where(name: crop).first_or_create
end

intercrop_list = ["None",
"Arecanut",
"Ash Gourd",
"Baby Corn",
"Bajra",
"Banana",
"Beetroot",
"Bengal Gram (Kadale)",
"Bitter Gourd",
"Black Gram",
"Bottle Gourd",
"Brinjal (Round)",
"Brinjal (White)",
"Cabbage",
"Capsicum",
"Carrot",
"Cashew",
"Castor",
"Cauliflower",
"Chilly",
"Cocoa",
"Coconut",
"Coffee",
"Cotton",
"Cotton + Marigold ",
"Cow Pea (Alasande)",
"Cucumber",
"Custard Apple",
"Drumstick",
"Elachi",
"Field Bean (Avare)",
"Garlic",
"Gherkin",
"Ginger",
"Grapes",
"Green Gram",
"Ground Nut",
"Guava",
"Foxtail millet (Haraka)",
"Horse Gram (Hurali)",
"Jack fruit",
"Jamun",
"Knol Khol",
"Kusume",
"Ladies finger (Okra)",
"Lemon",
"Lime",
"Maize",
"Mango",
"Musk melon",
"Mustard",
"Navane",
"Onion",
"Orange",
"Paddy",
"Papaya",
"Pea",
"Pepper",
"Pineapple",
"Pomegranate",
"Pop Corn",
"Potato",
"Pumpkin",
"Radish",
"Ragi",
"Red Gram",
"Ridge Gpurd",
"Saame",
"Sapota",
"Seeme Badane",
"Sesamum",
"Sorghum",
"Soya Bean",
"Sugarcane",
"Sunflower",
"Thale",
"Thondekaayi",
"Tobacco",
"Tomato",
"Turmeric",
"Water Melon",
"Wheat"]

intercrop_list.each do |inter_crop|
  IntercropList.where(name: inter_crop).first_or_create
end

fertilizers = ["Organic Manures (FYM/Compost/Vermicompost)",
"Bio-fertilizers",
"Urea",
"CAN",
"10:26:0",
"15:15:15",
"16:20:0",
"17:17:17",
"19:19:19",
"20:20:20",
"DAP",
"MOP",
"SOP",
"SSP",
"TSP",
"Ammonium Sulphate",
"Micro-nutrients",
"Soil Amendments",
"Others"]

fertilizers.each do |name|
  Fertilizer.where(name: name).first_or_create
end


@dav = District.where(name: "Tumkur").first_or_create

[{name:'Gubbi', district_id: @dav.id},
{name:'Madhugiri', district_id: @dav.id},
{name:'Pavgada', district_id: @dav.id},
{name:'Sira', district_id: @dav.id},
{name:'Tumkur', district_id: @dav.id},
{name:'Kunigal', district_id: @dav.id}].each do |t|
  Taluk.where(t).first_or_create
end


[{name:'Nitturu', taluk_id: Taluk.find_by_name('Gubbi').id},
{name:'Kandhikere', taluk_id: Taluk.find_by_name('Gubbi').id},
{name:'Hagalawadi', taluk_id: Taluk.find_by_name('Gubbi').id},
{name:'Cheluru', taluk_id: Taluk.find_by_name('Gubbi').id},
{name:'Kodigenahalli', taluk_id: Taluk.find_by_name('Madhugiri').id},
{name:'Itakadhibbadhahalli', taluk_id: Taluk.find_by_name('Madhugiri').id},
{name:'Y.N.Hosakote', taluk_id: Taluk.find_by_name('Pavgada').id},
{name:'Nagalamadike', taluk_id: Taluk.find_by_name('Pavgada').id},
{name:'Kasaba', taluk_id: Taluk.find_by_name('Sira').id},
{name:'Gowdagere', taluk_id: Taluk.find_by_name('Sira').id},
{name:'Bukkapatna', taluk_id: Taluk.find_by_name('Sira').id},
{name:'Hebburu', taluk_id: Taluk.find_by_name('Tumkur').id},
{name:'Kudhuru', taluk_id: Taluk.find_by_name('Tumkur').id},
{name:'Thippasandra', taluk_id: Taluk.find_by_name('Tumkur').id},
{name:'Kotthigere', taluk_id: Taluk.find_by_name('Kunigal').id}].each do |t|
  Hobli.where(t).first_or_create
end


[{name:'Handanahalli', hobli_id: Hobli.find_by_name('Nitturu').id},
{name:'Kondli', hobli_id: Hobli.find_by_name('Nitturu').id},
{name:'Sagasandra', hobli_id: Hobli.find_by_name('Nitturu').id},
{name:'Alilughatta', hobli_id: Hobli.find_by_name('Nitturu').id},
{name:'Theerthapura', hobli_id: Hobli.find_by_name('Kandhikere').id},
{name:'Barashidlahalli', hobli_id: Hobli.find_by_name('Kandhikere').id},
{name:'Kondli', hobli_id: Hobli.find_by_name('Kandhikere').id},
{name:'Hosakere', hobli_id: Hobli.find_by_name('Hagalawadi').id},
{name:'Shivapura', hobli_id: Hobli.find_by_name('Hagalawadi').id},
{name:'Alilughatta', hobli_id: Hobli.find_by_name('Hagalawadi').id},
{name:'Madenahalli', hobli_id: Hobli.find_by_name('Cheluru').id},
{name:'Kadagatthuru', hobli_id: Hobli.find_by_name('Kodigenahalli').id},
{name:'Parthihalli', hobli_id: Hobli.find_by_name('Kodigenahalli').id},
{name:'Teriyuru', hobli_id: Hobli.find_by_name('Kodigenahalli').id},
{name:'Suddekunte', hobli_id: Hobli.find_by_name('Kodigenahalli').id},
{name:'Muddenahalli', hobli_id: Hobli.find_by_name('Kodigenahalli').id},
{name:'Gundagal', hobli_id: Hobli.find_by_name('Kodigenahalli').id},
{name:'Chikkadalavata', hobli_id: Hobli.find_by_name('Itakadhibbadhahalli').id},
{name:'Janakaloti', hobli_id: Hobli.find_by_name('Itakadhibbadhahalli').id},
{name:'Jalodu', hobli_id: Hobli.find_by_name('Y.N.Hosakote').id},
{name:'Dalavayihalli', hobli_id: Hobli.find_by_name('Y.N.Hosakote').id},
{name:'Bheemanakunte', hobli_id: Hobli.find_by_name('Y.N.Hosakote').id},
{name:'Chikkahalli', hobli_id: Hobli.find_by_name('Y.N.Hosakote').id},
{name:'Kamanadurga', hobli_id: Hobli.find_by_name('Nagalamadike').id},
{name:'Bettada Kelaginahalli', hobli_id: Hobli.find_by_name('Nagalamadike').id},
{name:'Mosarukunte', hobli_id: Hobli.find_by_name('Kasaba').id},
{name:'Dwaralu', hobli_id: Hobli.find_by_name('Kasaba').id},
{name:'Mosarukunte', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Tavarekere', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Hunasehalli', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Kumbarahalli', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Hosuru', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Gowdagere', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Bandakunte', hobli_id: Hobli.find_by_name('Gowdagere').id},
{name:'Hunasehalli', hobli_id: Hobli.find_by_name('Bukkapatna').id},
{name:'Niduvalalu', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Bidanagere', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Thavarekere', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Hebburu', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Bisalahalli', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Maskal', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Kambalalu', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Kanakuppe', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Rayapura', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Nagavalli', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Ballagere', hobli_id: Hobli.find_by_name('Hebburu').id},
{name:'Thippasandra', hobli_id: Hobli.find_by_name('Kudhuru').id},
{name:'Niduvalalu', hobli_id: Hobli.find_by_name('Thippasandra').id},
{name:'Thippasandra', hobli_id: Hobli.find_by_name('Thippasandra').id},
{name:'Taredakuppa', hobli_id: Hobli.find_by_name('Kotthigere').id},
{name:'Shettigere', hobli_id: Hobli.find_by_name('Kotthigere').id},
{name:'Doddamalalavadi', hobli_id: Hobli.find_by_name('Kotthigere').id}].each do |t|
  Grampanchayat.where(t).first_or_create
end



[{name:'Obalapura', code: '4D3D8S', district_id: @dav.id},
{name:'Appanahalli', code: '4D3D8T', district_id: @dav.id},
{name:'Shivarampura', code: '4D3D6Q', district_id: @dav.id},
{name:'Kashinayakanahalli', code: '4C3H5I', district_id: @dav.id},
{name:'Chikkadalavatta', code: '4C3H4W', district_id: @dav.id},
{name:'Doddalavatta', code: '4C3H4V', district_id: @dav.id},
{name:'Chikkajalodu', code: '4C3H2D', district_id: @dav.id},
{name:'Gowdatimmanahalli', code: '4C3H2C', district_id: @dav.id},
{name:'Nagalapura', code: '4C3H2B', district_id: @dav.id},
{name:'Bettadakelaginahalli', code: '4C3H3W', district_id: @dav.id},
{name:'Melekote', code: '4D3D6W', district_id: @dav.id},
{name:'Kaluvarahalli', code: '4D3D6X', district_id: @dav.id},
{name:'Gajjiganahalli', code: '4D3D7Q', district_id: @dav.id},
{name:'Hunsehalli', code: '4D3D6J', district_id: @dav.id},
{name:'Hosalaya', code: '4B3C5N', district_id: @dav.id},
{name:'Kalkere', code: '4B3D2D', district_id: @dav.id},
{name:'Madikehalli', code: '4B3D5Q', district_id: @dav.id},
{name:'Gaddehalli', code: '4B3D2F', district_id: @dav.id},
{name:'Hebbur', code: '4B3D2E', district_id: @dav.id}].each do |t|
  SubWaterShed.where(t).first_or_create
end


[{name:'Bandanahalli-3', code: '4D3D8S1c', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Muddanahalli-3', code: '4D3D8S1d', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Muddanahalli-1', code: '4D3D8S1e', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Muddanahalli-2', code: '4D3D8S1f', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Obalapura-1', code: '4D3D8S2a', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Obalapura-2', code: '4D3D8S2b', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Obalapura-3', code: '4D3D8S2c', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Obalapura-4', code: '4D3D8S2d', sub_water_shed_id: SubWaterShed.find_by_name('Obalapura').id},
{name:'Singadahalli', code: '4D3D8T2a', sub_water_shed_id: SubWaterShed.find_by_name('Appanahalli').id},
{name:'Galigerkere-1', code: '4D3D8T2b', sub_water_shed_id: SubWaterShed.find_by_name('Appanahalli').id},
{name:'Galigerkere-2', code: '4D3D8T2c', sub_water_shed_id: SubWaterShed.find_by_name('Appanahalli').id},
{name:'Galigerkere-3', code: '4D3D8T2d', sub_water_shed_id: SubWaterShed.find_by_name('Appanahalli').id},
{name:'Haradagere-2', code: '4D3D8T1a', sub_water_shed_id: SubWaterShed.find_by_name('Appanahalli').id},
{name:'Appanahalli-1', code: '4D3D8T1d', sub_water_shed_id: SubWaterShed.find_by_name('Appanahalli').id},
{name:'Yakkalakatte', code: '4D3D6Q1a', sub_water_shed_id: SubWaterShed.find_by_name('Shivarampura').id},
{name:'Kadagatturu', code: '4C3H5I1d', sub_water_shed_id: SubWaterShed.find_by_name('Kashinayakanahalli').id},
{name:'Partihalli', code: '4C3H5I1e', sub_water_shed_id: SubWaterShed.find_by_name('Kashinayakanahalli').id},
{name:'Teriyur', code: '4C3H5I2a', sub_water_shed_id: SubWaterShed.find_by_name('Kashinayakanahalli').id},
{name:'Venganamanahalli', code: '4C3H5I2b', sub_water_shed_id: SubWaterShed.find_by_name('Kashinayakanahalli').id},
{name:'Kashinayakanahalli', code: '4C3H5I2c', sub_water_shed_id: SubWaterShed.find_by_name('Kashinayakanahalli').id},
{name:'Annenahalli', code: '4C3H4W1c', sub_water_shed_id: SubWaterShed.find_by_name('Chikkadalavatta').id},
{name:'Muddanahalli', code: '4C3H4W1d', sub_water_shed_id: SubWaterShed.find_by_name('Chikkadalavatta').id},
{name:'Singanahalli', code: '4C3H4W2e', sub_water_shed_id: SubWaterShed.find_by_name('Chikkadalavatta').id},
{name:'Polenahalli', code: '4C3H4W2f', sub_water_shed_id: SubWaterShed.find_by_name('Chikkadalavatta').id},
{name:'Chikkadalavatta', code: '4C3H4W2g', sub_water_shed_id: SubWaterShed.find_by_name('Chikkadalavatta').id},
{name:'Suddenakunte-1', code: '4C3H4V3e', sub_water_shed_id: SubWaterShed.find_by_name('Doddalavatta').id},
{name:'Suddenakunte-2', code: '4C3H4V3f', sub_water_shed_id: SubWaterShed.find_by_name('Doddalavatta').id},
{name:'Doodajalodu-1', code: '4C3H2D2a', sub_water_shed_id: SubWaterShed.find_by_name('Chikkajalodu').id},
{name:'Chikkajalodu and new', code: '4C3H2D2b', sub_water_shed_id: SubWaterShed.find_by_name('Chikkajalodu').id},
{name:'Doodajalodu-2', code: '4C3H2D2c', sub_water_shed_id: SubWaterShed.find_by_name('Chikkajalodu').id},
{name:'Gowdatimmanahalli-1', code: '4C3H2C2a', sub_water_shed_id: SubWaterShed.find_by_name('Gowdatimmanahalli').id},
{name:'Gowdatimmanahalli-2', code: '4C3H2C2b', sub_water_shed_id: SubWaterShed.find_by_name('Gowdatimmanahalli').id},
{name:'Bhimmanakunte-2', code: '4C3H2B2b', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Bhimmanakunte-1', code: '4C3H2B2d', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Nagalapura-3', code: '4C3H2B3b', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Nagalapura-2', code: '4C3H2B3c', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Nagalapura-1', code: '4C3H2B3d', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Nagalapura-5', code: '4C3H2B3e', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Ayyampalli RF 1', code: '4C3H2A1a', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Ayyampalli RF 2', code: '4C3H2A1b', sub_water_shed_id: SubWaterShed.find_by_name('Nagalapura').id},
{name:'Bettadakelaginahalli-3', code: '4C3H3W1a', sub_water_shed_id: SubWaterShed.find_by_name('Bettadakelaginahalli').id},
{name:'Bettadakelaginahalli-4', code: '4C3H3W1d', sub_water_shed_id: SubWaterShed.find_by_name('Bettadakelaginahalli').id},
{name:'Bettadakelaginahalli-6', code: '4C3H3W1f', sub_water_shed_id: SubWaterShed.find_by_name('Bettadakelaginahalli').id},
{name:'Gollarahatti-1', code: '4D3D6W1d', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Ujjankunte-2', code: '4D3D6W1e', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Ujjankunte-1', code: '4D3D6W1f', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Mosarakunte', code: '4D3D6W2b', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Puralehalli', code: '4D3D6W2c', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Melekote-1', code: '4D3D6W2d', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Melekote-2', code: '4D3D6W2e', sub_water_shed_id: SubWaterShed.find_by_name('Melekote').id},
{name:'Kumdarahalli-2', code: '4D3D6X2a', sub_water_shed_id: SubWaterShed.find_by_name('Kaluvarahalli').id},
{name:'Kumdarahalli-1', code: '4D3D6X2b', sub_water_shed_id: SubWaterShed.find_by_name('Kaluvarahalli').id},
{name:'Kaluvarahalli-3', code: '4D3D6X2c', sub_water_shed_id: SubWaterShed.find_by_name('Kaluvarahalli').id},
{name:'Kaggalodu', code: '4D3D7Q2f', sub_water_shed_id: SubWaterShed.find_by_name('Gajjiganahalli').id},
{name:'Marudi Hatti-1', code: '4D3D6J1c', sub_water_shed_id: SubWaterShed.find_by_name('Hunsehalli').id},
{name:'Halayapura-1', code: '4B3C5N1a', sub_water_shed_id: SubWaterShed.find_by_name('Hosalaya').id},
{name:'Halayapura-2', code: '4B3C5N1b', sub_water_shed_id: SubWaterShed.find_by_name('Hosalaya').id},
{name:'Bidanagere', code: '4B3D2D1a', sub_water_shed_id: SubWaterShed.find_by_name('Kalkere').id},
{name:'Nidvalalu', code: '4B3D2D1b', sub_water_shed_id: SubWaterShed.find_by_name('Kalkere').id},
{name:'Giddadapalya', code: '4B3D2D1c', sub_water_shed_id: SubWaterShed.find_by_name('Kalkere').id},
{name:'Kalkere', code: '4B3D2D2c', sub_water_shed_id: SubWaterShed.find_by_name('Kalkere').id},
{name:'Timmnayakanapalya', code: '4B3D2D2d', sub_water_shed_id: SubWaterShed.find_by_name('Kalkere').id},
{name:'Bannikuppe', code: '4B3D5Q1a', sub_water_shed_id: SubWaterShed.find_by_name('Madikehalli').id},
{name:'Lingapura', code: '4B3D2F2c', sub_water_shed_id: SubWaterShed.find_by_name('Gaddehalli').id},
{name:'Doddagollahalli', code: '4B3D2F3a', sub_water_shed_id: SubWaterShed.find_by_name('Gaddehalli').id},
{name:'Rajapura', code: '4B3D2E2e', sub_water_shed_id: SubWaterShed.find_by_name('Hebbur').id},
{name:'Thimmasandra', code: '4B3D2E2d', sub_water_shed_id: SubWaterShed.find_by_name('Hebbur').id},
{name:'Kalyanapura', code: '4B3D2E2c', sub_water_shed_id: SubWaterShed.find_by_name('Hebbur').id}].each do |t|
  MicroWaterShed.where(t).first_or_create
end


[{name: 'Bommarasanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bandanahalli-3').id},
{name: 'Handanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bandanahalli-3').id},
{name: 'Shivasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Bandanahalli-3').id},
{name: 'Virupakshapura', micro_water_shed_id: MicroWaterShed.find_by_name('Bandanahalli-3').id},

{name: 'Bogasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-3').id},
{name: 'Bommarasanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-3').id},
{name: 'Karadikallu', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-3').id},
{name: 'Mudlapalya', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-3').id},
{name: 'Shivasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-3').id},

{name: 'Bandanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-1').id},
{name: 'Bogasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-1').id},
{name: 'Haradhagere', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-1').id},
{name: 'Karadikallu', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-1').id},
{name: 'Muddanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-1').id},
{name: 'Yarekavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-1').id},

{name: 'Bogasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-2').id},
{name: 'Bommarasanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-2').id},
{name: 'Echalu Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-2').id},
{name: 'Haradhagere', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-2').id},
{name: 'Karadikallu', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli-2').id},

{name: 'Chikkarampura', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-1').id},
{name: 'Kondli', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-1').id},
{name: 'Mavinahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-1').id},
{name: 'Obalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-1').id},

{name: 'Karadikallu', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-2').id},
{name: 'Mudlapalya', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-2').id},
{name: 'Shivasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-2').id},

{name: 'Karadikallu', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-3').id},
{name: 'Kondli', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-3').id},
{name: 'Mavinahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-3').id},
{name: 'Obalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-3').id},

{name: 'Barashidlahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-4').id},
{name: 'Chikkarampura', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-4').id},
{name: 'Echalu Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-4').id},
{name: 'Karadikallu', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-4').id},
{name: 'Kottigal', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-4').id},
{name: 'Obalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Obalapura-4').id},

{name: 'Barashidlahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},
{name: 'Galigekere', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},
{name: 'Huleharuvi Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},
{name: 'Kallanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},
{name: 'Shingadhahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},
{name: 'Shivapura', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},
{name: 'Thimmalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Singadahalli').id},

{name: 'Akkanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-1').id},
{name: 'Galigekere', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-1').id},
{name: 'Shingadhahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-1').id},
{name: 'Thimmalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-1').id},

{name: 'G Gedlahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-2').id},
{name: 'Galigekere', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-2').id},
{name: 'Huleharuvu kavalu ', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-2').id},
{name: 'Oblapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-2').id},
{name: 'Shivapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-2').id},
{name: 'Yakkalakatte', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-2').id},

{name: 'Akkanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'G.Rangapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'G.Gedhlahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'Hagalawadi', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'Hosahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'Madhlapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'Obalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},
{name: 'Shivapura', micro_water_shed_id: MicroWaterShed.find_by_name('Galigerkere-3').id},

{name: 'Kallanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},
{name: 'Marehalladha Amruthamahal Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},
{name: 'Pura', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},
{name: 'Shettyhalli', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},
{name: 'Unaganala', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},
{name: 'Yakkalakatte', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},
{name: 'Yarekavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Haradagere-2').id},

{name: 'Appannanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},
{name: 'Huleharuvi Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},
{name: 'Kallanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},
{name: 'Marehalladha Amruthamahal Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},
{name: 'Pura', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},
{name: 'Shivapura', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},
{name: 'Yakkalakatte', micro_water_shed_id: MicroWaterShed.find_by_name('Appanahalli-1').id},

{name: 'Kempanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Yakkalakatte').id},
{name: 'Marehalladha Amruthamahal Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Yakkalakatte').id},
{name: 'Unaganala', micro_water_shed_id: MicroWaterShed.find_by_name('Yakkalakatte').id},
{name: 'Yakkalakatte', micro_water_shed_id: MicroWaterShed.find_by_name('Yakkalakatte').id},

{name: 'Kadagatthuru', micro_water_shed_id: MicroWaterShed.find_by_name('Kadagatturu').id},
{name: 'Nagenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kadagatturu').id},
{name: 'Parthihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kadagatturu').id},
{name: 'Thirumaladevarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kadagatturu').id},

{name: 'Chennavara', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'Gundagal', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'J.I.Vengalammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'K.G.Yaragunte', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'Kadagatthuru', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'Kasinayakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'Nagenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'Parthihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},
{name: 'Thirumaladevarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Partihalli').id},


{name: 'Chennavara', micro_water_shed_id: MicroWaterShed.find_by_name('Teriyur').id},
{name: 'Gundagal', micro_water_shed_id: MicroWaterShed.find_by_name('Teriyur').id},
{name: 'K.G.Yaragunte', micro_water_shed_id: MicroWaterShed.find_by_name('Teriyur').id},
{name: 'Kadagatthuru', micro_water_shed_id: MicroWaterShed.find_by_name('Teriyur').id},
{name: 'Theriyuru', micro_water_shed_id: MicroWaterShed.find_by_name('Teriyur').id},


{name: 'Chennavara', micro_water_shed_id: MicroWaterShed.find_by_name('Venganamanahalli').id},
{name: 'J.I.Vengalammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Venganamanahalli').id},
{name: 'Kasinayakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Venganamanahalli').id},
{name: 'Parthihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Venganamanahalli').id},
{name: 'Theriyuru', micro_water_shed_id: MicroWaterShed.find_by_name('Venganamanahalli').id},

{name: 'Kasinayakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kashinayakanahalli').id},

{name: 'Annenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Annenahalli').id},
{name: 'J.I.Vengalammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Annenahalli').id},
{name: 'Muddenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Annenahalli').id},
{name: 'Theriyuru', micro_water_shed_id: MicroWaterShed.find_by_name('Annenahalli').id},

{name: 'Annenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli').id},
{name: 'Chikkadalavata', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli').id},
{name: 'J.I.Vengalammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli').id},
{name: 'Muddenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli').id},
{name: 'Shyanaganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Muddanahalli').id},

{name: 'Masarapadi', micro_water_shed_id: MicroWaterShed.find_by_name('Singanahalli').id},
{name: 'Matyalammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Singanahalli').id},
{name: 'Singanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Singanahalli').id},
{name: 'Theriyuru', micro_water_shed_id: MicroWaterShed.find_by_name('Singanahalli').id},

{name: 'Annenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},
{name: 'Chikkadalavata', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},
{name: 'Masarapadi', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},
{name: 'Muddenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},
{name: 'Polenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},
{name: 'Shyanaganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},
{name: 'Theriyuru', micro_water_shed_id: MicroWaterShed.find_by_name('Polenahalli').id},

{name: 'Chikkadalavata', micro_water_shed_id: MicroWaterShed.find_by_name('Chikkadalavatta').id},
{name: 'Polenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Chikkadalavatta').id},
{name: 'Shyanaganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Chikkadalavatta').id},
{name: 'Vitalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Chikkadalavatta').id},

{name: 'K.G.Varadanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-1').id},
{name: 'Masarapadi', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-1').id},
{name: 'Polenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-1').id},
{name: 'Singanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-1').id},
{name: 'Suddekunte', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-1').id},

{name: 'Chikkadalavata', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'Hale Itakalote', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'Hosa Itakalote', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'K.G.Varadanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'Obalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'Polenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'Shyanaganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},
{name: 'Suddekunte', micro_water_shed_id: MicroWaterShed.find_by_name('Suddenakunte-2').id},

{name: 'Jalodu', micro_water_shed_id: MicroWaterShed.find_by_name('Doodajalodu-1').id},

{name: 'Jalodu', micro_water_shed_id: MicroWaterShed.find_by_name('Chikkajalodu and new').id},

{name: 'Gowdathimmanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Doodajalodu-2').id},
{name: 'Jalodu', micro_water_shed_id: MicroWaterShed.find_by_name('Doodajalodu-2').id},

{name: 'Dalavayihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Gowdatimmanahalli-1').id},
{name: 'Gowdathimmanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Gowdatimmanahalli-1').id},
{name: 'Jalodu', micro_water_shed_id: MicroWaterShed.find_by_name('Gowdatimmanahalli-1').id},

{name: 'Bheemanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Gowdatimmanahalli-2').id},
{name: 'Dalavayihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Gowdatimmanahalli-2').id},

{name: 'Bheemanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-2').id},
{name: 'Dalavayihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-2').id},
{name: 'Gowdathimmanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-2').id},
{name: 'Thimmammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-2').id},

{name: 'Bheemanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-1').id},
{name: 'Pothaganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-1').id},
{name: 'Thimmammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bhimmanakunte-1').id},

{name: 'Bheemanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-3').id},
{name: 'Nagalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-3').id},
{name: 'Thimmammanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-3').id},

{name: 'Chikkahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-2').id},
{name: 'Kamanadurga', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-2').id},
{name: 'Nagalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-2').id},

{name: 'Chikkahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-1').id},
{name: 'Nagalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-1').id},

{name: 'Nagalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Nagalapura-5').id},

{name: 'Chikkahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Ayyampalli RF 1').id},
{name: 'Nagalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Ayyampalli RF 1').id},

{name: 'Nagalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Ayyampalli RF 2').id},

{name: 'Bettada Kelaginahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bettadakelaginahalli-3').id},
{name: 'Chikkahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bettadakelaginahalli-3').id},
{name: 'Kamanadurga', micro_water_shed_id: MicroWaterShed.find_by_name('Bettadakelaginahalli-3').id},

{name: 'Bettadakelaginahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bettadakelaginahalli-4').id},

{name: 'Bettada Kelaginahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bettadakelaginahalli-6').id},
{name: 'Dalavayihalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bettadakelaginahalli-6').id},

{name: 'Cheelanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Gollarahatti-1').id},
{name: 'Maranagere', micro_water_shed_id: MicroWaterShed.find_by_name('Gollarahatti-1').id},
{name: 'Mosarukunte', micro_water_shed_id: MicroWaterShed.find_by_name('Gollarahatti-1').id},
{name: 'Tavarekere', micro_water_shed_id: MicroWaterShed.find_by_name('Gollarahatti-1').id},

{name: 'Maranagere', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-2').id},
{name: 'Melekote', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-2').id},
{name: 'Mosarukunte', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-2').id},
{name: 'Vujjanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-2').id},

{name: 'Melekote', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-1').id},
{name: 'Narayanapura', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-1').id},
{name: 'Pennanahole', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-1').id},
{name: 'Vujjanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Ujjankunte-1').id},

{name: 'Gajarehalli', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},
{name: 'Holakallu', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},
{name: 'J.I.Maddakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},
{name: 'Maranagere', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},
{name: 'Melekote', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},
{name: 'Mosarukunte', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},
{name: 'Tavarekere', micro_water_shed_id: MicroWaterShed.find_by_name('Mosarakunte').id},

{name: 'Balenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Puralehalli').id},
{name: 'Holakallu', micro_water_shed_id: MicroWaterShed.find_by_name('Puralehalli').id},
{name: 'J.I.Maddakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Puralehalli').id},
{name: 'Kumbarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Puralehalli').id},
{name: 'Mosarukunte', micro_water_shed_id: MicroWaterShed.find_by_name('Puralehalli').id},
{name: 'Ranganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Puralehalli').id},

{name: 'Melekote', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-1').id},
{name: 'Mosarukunte', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-1').id},
{name: 'Ranganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-1').id},

{name: 'Katanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-2').id},
{name: 'Melekote', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-2').id},
{name: 'Narayanapura', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-2').id},
{name: 'Ranganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Melekote-2').id},

{name: 'Balenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-2').id},
{name: 'Holakallu', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-2').id},
{name: 'J.I.Maddakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-2').id},
{name: 'Kumbarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-2').id},
{name: 'Mosarukunte', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-2').id},
{name: 'Ranganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-2').id},

{name: 'Balenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-1').id},
{name: 'Gomaranahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-1').id},
{name: 'Hosahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-1').id},
{name: 'Kumbarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-1').id},
{name: 'Ranganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kumdarahalli-1').id},

{name: 'Gomaranahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaluvarahalli-3').id},
{name: 'Kaluvarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaluvarahalli-3').id},
{name: 'Katanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaluvarahalli-3').id},
{name: 'Kumbarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaluvarahalli-3').id},
{name: 'Narayanapura', micro_water_shed_id: MicroWaterShed.find_by_name('Kaluvarahalli-3').id},
{name: 'Ranganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaluvarahalli-3').id},

{name: 'Badakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaggalodu').id},
{name: 'Gajarehalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaggalodu').id},
{name: 'J.I.Maddakanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Kaggalodu').id},
{name: 'Kaggaladu', micro_water_shed_id: MicroWaterShed.find_by_name('Kaggalodu').id},
{name: 'Kasaba Gowdagere', micro_water_shed_id: MicroWaterShed.find_by_name('Kaggalodu').id},

{name: 'Ganadhahunase', micro_water_shed_id: MicroWaterShed.find_by_name('Marudi Hatti-1').id},
{name: 'Ramanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Marudi Hatti-1').id},

{name: 'Niduvalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-1').id},
{name: 'S.I. Huliyapura', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-1').id},
{name: 'Sugganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-1').id},
{name: 'Thavarekere', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-1').id},
{name: 'Thondagere', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-1').id},
{name: 'Thondagere Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-1').id},

{name: 'Chikkanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-2').id},
{name: 'Niduvalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-2').id},
{name: 'Thondagere', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-2').id},
{name: 'Thondagere Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Halayapura-2').id},

{name: 'Bidanagere', micro_water_shed_id: MicroWaterShed.find_by_name('Bidanagere').id},
{name: 'Honnenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bidanagere').id},
{name: 'Niduvalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Bidanagere').id},
{name: 'Thavarekere', micro_water_shed_id: MicroWaterShed.find_by_name('Bidanagere').id},
{name: 'Thondagere', micro_water_shed_id: MicroWaterShed.find_by_name('Bidanagere').id},
{name: 'Thondagere Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Bidanagere').id},

{name: 'Honnenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Nidvalalu').id},
{name: 'Narayanakere', micro_water_shed_id: MicroWaterShed.find_by_name('Nidvalalu').id},
{name: 'Niduvalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Nidvalalu').id},
{name: 'Sulekuppe Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Nidvalalu').id},
{name: 'Thimmanapalya', micro_water_shed_id: MicroWaterShed.find_by_name('Nidvalalu').id},

{name: 'Doddaguni', micro_water_shed_id: MicroWaterShed.find_by_name('Giddadapalya').id},
{name: 'Narayanakere', micro_water_shed_id: MicroWaterShed.find_by_name('Giddadapalya').id},
{name: 'Niduvalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Giddadapalya').id},
{name: 'Sulekuppe Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Giddadapalya').id},
{name: 'Thimmanapalya', micro_water_shed_id: MicroWaterShed.find_by_name('Giddadapalya').id},

{name: 'Chikkamalalawadi', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'Doddaguni', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'J.I. Chikkamalilawadi Chaturbagha', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'Kasaba Hebburu', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'S.I. Kallakere', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'Sangalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'Sulekuppe Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},
{name: 'Tharedhakuppe', micro_water_shed_id: MicroWaterShed.find_by_name('Kalkere').id},

{name: 'Ballagere Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Timmnayakanapalya').id},
{name: 'Doddaguni', micro_water_shed_id: MicroWaterShed.find_by_name('Timmnayakanapalya').id},
{name: 'Hobburu Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Timmnayakanapalya').id},
{name: 'Kalyanapura', micro_water_shed_id: MicroWaterShed.find_by_name('Timmnayakanapalya').id},
{name: 'Karnakuppe', micro_water_shed_id: MicroWaterShed.find_by_name('Timmnayakanapalya').id},
{name: 'Kasaba Hebburu', micro_water_shed_id: MicroWaterShed.find_by_name('Timmnayakanapalya').id},

{name: 'Bannikuppe', micro_water_shed_id: MicroWaterShed.find_by_name('Bannikuppe').id},
{name: 'Chikkamalalawadi', micro_water_shed_id: MicroWaterShed.find_by_name('Bannikuppe').id},
{name: 'Doddamalalavadi', micro_water_shed_id: MicroWaterShed.find_by_name('Bannikuppe').id},
{name: 'J.I. Halaganahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Bannikuppe').id},

{name: 'Bommanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Dhulenahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Kambalalu Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Kanakuppe', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Kanakuppe Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Kodigehalli', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Lingapura', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},
{name: 'Ragi Muddanahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Lingapura').id},

{name: 'Bisalahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},
{name: 'Doddagollahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},
{name: 'Kanakuppe', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},
{name: 'Kuduvanakunte', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},
{name: 'Lingapura', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},
{name: 'Ramakrishnapura (Bisalehalli Kaval)', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},
{name: 'Shankardevarahalli', micro_water_shed_id: MicroWaterShed.find_by_name('Doddagollahalli').id},

{name: 'Hobburu Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Kambalalu Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Kanakuppe', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Kanakuppe Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Kembalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Kembalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Ramakrishnapura (Bisalehalli Kaval)', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},
{name: 'Thimmasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Rajapura').id},

{name: 'Hobburu Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},
{name: 'Kanakuppe Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},
{name: 'Kasaba Hebburu', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},
{name: 'Kembalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},
{name: 'Kembalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},
{name: 'Rayavara', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},
{name: 'Thimmasandra', micro_water_shed_id: MicroWaterShed.find_by_name('Thimmasandra').id},

{name: 'Ballagere Kavalu', micro_water_shed_id: MicroWaterShed.find_by_name('Kalyanapura').id},
{name: 'Cholapura', micro_water_shed_id: MicroWaterShed.find_by_name('Kalyanapura').id},
{name: 'Hobburu Amanikere', micro_water_shed_id: MicroWaterShed.find_by_name('Kalyanapura').id},
{name: 'Kalyanapura', micro_water_shed_id: MicroWaterShed.find_by_name('Kalyanapura').id},
{name: 'Kembalalu', micro_water_shed_id: MicroWaterShed.find_by_name('Kalyanapura').id},
{name: 'Kembalapura', micro_water_shed_id: MicroWaterShed.find_by_name('Kalyanapura').id}].each do |t|
  Village.where(t).first_or_create
end


migration_purpose_list = ["Job/wage/work","Business","Education of the children",
"Preference towards urban life","For agriculture in other location","Other"]

migration_purpose_list.each do |name|
  MigrationPurpose.where(name: name).first_or_create
end

migration_positive_conseq = ["Construction of house","Purchase of land",
"Purchase of household asset","Improved quality of life","Better children education","None","Other"]

migration_positive_conseq.each do |name|
  MigrationPositiveConsequence.where(name: name).first_or_create
end

migration_negative_conseq = ["Workload for other members of the family increased",
"Parent/aged neglected","Health hazards","Children education suffered","Disturbance in family life","None","Other"]

migration_negative_conseq.each do |name|
  MigrationNegativeConsequence.where(name: name).first_or_create
end

@cha = District.where(name: "Chamarajanagar").first_or_create

[{name:'Chamarajanagar', district_id: @cha.id},
{name:'Gundlapet', district_id: @cha.id}
].each do |t|
  Taluk.where(t).first_or_create
end


[{name:'Harve', taluk_id: Taluk.find_by_name('Chamarajanagar').id},
{name:'Terakanambi', taluk_id: Taluk.find_by_name('Chamarajanagar').id},
{name:'Terakanambi', taluk_id: Taluk.find_by_name('Gundlapet').id}].each do |t|
  Hobli.where(t).first_or_create
end

[{name:'Harave', hobli_id: Hobli.find_by_name('Harve').id},
{name:'Kottalavadi', hobli_id: Hobli.find_by_name('Harve').id},
{name:'Maleyuru', hobli_id: Hobli.find_by_name('Harve').id},
{name:'Mukadahalli', hobli_id: Hobli.find_by_name('Harve').id},
{name:'Nanjedevanapura ', hobli_id: Hobli.find_by_name('Harve').id},
{name:'Sagade', hobli_id: Hobli.find_by_name('Harve').id},
{name:'Terakanambi', hobli_id: Hobli.find_by_name('Terakanambi').id},
{name:'Sagade', hobli_id: Hobli.find_by_name('Terakanambi').id},
].each do |t|
  Grampanchayat.where(t).first_or_create
end


SubWaterShed.where({name:'Harve',code:'4B3E1G', district_id: @cha.id}).first_or_create


[{name:'Bettadapura',code:'4B3E1G1e', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Harve',code:'4B3E1G2c', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Harve-1',code:'4B3E1G2d', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Harve-2',code:'4B3E1G2c', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Hosahalli',code:'4B3E1G2b', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Kengaki',code:'4B3E1G1d', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Ketanapura',code:'4B3E1G2e', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Kethahalli',code:'4B3E1G2a', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Kumachahalli-I',code:'4B3E1G1b', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Kumachahalli-II',code:'4B3E1G1a', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Kuttegowdana hundi',code:'4B3E1G1c', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id},
{name:'Sagade',code:'4B3E1G1f', sub_water_shed_id: SubWaterShed.where(name: 'Harve', code:'4B3E1G').first.id}].each do |t|
  MicroWaterShed.where(t).first_or_create
end
 


[{name: 'Devalapura ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Hooradahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Kanchanahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Kumachahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Lakkuru ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Shyanadrahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Thammadahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code:'4B3E1G1a').first.id},
{name: 'Kanchanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-I', code:'4B3E1G1b').first.id},
{name: 'Kumachahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-I', code:'4B3E1G1b').first.id},
{name: 'Lakkuru ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-I', code:'4B3E1G1b').first.id},
{name: 'Thammadahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-I', code:'4B3E1G1b').first.id},
{name: 'Hooradahhali ', micro_water_shed_id: MicroWaterShed.where(name: 'Kuttegowdana hundi', code:'4B3E1G1c').first.id},
{name: 'Kanchanahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kuttegowdana hundi', code:'4B3E1G1c').first.id},
{name: 'Kengaki ', micro_water_shed_id: MicroWaterShed.where(name: 'Kuttegowdana hundi', code:'4B3E1G1c').first.id},
{name: 'Kumachahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kuttegowdana hundi', code:'4B3E1G1c').first.id},
{name: 'Kanchanahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kengaki', code: '4B3E1G1d').first.id},
{name: 'Kengaki ', micro_water_shed_id: MicroWaterShed.where(name: 'Kengaki', code: '4B3E1G1d').first.id},
{name: 'Sagadhe ', micro_water_shed_id: MicroWaterShed.where(name: 'Kengaki', code: '4B3E1G1d').first.id},
{name: 'Thammadahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kengaki', code: '4B3E1G1d').first.id},
{name: 'Bettadapura ', micro_water_shed_id: MicroWaterShed.where(name: 'Bettadapura', code: '4B3E1G1e').first.id},
{name: 'Kengaki ', micro_water_shed_id: MicroWaterShed.where(name: 'Bettadapura', code: '4B3E1G1e').first.id},
{name: 'Sagadhe ', micro_water_shed_id: MicroWaterShed.where(name: 'Bettadapura', code: '4B3E1G1e').first.id},
{name: 'Bettadhapura ', micro_water_shed_id: MicroWaterShed.where(name: 'Sagade', code: '4B3E1G1f').first.id},
{name: 'Kengaki ', micro_water_shed_id: MicroWaterShed.where(name: 'Sagade', code: '4B3E1G1f').first.id},
{name: 'Kethahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Sagade', code: '4B3E1G1f').first.id},
{name: 'Sagadhe', micro_water_shed_id: MicroWaterShed.where(name: 'Sagade', code: '4B3E1G1f').first.id},
{name: 'Kanchanhalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kethahalli', code: '4B3E1G2a').first.id},
{name: 'Kethahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Kethahalli', code: '4B3E1G2a').first.id},
{name: 'Moodanakodu', micro_water_shed_id: MicroWaterShed.where(name: 'Kethahalli', code: '4B3E1G2a').first.id},
{name: 'Sagadhe', micro_water_shed_id: MicroWaterShed.where(name: 'Kethahalli', code: '4B3E1G2a').first.id},
{name: 'Thammadahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kethahalli', code: '4B3E1G2a').first.id},
{name: 'Harave', micro_water_shed_id: MicroWaterShed.where(name: 'Hosahalli', code: '4B3E1G2b').first.id},
{name: 'Kethahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Hosahalli', code: '4B3E1G2b').first.id},
{name: 'Moodanakoodu', micro_water_shed_id: MicroWaterShed.where(name: 'Hosahalli', code: '4B3E1G2b').first.id},
{name: 'Thammadahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Hosahalli', code: '4B3E1G2b').first.id},
{name: 'Govindawadi', micro_water_shed_id: MicroWaterShed.where(name: 'Harve-2', code: '4B3E1G2c').first.id},
{name: 'Harave', micro_water_shed_id: MicroWaterShed.where(name: 'Harve', code: '4B3E1G2c').first.id},
{name: 'Kaggalipura', micro_water_shed_id: MicroWaterShed.where(name: 'Harve', code: '4B3E1G2c').first.id},
{name: 'Tammadahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Harve', code: '4B3E1G2c').first.id},
{name: 'Harave', micro_water_shed_id: MicroWaterShed.where(name: 'Harve-1', code: '4B3E1G2d').first.id},
{name: 'Kethanapura', micro_water_shed_id: MicroWaterShed.where(name: 'Harve-1', code: '4B3E1G2d').first.id},
{name: 'Govindawadi', micro_water_shed_id: MicroWaterShed.where(name: 'Ketanapura', code: '4B3E1G2e').first.id},
{name: 'Harave', micro_water_shed_id: MicroWaterShed.where(name: 'Ketanapura', code: '4B3E1G2e').first.id},
{name: 'Kethanapura', micro_water_shed_id: MicroWaterShed.where(name: 'Ketanapura', code: '4B3E1G2e').first.id},
{name: 'Mukadahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Ketanapura', code: '4B3E1G2e').first.id},
{name: 'Hooradahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code: '4B3E1G1a').first.id},
{name: 'Lakkuru', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code: '4B3E1G1a').first.id},
{name: 'Mukadahalli ', micro_water_shed_id: MicroWaterShed.where(name: 'Kumachahalli-II', code: '4B3E1G1a').first.id}].each do |t|
  Village.where(t).first_or_create
end

@dav = District.where(name: "Davanagere").first_or_create

[{name:'Harappanahalli', district_id: @dav.id},
{name:'Jagalur', district_id: @dav.id}].each do |t|
  Taluk.where(t).first_or_create
end


[{name:'Arasikere', taluk_id: Taluk.find_by_name('Harappanahalli').id},
{name:'Thelagi', taluk_id: Taluk.find_by_name('Harappanahalli').id},
{name:'Sokke', taluk_id: Taluk.find_by_name('Jagalur').id}].each do |t|
  Hobli.where(t).first_or_create
end


[{name:'Thauduru', hobli_id: Hobli.find_by_name('Arasikere').id},
{name:'Kanchikeri', hobli_id: Hobli.find_by_name('Arasikere').id},
{name:'Shingrihalli', hobli_id: Hobli.find_by_name('Arasikere').id},
{name:'Hiremagalageri', hobli_id: Hobli.find_by_name('Arasikere').id},
{name:'Yadihalli', hobli_id: Hobli.find_by_name('Thelagi').id},
{name:'Thelagi', hobli_id: Hobli.find_by_name('Thelagi').id},
{name:'Ragimasalawada', hobli_id: Hobli.find_by_name('Thelagi').id},
{name:'Shingrihalli', hobli_id: Hobli.find_by_name('Thelagi').id},
{name:'Gurusiddapura', hobli_id: Hobli.find_by_name('Sokke').id},
{name:'Hosakere', hobli_id: Hobli.find_by_name('Sokke').id},
{name:'Kechchenahalli', hobli_id: Hobli.find_by_name('Sokke').id},
{name:'Kyasenahalli', hobli_id: Hobli.find_by_name('Sokke').id},
{name:'Pallagatte', hobli_id: Hobli.find_by_name('Sokke').id},
{name:'Sokke', hobli_id: Hobli.find_by_name('Sokke').id},
{name:'Bilichodu', hobli_id: Hobli.find_by_name('Sokke').id}].each do |t|
  Grampanchayat.where(t).first_or_create
end



[{name:'Venkatapura', code:'4D4B4E', district_id: @dav.id},
{name:'Gaudagondanahalli', code: '4D4B2B', district_id: @dav.id},
{name:'Hanumanahalli', code:'4D4B2D', district_id: @dav.id}].each do |t|
  SubWaterShed.where(t).first_or_create
end


[{name:'Chikka Tanda',code:'4D4C1L1a', sub_water_shed_id: SubWaterShed.where(name:'Aliganchikere', code: '4D4C1L', district_id: @dav.id).first_or_create.id},
{name:'Telagi',code:'4D4C1L2b', sub_water_shed_id: SubWaterShed.where(name:'Aliganchikere', code: '4D4C1L', district_id: @dav.id).first_or_create.id},
{name:'Hallikeri-2',code:'4D4C1H1g', sub_water_shed_id: SubWaterShed.where(name:'Kunchikere', code: '4D4C1H', district_id: @dav.id).first_or_create.id}, 
{name:'Honnenahalli',code:'4D4C1H2a', sub_water_shed_id: SubWaterShed.where(name:'Kunchikere', code: '4D4C1H', district_id: @dav.id).first_or_create.id},
{name:'Vaddarahalli',code:'4D4C1H2b', sub_water_shed_id: SubWaterShed.where(name:'Kunchikere', code: '4D4C1H', district_id: @dav.id).first_or_create.id}, 
{name:'Nagalapura West',code:'4D4C1I2c', sub_water_shed_id: SubWaterShed.where(name:'Singrahalli', code: '4D4C1I', district_id: @dav.id).first_or_create.id},
{name:'Gollarahatti',code:'4D4C1I1c', sub_water_shed_id: SubWaterShed.where(name:'Singrahalli', code: '4D4C1I', district_id: @dav.id).first_or_create.id},
{name:'Malemachikere-1',code:'4D4B4E1b', sub_water_shed_id: SubWaterShed.where(name:'Venkatapura', code: '4D4B4E', district_id: @dav.id).first_or_create.id},
{name:'Gaudikatte',code:'4D4B4E1c', sub_water_shed_id: SubWaterShed.where(name:'Venkatapura', code: '4D4B4E', district_id: @dav.id).first_or_create.id},
{name:'Hosakere',code:'4D4B2B2g', sub_water_shed_id: SubWaterShed.where(name:'Gaudagondanahalli', code: '4D4B2B', district_id: @dav.id).first_or_create.id},
{name:'Gollarahatti-2',code:'4D4B2B2f', sub_water_shed_id: SubWaterShed.where(name:'Gaudagondanahalli', code: '4D4B2B', district_id: @dav.id).first_or_create.id},
{name:'Gaudagondanahalli-4',code:'4D4B2B1d', sub_water_shed_id: SubWaterShed.where(name:'Gaudagondanahalli', code: '4D4B2B', district_id: @dav.id).first_or_create.id},
{name:'Gaudagondanahalli-3',code:'4D4B2B1c', sub_water_shed_id: SubWaterShed.where(name:'Gaudagondanahalli', code: '4D4B2B', district_id: @dav.id).first_or_create.id},
{name:'Lakkampur West2',code:'4D4B2D2a', sub_water_shed_id: SubWaterShed.where(name:'Hanumanahalli', code: '4D4B2D', district_id: @dav.id).first_or_create.id},
{name:'Lakkampur West1',code:'4D4B2D2b', sub_water_shed_id: SubWaterShed.where(name:'Hanumanahalli', code: '4D4B2D', district_id: @dav.id).first_or_create.id},
{name:'Chikkaujjini',code:'4D4B2D1a', sub_water_shed_id: SubWaterShed.where(name:'Hanumanahalli', code: '4D4B2D', district_id: @dav.id).first_or_create.id},
{name:'Bantanahalli-2',code:'4D4B2D1c', sub_water_shed_id: SubWaterShed.where(name:'Hanumanahalli', code: '4D4B2D', district_id: @dav.id).first_or_create.id},
{name:'Bantanahalli-1',code:'4D4B2D1d', sub_water_shed_id: SubWaterShed.where(name:'Hanumanahalli', code: '4D4B2D', district_id: @dav.id).first_or_create.id}].each do |t|
  MicroWaterShed.where(t).first_or_create
end


[{name: 'Bommarasanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Bandanahalli-3', code:'4D3D8S1c').first_or_create.id},
{name: 'Kyarakatte', micro_water_shed_id: MicroWaterShed.where(name: 'Chikka Tanda', code:'4D4C1L1a').first_or_create.id},
{name: 'Yadihalli', micro_water_shed_id: MicroWaterShed.where(name: 'Chikka Tanda', code:'4D4C1L1a').first_or_create.id},
{name: 'Thelagi', micro_water_shed_id: MicroWaterShed.where(name: 'Telagi', code:'4D4C1L2b').first_or_create.id},
{name: 'Hallikere', micro_water_shed_id: MicroWaterShed.where(name: 'Hallikeri-2', code:'4D4C1H1g').first_or_create.id},
{name: 'Kanchigeri', micro_water_shed_id: MicroWaterShed.where(name: 'Hallikeri-2', code:'4D4C1H1g').first_or_create.id},
{name: 'Satturu', micro_water_shed_id: MicroWaterShed.where(name: 'Hallikeri-2', code:'4D4C1H1g').first_or_create.id},
{name: 'Hallikere', micro_water_shed_id: MicroWaterShed.where(name: 'Honnenahalli', code:'4D4C1H2a').first_or_create.id},
{name: 'Ragimasalavada', micro_water_shed_id: MicroWaterShed.where(name: 'Honnenahalli', code:'4D4C1H2a').first_or_create.id},
{name: 'Hallikere', micro_water_shed_id: MicroWaterShed.where(name: 'Vaddarahalli', code:'4D4C1H2b').first_or_create.id},
{name: 'Kanchigeri', micro_water_shed_id: MicroWaterShed.where(name: 'Vaddarahalli', code:'4D4C1H2b').first_or_create.id},
{name: 'Ragimasalavada', micro_water_shed_id: MicroWaterShed.where(name: 'Nagalapura West', code:'4D4C1I2c').first_or_create.id},
{name: 'Singrahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Nagalapura West', code:'4D4C1I2c').first_or_create.id},
{name: 'Hiremagalakeri', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti', code:'4D4C1I1c').first_or_create.id},
{name: 'Satturu', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti', code:'4D4C1I1c').first_or_create.id},
{name: 'Singrahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti', code:'4D4C1I1c').first_or_create.id},
{name: 'Gowdikatte', micro_water_shed_id: MicroWaterShed.where(name: 'Malemachikere-1', code:'4D4B4E1b').first_or_create.id},
{name: 'Mallemajikere', micro_water_shed_id: MicroWaterShed.where(name: 'Malemachikere-1', code:'4D4B4E1b').first_or_create.id},
{name: 'Gowdikatte', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudikatte', code:'4D4B4E1c').first_or_create.id},
{name: 'Mallemajikere', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudikatte', code:'4D4B4E1c').first_or_create.id},
{name: 'Hirebannihatti', micro_water_shed_id: MicroWaterShed.where(name: 'Hosakere', code:'4D4B2B2g').first_or_create.id},
{name: 'Hosakere', micro_water_shed_id: MicroWaterShed.where(name: 'Hosakere', code:'4D4B2B2g').first_or_create.id},
{name: 'Krishnapura', micro_water_shed_id: MicroWaterShed.where(name: 'Hosakere', code:'4D4B2B2g').first_or_create.id},
{name: 'Talavara Kariyappanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Hosakere', code:'4D4B2B2g').first_or_create.id},
{name: 'Chikkabannihatti', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti-2', code:'4D4B2B2f').first_or_create.id},
{name: 'Hirebannihatti', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti-2', code:'4D4B2B2f').first_or_create.id},
{name: 'Hosakere', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti-2', code:'4D4B2B2f').first_or_create.id},
{name: 'Seetharasanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gollarahatti-2', code:'4D4B2B2f').first_or_create.id},
{name: 'Veeravvanagathihalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-4', code:'4D4B2B1d').first_or_create.id},
{name: 'Gowdagondanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-4', code:'4D4B2B1d').first_or_create.id},
{name: 'Hosakere', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-4', code:'4D4B2B1d').first_or_create.id},
{name: 'Anebenakanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-3', code:'4D4B2B1c').first_or_create.id},
{name: 'Gowdagondanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-3', code:'4D4B2B1c').first_or_create.id},
{name: 'Godegobanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-3', code:'4D4B2B1c').first_or_create.id},
{name: 'Kysenahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Gaudagondanahalli-3', code:'4D4B2B1c').first_or_create.id},
{name: 'Katenahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West2', code:'4D4B2D2a').first_or_create.id},
{name: 'Mallemajikere', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West2', code:'4D4B2D2a').first_or_create.id},
{name: 'Talavara Kariyappanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West2', code:'4D4B2D2a').first_or_create.id},
{name: 'Yaralakatte', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West2', code:'4D4B2D2a').first_or_create.id},
{name: 'Katenahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West1', code:'4D4B2D2b').first_or_create.id},
{name: 'Krishnapura', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West1', code:'4D4B2D2b').first_or_create.id},
{name: 'Lakkamapura', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West1', code:'4D4B2D2b').first_or_create.id},
{name: 'Medakeripura', micro_water_shed_id: MicroWaterShed.where(name: 'Lakkampur West1', code:'4D4B2D2b').first_or_create.id},
{name: 'Chikkavujjani', micro_water_shed_id: MicroWaterShed.where(name: 'Chikkaujjini', code:'4D4B2D1a').first_or_create.id},
{name: 'Chikkabantanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-2', code:'4D4B2D1c').first_or_create.id},
{name: 'Gadimakunte', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-2', code:'4D4B2D1c').first_or_create.id},
{name: 'Gopalapura', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-2', code:'4D4B2D1c').first_or_create.id},
{name: 'Medakeripura', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-2', code:'4D4B2D1c').first_or_create.id},
{name: 'Sokke', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-2', code:'4D4B2D1c').first_or_create.id},
{name: 'Chikkabantanahalli', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-1', code:'4D4B2D1d').first_or_create.id},
{name: 'Sokke', micro_water_shed_id: MicroWaterShed.where(name: 'Bantanahalli-1', code:'4D4B2D1d').first_or_create.id}].each do |t|
  Village.where(t).first_or_create
end

