FROM ruby:2.3.3

RUN apt-get update -qq && apt-get install -qq -y --fix-missing --no-install-recommends build-essential nodejs nginx

ENV APP_HOME /usr/src/app
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
RUN mkdir -p shared/pids shared/sockets shared/log

COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install --jobs 20 --retry 5 --without development test
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES=true
ENV SECRET_KEY_BASE 98c8df39a61752765211dd35b9f2cf7b467ae99962464b67fd9e97e5b67312d0f14b4ef77a082acb675540c3913135ed4209ae7753fc8f751d4af1c93a887c96

ADD . .
RUN bundle exec rake assets:precompile

EXPOSE 80 9292

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/mime.types /etc/nginx/mime.types

CMD service nginx start && bundle exec puma -C config/puma.rb

