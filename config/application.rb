require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Sujala
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.action_dispatch.default_headers = {
      'Access-Control-Allow-Origin' => 'http://ameyasujala.com',
      'Access-Control-Request-Method' => %w{GET POST OPTIONS}.join(",")
    }

    config.middleware.insert_before 0, Rack::Cors do
  	  allow do
        origins 'http://ameyasujala.com'

	    resource '*',
	      :headers => :any,
	      :methods => [:get, :post, :delete, :put, :options, :head],
	      :expose  => ['access-token', 'expiry', 'token-type', 'uid', 'client'],
	      :max_age => 2
      end
	end
  end
end
