Rails.application.routes.draw do

  resources :credit_sources
  resources :crop_types
  mount_devise_token_auth_for 'User', at: 'auth'
  get 'report/index'
  get 'report/topography'
  get 'report/smu'
  get 'report/category'
  get 'report/report_list'
  get 'report/farmer_count'
  get 'report/cultivation_cost'
  get 'report/live_stock'
  get 'report/main_yield'
  get 'report/marketing'
  get 'report/comments'
  get 'report/soil_status'
  get 'report/additional_invst_src'
  
  get 'farmers/count'
  get 'users/register'
  get 'users/signin'
  get 'users/template'
  get 'users/index'
  get 'users/actions'
  get 'users/user_roles_template'
  get 'users/user_roles'
  post 'users/create_user_role'
  post 'users/delete_user_role'
 

  post 'offline/sync'

  get "/application.appcache", to: "offline#manifest", :format => :appcache

  get 'roles/ractions'
  get 'roles/role_ractions_template'
  get 'roles/role_ractions'
  get 'roles/role'
  post 'roles/create_role_raction'
  post 'roles/delete_role_raction'

    resources :ractions
  resources :actions
  resources :roles

    root 'socio_demographic_infos#new'
  resources :basic_need_details
  resources :migration_consequences
  resources :labour_migration_details
  resources :migration_negative_consequences
  resources :migration_positive_consequences
  resources :migration_purposes
  resources :fertilizers
  resources :pc_wage_rates
  resources :pc_yield_prices
  resources :intercrop_lists
  resources :crop_lists
  resources :pc_other_expenses
  resources :pc_operation_details
  resources :survey_numbers
  resources :forestry_plants
  resources :horticulture_plants
  resources :livestock_names
  resources :credit_avail_purposes
  resources :suggestion_paramaters
  resources :food_materials
  resources :constraints
  resources :conversation_structures
  resources :particulars
  resources :purposes
  resources :income_sources
  resources :intercrops
  resources :crops
  resources :grampanchayats
  resources :hoblis
  resources :micro_water_sheds
  resources :sub_water_sheds
  resources :taluks
  resources :districts
  resources :castes
  resources :smus
  resources :topographies
  resources :farmers
  resources :horticulture_forestry_plants
  resources :production_costs
  resources :fertilizer_details
  resources :ppc_details
  resources :forestry_cultivations
  resources :forestry_species
  resources :cultivate_horticultures
  resources :plant_counts
  resources :subsidiary_costs
  resources :subsidiary_enterprises
  resources :private_credits
  resources :institutional_credits
  resources :borrowing_statuses
  resources :basic_needs
  resources :labour_availabilities
  resources :watershed_comments
  resources :improvement_suggestions
  resources :household_food_materials
  resources :farming_constraints
  resources :conversation_practices
  resources :watershed_awarenesses
  resources :marketings
  resources :additional_investment_capacities
  resources :family_annual_incomes
  resources :fodder_availabilities
  resources :cropping_intensities
  resources :cropping_paterns
  resources :irrigations
  resources :surveys

  resources :durable_assets
  resources :machines
  resources :livestocks
  resources :livestock_types
  resources :machine_types
  resources :asset_types
  resources :house_types
  resources :houses
  resources :housetypes
  resources :local_organisations
  resources :genders
  resources :occupations
  resources :scaffolds
  resources :educations
  resources :socio_demographic_infos
  resources :villages
  match "*path", to: "welcome#catch_404", via: :all

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
